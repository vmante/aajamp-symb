# aajamp-symb
*Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel, Lorenzo Tancredi*

`aajamp-symb` is a repository which provides analytic results for one-loop and two-loop QCD corrections to diphoton production in association with an extra jet in full colour.

If you use the results distributed with `aajamp-symb` in your research work, please cite [2105.04585][6] along with its external dependency [2009.07803][3].

## External dependencies
The results distributed through this repository are in *Mathematica* readable format. Therefore, all the relevant symbolic manipulations and numerical evaluations can be carried out using *Mathematica*.

The evaluation of the transcendental functions relies on the *Mathematica* package [PentagonMI][7] by D. Chicherin and V. Sotnikov, so we strongly recommend to have this available. Further details on how to install and use the package can be found in the git repository [PentagonMI][7].

## Structure of the repository
The main object of this repository are the results for the one- and two-loop finite remainders of the helicity amplitudes for diphoton plus jet production. They are located in **helicity_remainders/**. See **helicity_remainders/README.md** for further details on the actual content of the files and the naming scheme adopted.

The **aux/** directory contains auxiliary files needed for the symbolic manipulation and numerical evaluation of the results in **helicity_remainders/**. Further, we provide files with the explicit expressions for the Catani $`I_1`$ and $`I_2`$ operators for the processes at hand (see [hep-ph/9802439][8] and the supplemental material in [2105.04585][6]).

In **integral_families/** we list the choice of integral families we adopted in our calculation of the one- and two-loop helicity amplitudes. Files are in *yaml* format.

Finally, in **examples/** we provide a few demos which show how to evaluate numerically the finite remainders of the helicity amplitudes, and how to construct the interference with the corresponding tree level. See **examples/README.md** for more details on each example file.


[1]: https://arxiv.org/abs/2102.01820 "2102.01820"
[2]: https://gitlab.com/pentagon-functions/PentagonFunctions-cpp "PentagonFunctions"
[3]: https://arxiv.org/abs/2009.07803 "2009.07803"
[4]: https://www.davidhbailey.com/dhbsoftware
[5]: https://www.davidhbailey.com/dhbpapers/quad-double.pdf
[6]: https://arxiv.org/abs/2105.04585 "2105.04585"
[7]: https://gitlab.com/pentagon-functions/PentagonMI "PentagonMI"
[8]: https://arxiv.org/abs/hep-ph/9802439 "hep-ph/9802439"
