(* qqb channel *)

I1["qqb"] = (-(Ca/2) - Cf)/ep^2 - 1/6 nf Tr F[1, 1, 2] - 
  1/6 nf Tr F[1, 1, 6] + ((nf Tr)/3 + 
  Cf (-(3/2) + F[1, 1, 1] - tci[1, 2]) + 
  Ca (-(11/12) - 1/2 F[1, 1, 1] + 1/2 F[1, 1, 2] + 1/2 F[1, 1, 6] + 1/2 tci[1, 2]))/ep + 
  Ca (-(3/4) F[1, 1, 1] + 1/4 F[1, 1, 1]^2 + 5/6 F[1, 1, 2] - 
  1/4 F[1, 1, 2]^2 + 5/6 F[1, 1, 6] - 1/4 F[1, 1, 6]^2 + 
  5/24 tci[1, 1]^2 + 3/4 tci[1, 2] - 1/2 F[1, 1, 1] tci[1, 2]) + 
  Cf (3/2 F[1, 1, 1] - 1/2 F[1, 1, 1]^2 - 7/12 tci[1, 1]^2 - 3/2 tci[1, 2] + F[1, 1, 1] tci[1, 2]) + 
  ep (1/12 nf Tr F[1, 1, 2]^2 + 1/12 nf Tr F[1, 1, 6]^2 + 1/36 nf Tr tci[1, 1]^2 + 
  Ca (3/8 F[1, 1, 1]^2 - 1/12 F[1, 1, 1]^3 - 5/12 F[1, 1, 2]^2 + 
  1/12 F[1, 1, 2]^3 - 5/12 F[1, 1, 6]^2 + 1/12 F[1, 1, 6]^3 + 
  43/144 tci[1, 1]^2 - 7/24 F[1, 1, 1] tci[1, 1]^2 + 
  1/24 F[1, 1, 2] tci[1, 1]^2 + 1/24 F[1, 1, 6] tci[1, 1]^2 - 
  3/4 F[1, 1, 1] tci[1, 2] + 1/4 F[1, 1, 1]^2 tci[1, 2] + 
  1/8 tci[1, 1]^2 tci[1, 2] + 1/6 tcr[3, 3]) + 
  Cf (-(3/4) F[1, 1, 1]^2 + 1/6 F[1, 1, 1]^3 - 7/8 tci[1, 1]^2 + 
  7/12 F[1, 1, 1] tci[1, 1]^2 + 3/2 F[1, 1, 1] tci[1, 2] - 
  1/2 F[1, 1, 1]^2 tci[1, 2] - 1/4 tci[1, 1]^2 tci[1, 2] + 1/3 tcr[3, 3])) + 
  ep^2 (-(1/36) nf Tr F[1, 1, 2]^3 - 1/36 nf Tr F[1, 1, 6]^3 - 1/72 nf Tr F[1, 1, 2] tci[1, 1]^2 - 
  1/72 nf Tr F[1, 1, 6] tci[1, 1]^2 - 1/9 nf Tr tcr[3, 3] + 
  Ca (-(1/8) F[1, 1, 1]^3 + 1/48 F[1, 1, 1]^4 + 5/36 F[1, 1, 2]^3 - 
  1/48 F[1, 1, 2]^4 + 5/36 F[1, 1, 6]^3 - 1/48 F[1, 1, 6]^4 - 
  7/16 F[1, 1, 1] tci[1, 1]^2 + 7/48 F[1, 1, 1]^2 tci[1, 1]^2 + 
  5/72 F[1, 1, 2] tci[1, 1]^2 - 1/48 F[1, 1, 2]^2 tci[1, 1]^2 + 
  5/72 F[1, 1, 6] tci[1, 1]^2 - 1/48 F[1, 1, 6]^2 tci[1, 1]^2 + 
  (119 tci[1, 1]^4)/2880 + 3/8 F[1, 1, 1]^2 tci[1, 2] - 
  1/12 F[1, 1, 1]^3 tci[1, 2] + 3/16 tci[1, 1]^2 tci[1, 2] - 
  1/8 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] + 11/36 tcr[3, 3] + 
  1/6 F[1, 1, 1] tcr[3, 3] - 1/6 F[1, 1, 2] tcr[3, 3] - 
  1/6 F[1, 1, 6] tcr[3, 3] - 1/6 tci[1, 2] tcr[3, 3]) + 
  Cf (1/4 F[1, 1, 1]^3 - 1/24 F[1, 1, 1]^4 + 
  7/8 F[1, 1, 1] tci[1, 1]^2 - 7/24 F[1, 1, 1]^2 tci[1, 1]^2 - 
  (121 tci[1, 1]^4)/1440 - 3/4 F[1, 1, 1]^2 tci[1, 2] + 
  1/6 F[1, 1, 1]^3 tci[1, 2] - 3/8 tci[1, 1]^2 tci[1, 2] + 
  1/4 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] + 1/2 tcr[3, 3] - 
  1/3 F[1, 1, 1] tcr[3, 3] + 1/3 tci[1, 2] tcr[3, 3]));

I2["qqb"] = -((Ca + 2 Cf)^2/(8 ep^4)) + 5/27 nf^2 Tr^2 F[1, 1, 2] - 
  7/72 nf^2 Tr^2 F[1, 1, 2]^2 + 5/27 nf^2 Tr^2 F[1, 1, 6] - 
  1/36 nf^2 Tr^2 F[1, 1, 2] F[1, 1, 6] - 
  7/72 nf^2 Tr^2 F[1, 1, 6]^2 + ((11 Ca^2)/48 - (7 Ca Cf)/24 - (3 Cf^2)/2 - (Ca nf Tr)/12 - (Cf nf Tr)/6 + 
  (-(Ca^2/4) + Cf^2) F[1, 1, 1] + (Ca^2/4 + (Ca Cf)/2) F[1, 1, 2] + 
  (Ca^2/4 + (Ca Cf)/2) F[1, 1, 6] + (Ca^2/4 - Cf^2) tci[1, 2])/ep^3 + 
  (1/(ep^2))(-((13 Ca^2)/288) - (67 Ca Cf)/72 - (9 Cf^2)/8 - (Ca nf Tr)/6 + (5 Cf nf Tr)/18 + 
  (nf^2 Tr^2)/18 + ((Ca Cf)/2 - Cf^2) F[1, 1, 1]^2 + (-(Ca^2/4) - (Ca Cf)/4) F[1,1, 2]^2 + 
  (-(Ca^2/4) - (Ca Cf)/4) F[1, 1, 6]^2 + (Ca^2/12 - (Ca Cf)/8 - (7 Cf^2)/12) tci[1, 1]^2 + 
  ((3 Ca^2)/8 + (3 Ca Cf)/4 - 3 Cf^2) tci[1, 2] + (-(Ca^2/8) + (Ca Cf)/2 - Cf^2/2) tci[1, 2]^2 + 
  F[1, 1, 6] ((5 Ca^2)/12 + (19 Ca Cf)/12 - (Ca nf Tr)/12 - (Cf nf Tr)/6 + (-(Ca^2/4) + (Ca Cf)/2) tci[1, 2]) + 
  F[1, 1, 2] ((5 Ca^2)/12 + (19 Ca Cf)/12 - (Ca nf Tr)/12 - (Cf nf Tr)/6 - 1/4 Ca^2 F[1, 1, 6] + 
  (-(Ca^2/4) + (Ca Cf)/2) tci[1, 2]) + F[1, 1, 1] (-((3 Ca^2)/8) - (3 Ca Cf)/4 + 3 Cf^2 + 
  (Ca^2/4 - (Ca Cf)/2) F[1, 1, 2] + (Ca^2/4 - (Ca Cf)/2) F[1, 1, 6] + (-Ca Cf + 2 Cf^2) tci[1, 2])) + 
  Cf nf Tr (-(5/3) F[1, 1, 1] + 47/36 F[1, 1, 1]^2 - 
  1/6 F[1, 1, 1]^3 + 1/4 F[1, 1, 1] F[1, 1, 2] - 
  1/12 F[1, 1, 1]^2 F[1, 1, 2] + 1/8 F[1, 1, 2]^2 - 
  1/12 F[1, 1, 1] F[1, 1, 2]^2 - 1/36 F[1, 1, 2]^3 + 
  1/4 F[1, 1, 1] F[1, 1, 6] - 1/12 F[1, 1, 1]^2 F[1, 1, 6] + 
  1/8 F[1, 1, 6]^2 - 1/12 F[1, 1, 1] F[1, 1, 6]^2 - 
  1/36 F[1, 1, 6]^3 + 37/27 tci[1, 1]^2 - 
  19/36 F[1, 1, 1] tci[1, 1]^2 - 1/9 F[1, 1, 2] tci[1, 1]^2 - 
  1/9 F[1, 1, 6] tci[1, 1]^2 + 5/3 tci[1, 2] - 
  47/18 F[1, 1, 1] tci[1, 2] + 1/2 F[1, 1, 1]^2 tci[1, 2] - 
  1/4 F[1, 1, 2] tci[1, 2] + 1/6 F[1, 1, 1] F[1, 1, 2] tci[1, 2] + 
  1/12 F[1, 1, 2]^2 tci[1, 2] - 1/4 F[1, 1, 6] tci[1, 2] + 
  1/6 F[1, 1, 1] F[1, 1, 6] tci[1, 2] + 
  1/12 F[1, 1, 6]^2 tci[1, 2] + 7/36 tci[1, 1]^2 tci[1, 2] - 
  1/18 tcr[3, 3]) + (1/ep)(-((737 Ca^2)/432) - (67 Ca Cf)/24 + (5 Ca^2)/48 + (245 Ca Cf)/432 - (3 Cf^2)/16 + 
  (61 Ca nf Tr)/54 + (5 Cf nf Tr)/6 - 29/54 Ca nf Tr + 
  1/4 Cf nf Tr - 25/108 Cf nf Tr - (5 nf^2 Tr^2)/27 + 
  5/27 nf^2 Tr^2 + (Ca^2/8 - (Ca Cf)/2 + Cf^2/2) F[1, 1, 1]^3 + 1/8 Ca^2 F[1, 1, 2]^3 + 1/8 Ca^2 F[1, 1, 6]^3 + 
  Cf nf Tr (1/12 F[1, 1, 2]^2 + 1/12 F[1, 1, 6]^2 + 
  1/36 tci[1, 1]^2) + ((233 Ca^2)/144 - (19 Ca Cf)/9 - (9 Cf^2)/4 - 
  (19 Ca nf Tr)/36 + (19 Cf nf Tr)/18) tci[1, 2] + (-((3 Ca^2)/8) + (3 Ca Cf)/2 - (3 Cf^2)/2) tci[1, 2]^2 + 
  F[1, 1, 6]^2 (-((31 Ca^2)/48) - (3 Ca Cf)/8 + (Ca nf Tr)/6 + (Ca^2/8 - (Ca Cf)/4) tci[1, 2]) + 
  F[1, 1, 2]^2 (-((31 Ca^2)/48) - (3 Ca Cf)/8 + (Ca nf Tr)/6 + 
  1/8 Ca^2 F[1, 1, 6] + (Ca^2/8 - (Ca Cf)/4) tci[1, 2]) + F[1, 1, 1]^2 (-((7 Ca^2)/48) + (17 Ca Cf)/12 - 
  (9 Cf^2)/4 - (Ca nf Tr)/12 + (Cf nf Tr)/6 + (-(Ca^2/8) + (Ca Cf)/4) F[1, 1, 2] + 
  (-(Ca^2/8) + (Ca Cf)/4) F[1, 1, 6] + (-((3 Ca^2)/8) + (3 Ca Cf)/2 - (3 Cf^2)/2) tci[1, 2]) + 
  tci[1, 1]^2 ((11 Ca^2)/64 - (67 Ca Cf)/288 - (7 Cf^2)/8 - (11 Ca^2)/576 + (23 Ca Cf)/96 - (Cf^2)/4 - 
  (Ca nf Tr)/16 + (11 Cf nf Tr)/72 + 1/144 Ca nf Tr - 1/24 Cf nf Tr + (-(Ca^2/16) + (5 Ca Cf)/12 - (7 Cf^2)/12) tci[1, 2]) + 
  F[1, 1, 6] ((61 Ca^2)/36 + (5 Ca Cf)/4 - (17 Ca nf Tr)/24 - (Cf nf Tr)/4 + (nf^2 Tr^2)/18 + 
  (-(Ca^2/16) + (7 Ca Cf)/24) tci[1, 1]^2 + (-((19 Ca^2)/24) + (19 Ca Cf)/12 + (Ca nf Tr)/12 - 
  (Cf nf Tr)/6) tci[1, 2]) + F[1, 1, 2] ((61 Ca^2)/36 + (5 Ca Cf)/4 - (17 Ca nf Tr)/24 - 
  (Cf nf Tr)/4 + (nf^2 Tr^2)/18 + (-((5 Ca^2)/6) + (Ca nf Tr)/6) F[1, 1, 6] + 
  1/8 Ca^2 F[1, 1, 6]^2 + (-(Ca^2/16) + (7 Ca Cf)/24) tci[1, 1]^2 + (-((19 Ca^2)/24) + (19 Ca Cf)/12 + (Ca nf Tr)/12 - (Cf nf Tr)/6) tci[1, 2]) + 
  F[1, 1, 1] (-((233 Ca^2)/144) + (19 Ca Cf)/9 + (9 Cf^2)/4 + (19 Ca nf Tr)/36 - 
  (19 Cf nf Tr)/18 + (-(Ca^2/8) + (Ca Cf)/4) F[1, 1, 2]^2 + (-(Ca^2/8) + (Ca Cf)/4) F[1, 1, 6]^2 + 
  (Ca^2/16 - (5 Ca Cf)/12 + (7 Cf^2)/12) tci[1, 1]^2 + ((7 Ca^2)/24 - 
  (17 Ca Cf)/6 + (9 Cf^2)/2 + (Ca nf Tr)/6 - (Cf nf Tr)/3) tci[1, 2] + (Ca^2/4 - Ca Cf + Cf^2) tci[1, 2]^2 + 
  F[1, 1, 2] ((19 Ca^2)/24 - (19 Ca Cf)/12 - (Ca nf Tr)/12 + (Cf nf Tr)/6 + (Ca^2/4 - (Ca Cf)/2) tci[1, 2]) + 
  F[1, 1, 6] ((19 Ca^2)/24 - (19 Ca Cf)/12 - (Ca nf Tr)/12 + (Cf nf Tr)/6 + (Ca^2/4 - (Ca Cf)/2) tci[1, 2])) + 
  Ca (nf Tr (1/24 F[1, 1, 2]^2 + 1/24 F[1, 1, 6]^2 + 1/72 tci[1, 1]^2) + 
  Cf (-(5/12) F[1, 1, 2]^2 + 1/12 F[1, 1, 2]^3 - 
  5/12 F[1, 1, 6]^2 + 1/12 F[1, 1, 6]^3 - 5/36 tci[1, 1]^2 + 
  1/24 F[1, 1, 2] tci[1, 1]^2 + 1/24 F[1, 1, 6] tci[1, 1]^2 + 
  1/3 tcr[3, 3])) + 
  Ca^2 (3/16 F[1, 1, 1]^2 - 1/24 F[1, 1, 1]^3 - 5/24 F[1, 1, 2]^2 + 
  1/24 F[1, 1, 2]^3 - 5/24 F[1, 1, 6]^2 + 1/24 F[1, 1, 6]^3 + 
  43/288 tci[1, 1]^2 - 7/48 F[1, 1, 1] tci[1, 1]^2 + 
  1/48 F[1, 1, 2] tci[1, 1]^2 + 1/48 F[1, 1, 6] tci[1, 1]^2 - 
  3/8 F[1, 1, 1] tci[1, 2] + 1/8 F[1, 1, 1]^2 tci[1, 2] + 
  1/16 tci[1, 1]^2 tci[1, 2] + 1/12 tcr[3, 3]) + 
  Cf^2 (-(3/4) F[1, 1, 1]^2 + 1/6 F[1, 1, 1]^3 - 7/8 tci[1, 1]^2 + 
  7/12 F[1, 1, 1] tci[1, 1]^2 + 3/2 F[1, 1, 1] tci[1, 2] - 
  1/2 F[1, 1, 1]^2 tci[1, 2] - 1/4 tci[1, 1]^2 tci[1, 2] + 
  1/3 tcr[3, 3]) + ((Ca^2)/8 + (13 Ca Cf)/4 - 
  3 Cf^2) tcr[3, 3]) + 
  Ca^2 (-(67/24) F[1, 1, 1] + 121/72 F[1, 1, 1]^2 + 
  1/12 F[1, 1, 1]^3 - 1/16 F[1, 1, 1]^4 + 335/108 F[1, 1, 2] + 
  5/8 F[1, 1, 1] F[1, 1, 2] - 19/48 F[1, 1, 1]^2 F[1, 1, 2] + 
  1/24 F[1, 1, 1]^3 F[1, 1, 2] - 349/144 F[1, 1, 2]^2 - 
  19/48 F[1, 1, 1] F[1, 1, 2]^2 + 1/16 F[1, 1, 1]^2 F[1, 1, 2]^2 + 
  103/144 F[1, 1, 2]^3 + 1/24 F[1, 1, 1] F[1, 1, 2]^3 - 
  1/12 F[1, 1, 2]^4 + 335/108 F[1, 1, 6] + 
  5/8 F[1, 1, 1] F[1, 1, 6] - 19/48 F[1, 1, 1]^2 F[1, 1, 6] + 
  1/24 F[1, 1, 1]^3 F[1, 1, 6] - 25/36 F[1, 1, 2] F[1, 1, 6] + 
  5/12 F[1, 1, 2]^2 F[1, 1, 6] - 1/24 F[1, 1, 2]^3 F[1, 1, 6] - 
  349/144 F[1, 1, 6]^2 - 19/48 F[1, 1, 1] F[1, 1, 6]^2 + 
  1/16 F[1, 1, 1]^2 F[1, 1, 6]^2 + 5/12 F[1, 1, 2] F[1, 1, 6]^2 - 
  1/16 F[1, 1, 2]^2 F[1, 1, 6]^2 + 103/144 F[1, 1, 6]^3 + 
  1/24 F[1, 1, 1] F[1, 1, 6]^3 - 1/24 F[1, 1, 2] F[1, 1, 6]^3 - 
  1/12 F[1, 1, 6]^4 + (3323 tci[1, 1]^2)/1728 - 
  101/288 F[1, 1, 1] tci[1, 1]^2 - 5/24 F[1, 1, 1]^2 tci[1, 1]^2 - 
  43/288 F[1, 1, 2] tci[1, 1]^2 + 
  1/6 F[1, 1, 1] F[1, 1, 2] tci[1, 1]^2 - 
  1/48 F[1, 1, 2]^2 tci[1, 1]^2 - 43/288 F[1, 1, 6] tci[1, 1]^2 + 
  1/6 F[1, 1, 1] F[1, 1, 6] tci[1, 1]^2 - 
  1/24 F[1, 1, 2] F[1, 1, 6] tci[1, 1]^2 - 
  1/48 F[1, 1, 6]^2 tci[1, 1]^2 - 17/720 tci[1, 1]^4 + 
  67/24 tci[1, 2] - 121/36 F[1, 1, 1] tci[1, 2] - 
  1/4 F[1, 1, 1]^2 tci[1, 2] + 1/4 F[1, 1, 1]^3 tci[1, 2] - 
  5/8 F[1, 1, 2] tci[1, 2] + 
  19/24 F[1, 1, 1] F[1, 1, 2] tci[1, 2] - 
  1/8 F[1, 1, 1]^2 F[1, 1, 2] tci[1, 2] + 
  19/48 F[1, 1, 2]^2 tci[1, 2] - 
  1/8 F[1, 1, 1] F[1, 1, 2]^2 tci[1, 2] - 
  1/24 F[1, 1, 2]^3 tci[1, 2] - 5/8 F[1, 1, 6] tci[1, 2] + 
  19/24 F[1, 1, 1] F[1, 1, 6] tci[1, 2] - 
  1/8 F[1, 1, 1]^2 F[1, 1, 6] tci[1, 2] + 
  19/48 F[1, 1, 6]^2 tci[1, 2] - 
  1/8 F[1, 1, 1] F[1, 1, 6]^2 tci[1, 2] - 
  1/24 F[1, 1, 6]^3 tci[1, 2] + 41/288 tci[1, 1]^2 tci[1, 2] + 
  1/6 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] - 
  1/12 F[1, 1, 2] tci[1, 1]^2 tci[1, 2] - 
  1/12 F[1, 1, 6] tci[1, 1]^2 tci[1, 2] - 9/32 tci[1, 2]^2 + 
  3/8 F[1, 1, 1] tci[1, 2]^2 - 1/8 F[1, 1, 1]^2 tci[1, 2]^2 + 
  11/144 tcr[3, 3] + 1/6 F[1, 1, 1] tcr[3, 3] - 
  1/6 F[1, 1, 2] tcr[3, 3] - 1/6 F[1, 1, 6] tcr[3, 3] - 
  1/6 tci[1, 2] tcr[3, 3]) + 
  Cf^2 (-(9/4) F[1, 1, 1]^2 + 2 F[1, 1, 1]^3 - 1/3 F[1, 1, 1]^4 - 
  21/16 tci[1, 1]^2 + 5 F[1, 1, 1] tci[1, 1]^2 - 
  5/3 F[1, 1, 1]^2 tci[1, 1]^2 - 121/240 tci[1, 1]^4 + 
  9/2 F[1, 1, 1] tci[1, 2] - 6 F[1, 1, 1]^2 tci[1, 2] + 
  4/3 F[1, 1, 1]^3 tci[1, 2] - 5/2 tci[1, 1]^2 tci[1, 2] + 
  5/3 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] - 9/8 tci[1, 2]^2 + 
  3/2 F[1, 1, 1] tci[1, 2]^2 - 1/2 F[1, 1, 1]^2 tci[1, 2]^2 + 
  tcr[3, 3] - 2/3 F[1, 1, 1] tcr[3, 3] + 2/3 tci[1, 2] tcr[3, 3]) + 
  Ca (nf Tr (5/6 F[1, 1, 1] - 47/72 F[1, 1, 1]^2 + 1/12 F[1, 1, 1]^3 - 
  167/108 F[1, 1, 2] - 1/8 F[1, 1, 1] F[1, 1, 2] + 
  1/24 F[1, 1, 1]^2 F[1, 1, 2] + 17/16 F[1, 1, 2]^2 + 
  1/24 F[1, 1, 1] F[1, 1, 2]^2 - 13/72 F[1, 1, 2]^3 - 
  167/108 F[1, 1, 6] - 1/8 F[1, 1, 1] F[1, 1, 6] + 
  1/24 F[1, 1, 1]^2 F[1, 1, 6] + 5/18 F[1, 1, 2] F[1, 1, 6] - 
  1/12 F[1, 1, 2]^2 F[1, 1, 6] + 17/16 F[1, 1, 6]^2 + 
  1/24 F[1, 1, 1] F[1, 1, 6]^2 - 1/12 F[1, 1, 2] F[1, 1, 6]^2 - 
  13/72 F[1, 1, 6]^3 - 277/432 tci[1, 1]^2 + 
  19/72 F[1, 1, 1] tci[1, 1]^2 - 1/72 F[1, 1, 2] tci[1, 1]^2 - 
  1/72 F[1, 1, 6] tci[1, 1]^2 - 5/6 tci[1, 2] + 
  47/36 F[1, 1, 1] tci[1, 2] - 1/4 F[1, 1, 1]^2 tci[1, 2] + 
  1/8 F[1, 1, 2] tci[1, 2] - 
  1/12 F[1, 1, 1] F[1, 1, 2] tci[1, 2] - 
  1/24 F[1, 1, 2]^2 tci[1, 2] + 1/8 F[1, 1, 6] tci[1, 2] - 
  1/12 F[1, 1, 1] F[1, 1, 6] tci[1, 2] - 
  1/24 F[1, 1, 6]^2 tci[1, 2] - 7/72 tci[1, 1]^2 tci[1, 2] - 
  1/36 tcr[3, 3]) + 
  Cf (67/12 F[1, 1, 1] - 161/72 F[1, 1, 1]^2 - 7/6 F[1, 1, 1]^3 + 
  7/24 F[1, 1, 1]^4 - 5/4 F[1, 1, 1] F[1, 1, 2] + 
  19/24 F[1, 1, 1]^2 F[1, 1, 2] - 1/12 F[1, 1, 1]^3 F[1, 1, 2] - 
  5/8 F[1, 1, 2]^2 + 19/24 F[1, 1, 1] F[1, 1, 2]^2 - 
  1/8 F[1, 1, 1]^2 F[1, 1, 2]^2 + 19/72 F[1, 1, 2]^3 - 
  1/12 F[1, 1, 1] F[1, 1, 2]^3 - 1/48 F[1, 1, 2]^4 - 
  5/4 F[1, 1, 1] F[1, 1, 6] + 19/24 F[1, 1, 1]^2 F[1, 1, 6] - 
  1/12 F[1, 1, 1]^3 F[1, 1, 6] - 5/8 F[1, 1, 6]^2 + 
  19/24 F[1, 1, 1] F[1, 1, 6]^2 - 
  1/8 F[1, 1, 1]^2 F[1, 1, 6]^2 + 19/72 F[1, 1, 6]^3 - 
  1/12 F[1, 1, 1] F[1, 1, 6]^3 - 1/48 F[1, 1, 6]^4 - 
  1535/432 tci[1, 1]^2 - 259/144 F[1, 1, 1] tci[1, 1]^2 + 
  5/4 F[1, 1, 1]^2 tci[1, 1]^2 + 19/18 F[1, 1, 2] tci[1, 1]^2 - 
  1/3 F[1, 1, 1] F[1, 1, 2] tci[1, 1]^2 - 
  1/6 F[1, 1, 2]^2 tci[1, 1]^2 + 19/18 F[1, 1, 6] tci[1, 1]^2 - 
  1/3 F[1, 1, 1] F[1, 1, 6] tci[1, 1]^2 - 
  1/6 F[1, 1, 6]^2 tci[1, 1]^2 + (409 tci[1, 1]^4)/1440 - 
  67/12 tci[1, 2] + 161/36 F[1, 1, 1] tci[1, 2] + 
  7/2 F[1, 1, 1]^2 tci[1, 2] - 7/6 F[1, 1, 1]^3 tci[1, 2] + 
  5/4 F[1, 1, 2] tci[1, 2] - 
  19/12 F[1, 1, 1] F[1, 1, 2] tci[1, 2] + 
  1/4 F[1, 1, 1]^2 F[1, 1, 2] tci[1, 2] - 
  19/24 F[1, 1, 2]^2 tci[1, 2] + 
  1/4 F[1, 1, 1] F[1, 1, 2]^2 tci[1, 2] + 
  1/12 F[1, 1, 2]^3 tci[1, 2] + 5/4 F[1, 1, 6] tci[1, 2] - 
  19/12 F[1, 1, 1] F[1, 1, 6] tci[1, 2] + 
  1/4 F[1, 1, 1]^2 F[1, 1, 6] tci[1, 2] - 
  19/24 F[1, 1, 6]^2 tci[1, 2] + 
  1/4 F[1, 1, 1] F[1, 1, 6]^2 tci[1, 2] + 
  1/12 F[1, 1, 6]^3 tci[1, 2] + 139/144 tci[1, 1]^2 tci[1, 2] - 
  7/6 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] + 
  1/6 F[1, 1, 2] tci[1, 1]^2 tci[1, 2] + 
  1/6 F[1, 1, 6] tci[1, 1]^2 tci[1, 2] + 9/8 tci[1, 2]^2 - 
  3/2 F[1, 1, 1] tci[1, 2]^2 + 1/2 F[1, 1, 1]^2 tci[1, 2]^2 + 
  47/72 tcr[3, 3] - 1/3 F[1, 1, 2] tcr[3, 3] - 
  1/3 F[1, 1, 6] tcr[3, 3]));

(* qg channel *)

I1["qg"] = (-(Ca/2) - Cf)/ep^2 - 1/6 nf Tr F[1, 1, 1] - 1/6 nf Tr F[1, 1, 2] + 
  Cf (3/2 F[1, 1, 6] - 1/2 F[1, 1, 6]^2 - 1/12 tci[1, 1]^2) + 
  ((nf Tr)/3 + Cf (-(3/2) + F[1, 1, 6]) + 
  Ca (-(11/12) + 1/2 F[1, 1, 1] + 1/2 F[1, 1, 2] - 1/2 F[1, 1, 6] - 1/2 tci[1, 2]))/ep + 
  1/6 nf Tr tci[1, 2] + Ca (5/6 F[1, 1, 1] - 1/4 F[1, 1, 1]^2 + 5/6 F[1, 1, 2] - 
  1/4 F[1, 1, 2]^2 - 3/4 F[1, 1, 6] + 1/4 F[1, 1, 6]^2 - 7/24 tci[1, 1]^2 - 5/6 tci[1, 2] + 1/2 F[1, 1, 1] tci[1, 2]) + 
  ep (1/12 nf Tr F[1, 1, 1]^2 + 1/12 nf Tr F[1, 1, 2]^2 + 1/9 nf Tr tci[1, 1]^2 - 1/6 nf Tr F[1, 1, 1] tci[1, 2] + 
  Ca (-(5/12) F[1, 1, 1]^2 + 1/12 F[1, 1, 1]^3 - 
  5/12 F[1, 1, 2]^2 + 1/12 F[1, 1, 2]^3 + 3/8 F[1, 1, 6]^2 - 
  1/12 F[1, 1, 6]^3 - 71/144 tci[1, 1]^2 + 
  7/24 F[1, 1, 1] tci[1, 1]^2 + 1/24 F[1, 1, 2] tci[1, 1]^2 - 
  1/24 F[1, 1, 6] tci[1, 1]^2 + 5/6 F[1, 1, 1] tci[1, 2] - 
  1/4 F[1, 1, 1]^2 tci[1, 2] - 1/8 tci[1, 1]^2 tci[1, 2] + 
  1/6 tcr[3, 3]) + Cf (-(3/4) F[1, 1, 6]^2 + 1/6 F[1, 1, 6]^3 - 1/8 tci[1, 1]^2 + 
  1/12 F[1, 1, 6] tci[1, 1]^2 + 1/3 tcr[3, 3])) + 
  ep^2 (-(1/36) nf Tr F[1, 1, 1]^3 - 1/36 nf Tr F[1, 1, 2]^3 - 7/72 nf Tr F[1, 1, 1] tci[1, 1]^2 - 
  1/72 nf Tr F[1, 1, 2] tci[1, 1]^2 + 1/12 nf Tr F[1, 1, 1]^2 tci[1, 2] + 
  1/24 nf Tr tci[1, 1]^2 tci[1, 2] - 1/9 nf Tr tcr[3, 3] + 
  Cf (1/4 F[1, 1, 6]^3 - 1/24 F[1, 1, 6]^4 + 
  1/8 F[1, 1, 6] tci[1, 1]^2 - 1/24 F[1, 1, 6]^2 tci[1, 1]^2 - 
  tci[1, 1]^4/1440 + 1/2 tcr[3, 3] - 1/3 F[1, 1, 6] tcr[3, 3]) + 
  Ca (5/36 F[1, 1, 1]^3 - 1/48 F[1, 1, 1]^4 + 5/36 F[1, 1, 2]^3 - 
  1/48 F[1, 1, 2]^4 - 1/8 F[1, 1, 6]^3 + 1/48 F[1, 1, 6]^4 + 
  35/72 F[1, 1, 1] tci[1, 1]^2 - 7/48 F[1, 1, 1]^2 tci[1, 1]^2 + 
  5/72 F[1, 1, 2] tci[1, 1]^2 - 1/48 F[1, 1, 2]^2 tci[1, 1]^2 - 
  1/16 F[1, 1, 6] tci[1, 1]^2 + 1/48 F[1, 1, 6]^2 tci[1, 1]^2 - 
  (121 tci[1, 1]^4)/2880 - 5/12 F[1, 1, 1]^2 tci[1, 2] + 
  1/12 F[1, 1, 1]^3 tci[1, 2] - 5/24 tci[1, 1]^2 tci[1, 2] + 
  1/8 F[1, 1, 1] tci[1, 1]^2 tci[1, 2] + 11/36 tcr[3, 3] - 
  1/6 F[1, 1, 1] tcr[3, 3] - 1/6 F[1, 1, 2] tcr[3, 3] + 
  1/6 F[1, 1, 6] tcr[3, 3] + 1/6 tci[1, 2] tcr[3, 3]));

I2["qg"] = (-(Ca^2/8) - (Ca Cf)/2 - Cf^2/2)/ep^4 + (-(1/12) Ca nf Tr - 
  (Cf nf Tr)/6 + Cf^2 (-(3/2) + F[1, 1, 6]) + 
  Ca Cf (-(7/24) + 1/2 F[1, 1, 1] + 1/2 F[1, 1, 2] - 1/2 tci[1, 2]) + 
  Ca^2 (11/48 + 1/4 F[1, 1, 1] + 1/4 F[1, 1, 2] - 1/4 F[1, 1, 6] - 1/4 tci[1, 2]))/ep^3 + 
  nf^2 Tr^2 (-(7/72) F[1, 1, 1]^2 - 7/72 F[1, 1, 2]^2 - 
  1/12 tci[1, 1]^2 + F[1, 1, 2] (5/27 + 1/36 tci[1, 2]) + 
  F[1, 1, 1] (5/27 - 1/36 F[1, 1, 2] + 7/36 tci[1, 2]) - 
  5/27 tci[1, 2] - 1/72 tci[1, 2]^2) + 
  1/ep^2 ((nf^2 Tr^2)/18 + Cf^2 (-(9/8) + 3 F[1, 1, 6] - F[1, 1, 6]^2 - 1/12 tci[1, 1]^2) + 
  Ca Cf (-(67/72) - 1/4 F[1, 1, 1]^2 - 1/4 F[1, 1, 2]^2 + 
  F[1, 1, 2] (19/12 - 1/2 F[1, 1, 6]) + 1/2 F[1, 1, 6]^2 - 
  3/8 tci[1, 1]^2 + F[1, 1, 6] (-(3/4) + 1/2 tci[1, 2]) + 
  F[1, 1, 1] (19/12 - 1/2 F[1, 1, 6] + 1/2 tci[1, 2]) - 
  19/12 tci[1, 2]) + Ca nf Tr (-(1/6) - 1/12 F[1, 1, 1] - 1/12 F[1, 1, 2] + 
  1/12 tci[1, 2]) + Cf nf Tr (5/18 - 1/6 F[1, 1, 1] - 1/6 F[1, 1, 2] + 
  1/6 tci[1, 2]) + Ca^2 (-(13/288) - 1/4 F[1, 1, 1]^2 - 1/4 F[1, 1, 2]^2 - 
  1/6 tci[1, 1]^2 + F[1, 1, 6] (-(3/8) - 1/4 tci[1, 2]) + 
  F[1, 1, 2] (5/12 + 1/4 F[1, 1, 6] + 1/4 tci[1, 2]) + 
  F[1, 1, 1] (5/12 - 1/4 F[1, 1, 2] + 1/4 F[1, 1, 6] + 
  1/2 tci[1, 2]) - 5/12 tci[1, 2] - 1/8 tci[1, 2]^2)) + 
  Cf nf Tr (-(1/36) F[1, 1, 1]^3 - 1/36 F[1, 1, 2]^3 + 
  F[1, 1, 2]^2 (1/8 - 1/12 F[1, 1, 6]) - 1/6 F[1, 1, 6]^3 + 
  F[1, 1, 2] (1/4 F[1, 1, 6] - 1/12 F[1, 1, 6]^2 - 
  1/36 tci[1, 1]^2) + F[1, 1, 6] (-(5/3) - 1/9 tci[1, 1]^2 - 1/4 tci[1, 2]) + 
  F[1, 1, 1] (-(1/12) F[1, 1, 6]^2 - 1/9 tci[1, 1]^2 + 
  F[1, 1, 6] (1/4 + 1/6 tci[1, 2]) - 1/4 tci[1, 2]) + 
  tci[1, 1]^2 (41/216 + 1/18 tci[1, 2]) + 
  F[1, 1, 6]^2 (47/36 + 1/12 tci[1, 2]) + 
  F[1, 1, 1]^2 (1/8 - 1/12 F[1, 1, 6] + 1/12 tci[1, 2]) - 1/18 tcr[3, 3]) + 
  Ca nf Tr (-(13/72) F[1, 1, 1]^3 - 13/72 F[1, 1, 2]^3 + 1/12 F[1, 1, 6]^3 + 
  F[1, 1, 2] (-(167/108) - 1/8 F[1, 1, 6] + 1/24 F[1, 1, 6]^2 - 
  5/36 tci[1, 1]^2 - 5/18 tci[1, 2]) + 
  F[1, 1, 6]^2 (-(47/72) - 1/24 tci[1, 2]) + 
  F[1, 1, 2]^2 (17/16 + 1/24 F[1, 1, 6] + 1/12 tci[1, 2]) + 
  F[1, 1, 6] (5/6 + 1/18 tci[1, 1]^2 + 1/8 tci[1, 2]) + 
  tci[1, 1]^2 (101/108 + 17/72 tci[1, 2]) + 
  F[1, 1, 1]^2 (17/16 - 1/12 F[1, 1, 2] + 1/24 F[1, 1, 6] + 
  13/24 tci[1, 2]) + 167/108 tci[1, 2] + 5/36 tci[1, 2]^2 + 
  F[1, 1, 1] (-(167/108) - 1/12 F[1, 1, 2]^2 + 1/24 F[1, 1, 6]^2 - 
  31/72 tci[1, 1]^2 + F[1, 1, 6] (-(1/8) - 1/12 tci[1, 2]) + 
  F[1, 1, 2] (5/18 + 1/6 tci[1, 2]) - 17/8 tci[1, 2] - 
  1/6 tci[1, 2]^2) - 1/36 tcr[3, 3]) + 
  Ca^2 (-(1/12) F[1, 1, 1]^4 - 1/12 F[1, 1, 2]^4 - 1/16 F[1, 1, 6]^4 - 
  77/720 tci[1, 1]^4 + F[1, 1, 2]^2 (-(349/144) - 19/48 F[1, 1, 6] + 1/16 F[1, 1, 6]^2 - 
  7/48 tci[1, 1]^2 - 5/12 tci[1, 2]) + F[1, 1, 6]^3 (1/12 - 1/24 tci[1, 2]) + 
  F[1, 1, 2]^3 (103/144 + 1/24 F[1, 1, 6] + 1/24 tci[1, 2]) + 
  F[1, 1, 1]^3 (103/144 - 1/24 F[1, 1, 2] + 1/24 F[1, 1, 6] + 1/3 tci[1, 2]) + 
  F[1, 1, 6]^2 (121/72 + 5/48 tci[1, 1]^2 + 19/48 tci[1, 2]) - 25/72 tci[1, 2]^2 + 
  F[1, 1, 1]^2 (-(349/144) - 1/16 F[1, 1, 2]^2 + 
  1/16 F[1, 1, 6]^2 - 1/3 tci[1, 1]^2 + F[1, 1, 6] (-(19/48) - 1/8 tci[1, 2]) + 
  F[1, 1, 2] (5/12 + 1/8 tci[1, 2]) - 103/48 tci[1, 2] - 1/4 tci[1, 2]^2) + 
  tci[1, 1]^2 (-(3655/1728) - 277/288 tci[1, 2] - 1/16 tci[1, 2]^2) + 
  F[1, 1, 2] (335/108 - 19/48 F[1, 1, 6]^2 + 1/24 F[1, 1, 6]^3 + 
  F[1, 1, 6] (5/8 + 1/24 tci[1, 1]^2) + 
  tci[1, 1]^2 (191/288 + 1/12 tci[1, 2]) + 25/36 tci[1, 2] - 1/6 tcr[3, 3]) + 
  F[1, 1, 1] (335/108 - 1/24 F[1, 1, 2]^3 + 1/24 F[1, 1, 6]^3 + 
  F[1, 1, 2] (-(25/36) - 1/6 tci[1, 1]^2 - 5/6 tci[1, 2]) + 
  F[1, 1, 6]^2 (-(19/48) - 1/8 tci[1, 2]) + F[1, 1, 2]^2 (5/12 + 1/8 tci[1, 2]) + 
  tci[1, 1]^2 (449/288 + 1/2 tci[1, 2]) + F[1, 1, 6] (5/8 + 1/6 tci[1, 1]^2 + 19/24 tci[1, 2]) + 
  349/72 tci[1, 2] + 5/6 tci[1, 2]^2 - 1/6 tcr[3, 3]) + 
  tci[1, 2] (-(335/108) + 1/6 tcr[3, 3]) + 
  F[1, 1, 6] (-(67/24) + tci[1, 1]^2 (-(179/288) - 1/12 tci[1, 2]) - 5/8 tci[1, 2] + 
  1/6 tcr[3, 3]) + 11/144 tcr[3, 3]) + 
  Cf^2 (2 F[1, 1, 6]^3 - 1/3 F[1, 1, 6]^4 - 3/16 tci[1, 1]^2 - 
  1/240 tci[1, 1]^4 + F[1, 1, 6]^2 (-(9/4) - 1/6 tci[1, 1]^2) + 
  F[1, 1, 6] (1/2 tci[1, 1]^2 - 2/3 tcr[3, 3]) + tcr[3, 3]) + 
  Ca Cf (-(1/48) F[1, 1, 1]^4 - 1/48 F[1, 1, 2]^4 + 
  F[1, 1, 2]^3 (19/72 - 1/12 F[1, 1, 6]) + 7/24 F[1, 1, 6]^4 - 
  (101 tci[1, 1]^4)/1440 + F[1, 1, 2]^2 (-(5/8) + 19/24 F[1, 1, 6] - 1/8 F[1, 1, 6]^2 - 1/24 tci[1, 1]^2) + 
  F[1, 1, 6]^2 (-(161/72) - 1/8 tci[1, 1]^2 - 19/24 tci[1, 2]) + 
  F[1, 1, 1]^2 (-(5/8) - 1/8 F[1, 1, 6]^2 - 1/6 tci[1, 1]^2 + 
  F[1, 1, 6] (19/24 + 1/4 tci[1, 2]) - 19/24 tci[1, 2]) + 
  tci[1, 1]^2 (-(353/432) - 19/36 tci[1, 2]) + 
  F[1, 1, 6]^3 (-(7/6) + 1/12 tci[1, 2]) + 
  F[1, 1, 1]^3 (19/72 - 1/12 F[1, 1, 6] + 1/12 tci[1, 2]) + 
  F[1, 1, 6] (67/12 + tci[1, 1]^2 (143/144 + 1/6 tci[1, 2]) + 
  5/4 tci[1, 2]) + F[1, 1, 2] (19/24 F[1, 1, 6]^2 - 1/12 F[1, 1, 6]^3 + 
  19/72 tci[1, 1]^2 + F[1, 1, 6] (-(5/4) - 1/12 tci[1, 1]^2) - 
  1/3 tcr[3, 3]) + F[1, 1, 1] (-(1/12) F[1, 1, 6]^3 + 
  F[1, 1, 6] (-(5/4) - 1/3 tci[1, 1]^2 - 19/12 tci[1, 2]) + 
  tci[1, 1]^2 (19/18 + 1/6 tci[1, 2]) + 
  F[1, 1, 6]^2 (19/24 + 1/4 tci[1, 2]) + 5/4 tci[1, 2] - 
  1/3 tcr[3, 3]) + 47/72 tcr[3, 3] + 1/3 tci[1, 2] tcr[3, 3]) + 
  1/ep (nf^2 Tr^2 (1/18 F[1, 1, 1] + 
  1/18 F[1, 1, 2] - 1/18 tci[1, 2]) + 
  Cf nf Tr (23/27 + 1/12 F[1, 1, 1]^2 + 
  1/12 F[1, 1, 2]^2 + F[1, 1, 2] (-(1/4) + 1/6 F[1, 1, 6]) + 
  1/6 F[1, 1, 6]^2 + tci[1, 1]^2/18 + 
  F[1, 1, 6] (-(19/18) - 1/6 tci[1, 2]) + 
  F[1, 1, 1] (-(1/4) + 1/6 F[1, 1, 6] - 1/6 tci[1, 2]) + 
  1/4 tci[1, 2]) + Ca nf Tr (16/27 + 5/24 F[1, 1, 1]^2 + 
  5/24 F[1, 1, 2]^2 - 1/12 F[1, 1, 6]^2 + tci[1, 1]^2/6 + 
  F[1, 1, 1] (-(17/24) + 1/6 F[1, 1, 2] - 1/12 F[1, 1, 6] - 
  5/12 tci[1, 2]) + F[1, 1, 2] (-(17/24) - 1/12 F[1, 1, 6] - 1/6 tci[1, 2]) + 
  F[1, 1, 6] (19/36 + 1/12 tci[1, 2]) + 17/24 tci[1, 2] + 1/12 tci[1, 2]^2) + 
  Ca^2 (-173/108 + 1/6 F[1, 1, 1]^3 + 
  1/6 F[1, 1, 2]^3 + 1/12 F[1, 1, 6]^3 + 
  F[1, 1, 6] (-(233/144) - 5/24 tci[1, 1]^2 - 19/24 tci[1, 2]) + 
  F[1, 1, 1]^2 (-(41/48) + 1/8 F[1, 1, 2] - 1/8 F[1, 1, 6] - 
  1/2 tci[1, 2]) + tci[1, 1]^2 (-53/96 - 1/4 tci[1, 2]) + 
  F[1, 1, 2]^2 (-(41/48) - 1/8 F[1, 1, 6] - 1/8 tci[1, 2]) + 
  F[1, 1, 6]^2 (1/24 + 1/8 tci[1, 2]) + 
  F[1, 1, 2] (61/36 + 19/24 F[1, 1, 6] - 1/8 F[1, 1, 6]^2 + 
  5/24 tci[1, 1]^2 + 5/6 tci[1, 2]) - 61/36 tci[1, 2] - 
  5/12 tci[1, 2]^2 + F[1, 1, 1] (61/36 + 1/8 F[1, 1, 2]^2 - 1/8 F[1, 1, 6]^2 + 
  1/3 tci[1, 1]^2 + F[1, 1, 2] (-(5/6) - 1/4 tci[1, 2]) + 
  F[1, 1, 6] (19/24 + 1/4 tci[1, 2]) + 41/24 tci[1, 2] + 
  1/4 tci[1, 2]^2) + 5/24 tcr[3, 3]) + Cf^2 (-3/16 - 3 F[1, 1, 6]^2 + 
  2/3 F[1, 1, 6]^3 - tci[1, 1]^2/2 + F[1, 1, 6] (9/4 + 1/6 tci[1, 1]^2) -8/3 tcr[3, 3]) + 
  Ca Cf (-961/432 + 1/12 F[1, 1, 1]^3 + 1/12 F[1, 1, 2]^3 + 
  F[1, 1, 2]^2 (-(19/24) + 1/4 F[1, 1, 6]) - 1/2 F[1, 1, 6]^3 + 
  F[1, 1, 2] (5/4 - 19/12 F[1, 1, 6] + 1/4 F[1, 1, 6]^2 + 
  1/12 tci[1, 1]^2) + F[1, 1, 6]^2 (17/12 - 1/4 tci[1, 2]) + 
  F[1, 1, 1]^2 (-(19/24) + 1/4 F[1, 1, 6] - 1/4 tci[1, 2]) + 
  tci[1, 1]^2 (-(311/288) + 23/96 - 1/6 tci[1, 2]) - 
  5/4 tci[1, 2] + F[1, 1, 6] (19/9 + 1/3 tci[1, 1]^2 + 19/12 tci[1, 2]) + 
  F[1, 1, 1] (5/4 + 1/4 F[1, 1, 6]^2 + 1/3 tci[1, 1]^2 + 
  F[1, 1, 6] (-(19/12) - 1/2 tci[1, 2]) + 
  19/12 tci[1, 2]) + (1/3 + 13/4) tcr[3, 3]));

(* gqb channel *)

I1["gqb"] = (-Ca/2 - Cf)/ep^2 + Cf*((3*F[1, 1, 2])/2 - F[1, 1, 2]^2/2 - tci[1, 1]^2/12) + 
  ((nf*Tr)/3 + Cf*(-3/2 + F[1, 1, 2]) + Ca*(-11/12 + F[1, 1, 1]/2 - F[1, 1, 2]/2 + F[1, 1, 6]/2 - tci[1, 2]/2))/ep + 
  Ca*(-F[1, 1, 1]^2/4 - (3*F[1, 1, 2])/4 + F[1, 1, 2]^2/4 + 
  (5*F[1, 1, 6])/6 - F[1, 1, 6]^2/4 - (7*tci[1, 1]^2)/24 + 
  F[1, 1, 1]*(5/6 + tci[1, 2]/2) - (5*tci[1, 2])/6) + nf*Tr*(-F[1, 1, 1]/6 - F[1, 1, 6]/6 + tci[1, 2]/6) + 
  ep*(nf*Tr*(F[1, 1, 1]^2/12 + F[1, 1, 6]^2/12 + tci[1, 1]^2/9 - 
  (F[1, 1, 1]*tci[1, 2])/6) + Ca*(F[1, 1, 1]^3/12 + (3*F[1, 1, 2]^2)/8 - 
  F[1, 1, 2]^3/12 - (5*F[1, 1, 6]^2)/12 + F[1, 1, 6]^3/12 - 
  (F[1, 1, 2]*tci[1, 1]^2)/24 + (F[1, 1, 6]*tci[1, 1]^2)/24 + F[1, 1, 1]^2*(-5/12 - tci[1, 2]/4) + 
  tci[1, 1]^2*(-71/144 + tci[1, 2]/8) + F[1, 1, 1]*((7*tci[1, 1]^2)/24 + 
  (5*tci[1, 2])/6) - tci[1, 2]^3/4 + tcr[3, 3]/6) + Cf*((-3*F[1, 1, 2]^2)/4 + F[1, 1, 2]^3/6 + 
  (F[1, 1, 2]*tci[1, 1]^2)/12 + tci[1, 1]^2*(-1/8 - tci[1, 2]/4) + tci[1, 2]^3/4 + tcr[3, 3]/3)) + 
  ep^2*(nf*Tr*(-F[1, 1, 1]^3/36 - F[1, 1, 6]^3/36 - (7*F[1, 1, 1]*tci[1, 1]^2)/
  72 - (F[1, 1, 6]*tci[1, 1]^2)/72 + (F[1, 1, 1]^2*tci[1, 2])/12 + 
  tci[1, 2]^3/24 - tcr[3, 3]/9) + Cf*(F[1, 1, 2]^3/4 - F[1, 1, 2]^4/24 - 
  (F[1, 1, 2]^2*tci[1, 1]^2)/24 - tci[1, 1]^4/1440 + (3*tci[1, 2]^3)/8 - 
  tci[1, 2]^4/4 + tci[1, 1]^2*((-3*tci[1, 2])/8 + tci[1, 2]^2/4) + 
  F[1, 1, 2]*(tci[1, 1]^2*(1/8 + tci[1, 2]/4) - tci[1, 2]^3/4 - 
  tcr[3, 3]/3) + tcr[3, 3]/2) + Ca*(-F[1, 1, 1]^4/48 - F[1, 1, 2]^3/8 + 
  F[1, 1, 2]^4/48 + (5*F[1, 1, 6]^3)/36 - F[1, 1, 6]^4/48 + 
  (F[1, 1, 2]^2*tci[1, 1]^2)/48 - (F[1, 1, 6]^2*tci[1, 1]^2)/48 - 
  (121*tci[1, 1]^4)/2880 + F[1, 1, 1]^2*((-7*tci[1, 1]^2)/48 - 
  (5*tci[1, 2])/12) + F[1, 1, 1]^3*(5/36 + tci[1, 2]/12) - 
  (19*tci[1, 2]^3)/48 + tci[1, 2]^4/8 + tci[1, 1]^2*((3*tci[1, 2])/16 - tci[1, 2]^2/8) + 
  F[1, 1, 6]*((5*tci[1, 1]^2)/72 - tcr[3, 3]/6) + F[1, 1, 1]*((35*tci[1, 1]^2)/72 + tci[1, 2]^3/8 - tcr[3, 3]/6) + 
  F[1, 1, 2]*(tci[1, 1]^2*(-1/16 - tci[1, 2]/8) + tci[1, 2]^3/8 + 
  tcr[3, 3]/6) + (11*tcr[3, 3])/36 + (tci[1, 2]*tcr[3, 3])/6));

I2["gqb"] = (-Ca^2/8 - (Ca*Cf)/2 - Cf^2/2)/ep^4 - (7*nf^2*Tr^2*F[1, 1, 1]^2)/72 - 
  (7*nf^2*Tr^2*F[1, 1, 6]^2)/72 - (7*nf^2*Tr^2*tci[1, 1]^2)/72 + 
  (-(Cf*nf*Tr)/6 + Cf^2*(-3/2 + F[1, 1, 2]) + Ca*(-(nf*Tr)/12 + Cf*(-7/24 + F[1, 1, 1]/2 + F[1, 1, 6]/2 - 
  tci[1, 2]/2)) + Ca^2*(11/48 + F[1, 1, 1]/4 - F[1, 1, 2]/4 + 
  F[1, 1, 6]/4 - tci[1, 2]/4))/ep^3 + 
  ((nf^2*Tr^2)/18 + Cf^2*(-9/8 + 3*F[1, 1, 2] - F[1, 1, 2]^2 - 
  tci[1, 1]^2/12) + Ca*(Cf*(-67/72 - F[1, 1, 1]^2/4 + F[1, 1, 2]^2/2 + 
  (19*F[1, 1, 6])/12 - F[1, 1, 6]^2/4 - (3*tci[1, 1]^2)/8 + 
  F[1, 1, 1]*(19/12 - F[1, 1, 2]/2 + tci[1, 2]/2) + 
  F[1, 1, 2]*(-3/4 - F[1, 1, 6]/2 + tci[1, 2]/2) - (19*tci[1, 2])/12) + 
  nf*Tr*(-1/6 - F[1, 1, 1]/12 - F[1, 1, 6]/12 + tci[1, 2]/12)) + 
  Ca^2*(-13/288 - F[1, 1, 1]^2/4 - F[1, 1, 6]^2/4 - (7*tci[1, 1]^2)/24 + 
  F[1, 1, 2]*(-3/8 + F[1, 1, 6]/4 - tci[1, 2]/4) + 
  F[1, 1, 6]*(5/12 + tci[1, 2]/4) + F[1, 1, 1]*(5/12 + F[1, 1, 2]/4 - 
  F[1, 1, 6]/4 + tci[1, 2]/2) - (5*tci[1, 2])/12) + 
  Cf*nf*Tr*(5/18 - F[1, 1, 1]/6 - F[1, 1, 6]/6 + tci[1, 2]/6))/ep^2 - 
  (5*nf^2*Tr^2*tci[1, 2])/27 + F[1, 1, 6]*((5*nf^2*Tr^2)/27 + 
  (nf^2*Tr^2*tci[1, 2])/36) + F[1, 1, 1]*((5*nf^2*Tr^2)/27 - 
  (nf^2*Tr^2*F[1, 1, 6])/36 + (7*nf^2*Tr^2*tci[1, 2])/36) + 
  Cf*nf*Tr*(-F[1, 1, 1]^3/36 - F[1, 1, 2]^3/6 + F[1, 1, 6]^2/8 - 
  F[1, 1, 6]^3/36 - (F[1, 1, 6]*tci[1, 1]^2)/36 + 
  F[1, 1, 2]*(-5/3 + F[1, 1, 6]/4 - F[1, 1, 6]^2/12 - tci[1, 1]^2/9 - 
  tci[1, 2]/4) + F[1, 1, 1]*(-F[1, 1, 2]^2/12 - tci[1, 1]^2/9 + 
  F[1, 1, 2]*(1/4 + tci[1, 2]/6) - tci[1, 2]/4) + 
  tci[1, 1]^2*(41/216 + tci[1, 2]/18) + 
  F[1, 1, 1]^2*(1/8 - F[1, 1, 2]/12 + tci[1, 2]/12) + 
  F[1, 1, 2]^2*(47/36 - F[1, 1, 6]/12 + tci[1, 2]/12) - tcr[3, 3]/18) + 
  Ca^2*(-F[1, 1, 1]^4/12 - F[1, 1, 2]^4/16 - F[1, 1, 6]^4/12 - 
  (61*tci[1, 1]^4)/360 + F[1, 1, 1]^2*(-349/144 + F[1, 1, 2]^2/16 - 
  F[1, 1, 6]^2/16 - (7*tci[1, 1]^2)/12 + 
  F[1, 1, 2]*(-19/48 - tci[1, 2]/8) + F[1, 1, 6]*(5/12 + tci[1, 2]/8) - 
  (103*tci[1, 2])/48) + tci[1, 1]^2*(-4255/1728 - (277*tci[1, 2])/288) + 
  F[1, 1, 6]^2*(-349/144 - (7*tci[1, 1]^2)/48 - (5*tci[1, 2])/12) + 
  F[1, 1, 2]^3*(1/12 + F[1, 1, 6]/24 - tci[1, 2]/24) + 
  F[1, 1, 6]^3*(103/144 + tci[1, 2]/24) + 
  F[1, 1, 1]^3*(103/144 + F[1, 1, 2]/24 - F[1, 1, 6]/24 + tci[1, 2]/3) + 
  F[1, 1, 2]^2*(121/72 - (19*F[1, 1, 6])/48 + F[1, 1, 6]^2/16 + 
  (5*tci[1, 1]^2)/48 + (19*tci[1, 2])/48) + 
  F[1, 1, 6]*(335/108 + tci[1, 1]^2*(191/288 + tci[1, 2]/12) + 
  (25*tci[1, 2])/36 - tcr[3, 3]/6) + 
  F[1, 1, 1]*(335/108 + F[1, 1, 2]^3/24 - F[1, 1, 6]^3/24 + 
  F[1, 1, 6]*(-25/36 - tci[1, 1]^2/6 - (5*tci[1, 2])/6) + 
  F[1, 1, 2]^2*(-19/48 - tci[1, 2]/8) + F[1, 1, 6]^2*
  (5/12 + tci[1, 2]/8) + tci[1, 1]^2*(689/288 + tci[1, 2]/2) + 
  F[1, 1, 2]*(5/8 + tci[1, 1]^2/6 + (19*tci[1, 2])/24) + 
  (349*tci[1, 2])/72 - tcr[3, 3]/6) + tci[1, 2]*(-335/108 + tcr[3, 3]/6) + 
  F[1, 1, 2]*(-67/24 - (19*F[1, 1, 6]^2)/48 + F[1, 1, 6]^3/24 + 
  F[1, 1, 6]*(5/8 + tci[1, 1]^2/24) + tci[1, 1]^2*
  (-179/288 - tci[1, 2]/12) - (5*tci[1, 2])/8 + tcr[3, 3]/6) + 
  (11*tcr[3, 3])/144) + Cf^2*(2*F[1, 1, 2]^3 - F[1, 1, 2]^4/3 - 
  (3*tci[1, 1]^2)/16 - tci[1, 1]^4/240 + 
  F[1, 1, 2]^2*(-9/4 - tci[1, 1]^2/6) + 
  F[1, 1, 2]*(tci[1, 1]^2/2 - (2*tcr[3, 3])/3) + tcr[3, 3]) + 
  Ca*(nf*Tr*((-13*F[1, 1, 1]^3)/72 + F[1, 1, 2]^3/12 - (13*F[1, 1, 6]^3)/72 + 
  F[1, 1, 1]*(-167/108 + F[1, 1, 2]^2/24 - F[1, 1, 6]^2/12 - 
  (43*tci[1, 1]^2)/72 + F[1, 1, 2]*(-1/8 - tci[1, 2]/12) + 
  F[1, 1, 6]*(5/18 + tci[1, 2]/6) - (17*tci[1, 2])/8) + 
  F[1, 1, 6]*(-167/108 - (5*tci[1, 1]^2)/36 - (5*tci[1, 2])/18) + 
  F[1, 1, 2]^2*(-47/72 + F[1, 1, 6]/24 - tci[1, 2]/24) + 
  F[1, 1, 6]^2*(17/16 + tci[1, 2]/12) + 
  F[1, 1, 2]*(5/6 - F[1, 1, 6]/8 + F[1, 1, 6]^2/24 + tci[1, 1]^2/18 + 
  tci[1, 2]/8) + tci[1, 1]^2*(29/27 + (17*tci[1, 2])/72) + 
  F[1, 1, 1]^2*(17/16 + F[1, 1, 2]/24 - F[1, 1, 6]/12 + 
  (13*tci[1, 2])/24) + (167*tci[1, 2])/108 - tcr[3, 3]/36) + 
  Cf*(-F[1, 1, 1]^4/48 + (7*F[1, 1, 2]^4)/24 + (19*F[1, 1, 6]^3)/72 - 
  F[1, 1, 6]^4/48 - (101*tci[1, 1]^4)/1440 + 
  F[1, 1, 6]^2*(-5/8 - tci[1, 1]^2/24) + F[1, 1, 2]^2*
  (-161/72 + (19*F[1, 1, 6])/24 - F[1, 1, 6]^2/8 - tci[1, 1]^2/8 - 
  (19*tci[1, 2])/24) + F[1, 1, 1]^2*(-5/8 - F[1, 1, 2]^2/8 - 
  tci[1, 1]^2/6 + F[1, 1, 2]*(19/24 + tci[1, 2]/4) - 
  (19*tci[1, 2])/24) + tci[1, 1]^2*(-353/432 - (19*tci[1, 2])/36) + 
  F[1, 1, 1]^3*(19/72 - F[1, 1, 2]/12 + tci[1, 2]/12) + 
  F[1, 1, 2]^3*(-7/6 - F[1, 1, 6]/12 + tci[1, 2]/12) + 
  F[1, 1, 2]*(67/12 + (19*F[1, 1, 6]^2)/24 - F[1, 1, 6]^3/12 + 
  F[1, 1, 6]*(-5/4 - tci[1, 1]^2/12) + tci[1, 1]^2*
  (143/144 + tci[1, 2]/6) + (5*tci[1, 2])/4) + 
  F[1, 1, 6]*((19*tci[1, 1]^2)/72 - tcr[3, 3]/3) + 
  F[1, 1, 1]*(-F[1, 1, 2]^3/12 + F[1, 1, 2]*(-5/4 - tci[1, 1]^2/3 - 
  (19*tci[1, 2])/12) + tci[1, 1]^2*(19/18 + tci[1, 2]/6) + 
  F[1, 1, 2]^2*(19/24 + tci[1, 2]/4) + (5*tci[1, 2])/4 - tcr[3, 3]/3) + 
  (47*tcr[3, 3])/72 + (tci[1, 2]*tcr[3, 3])/3)) + 
  ((nf^2*Tr^2*F[1, 1, 1])/18 + (nf^2*Tr^2*F[1, 1, 6])/18 + 
  Cf*nf*Tr*(23/27 + F[1, 1, 1]^2/12 + F[1, 1, 2]^2/6 - F[1, 1, 6]/4 + 
  F[1, 1, 6]^2/12 + tci[1, 1]^2/18 + F[1, 1, 1]*(-1/4 + F[1, 1, 2]/6 - 
  tci[1, 2]/6) + F[1, 1, 2]*(-19/18 + F[1, 1, 6]/6 - tci[1, 2]/6) + 
  tci[1, 2]/4) - (nf^2*Tr^2*tci[1, 2])/18 + 
  Cf^2*(-3/16 - 3*F[1, 1, 2]^2 + (2*F[1, 1, 2]^3)/3 - tci[1, 1]^2/2 + 
  F[1, 1, 2]*(9/4 + tci[1, 1]^2/6) - (8*tcr[3, 3])/3) + 
  Ca^2*(-173/108 + F[1, 1, 1]^3/6 + F[1, 1, 2]^3/12 + F[1, 1, 6]^3/6 + 
  F[1, 1, 2]*(-233/144 + (19*F[1, 1, 6])/24 - F[1, 1, 6]^2/8 - 
  (5*tci[1, 1]^2)/24 - (19*tci[1, 2])/24) + 
  F[1, 1, 1]^2*(-41/48 - F[1, 1, 2]/8 + F[1, 1, 6]/8 - tci[1, 2]/2) + 
  tci[1, 1]^2*(-31/32 - tci[1, 2]/4) + F[1, 1, 6]^2*
  (-41/48 - tci[1, 2]/8) + F[1, 1, 2]^2*(1/24 - F[1, 1, 6]/8 + 
  tci[1, 2]/8) + F[1, 1, 6]*(61/36 + (5*tci[1, 1]^2)/24 + 
  (5*tci[1, 2])/6) - (61*tci[1, 2])/36 + 
  F[1, 1, 1]*(61/36 - F[1, 1, 2]^2/8 + F[1, 1, 6]^2/8 + 
  (7*tci[1, 1]^2)/12 + F[1, 1, 6]*(-5/6 - tci[1, 2]/4) + 
  F[1, 1, 2]*(19/24 + tci[1, 2]/4) + (41*tci[1, 2])/24) + 
  (5*tcr[3, 3])/24) + Ca*(nf*Tr*(16/27 + (5*F[1, 1, 1]^2)/24 - F[1, 1, 2]^2/12 + 
  (5*F[1, 1, 6]^2)/24 + tci[1, 1]^2/4 + F[1, 1, 1]*
  (-17/24 - F[1, 1, 2]/12 + F[1, 1, 6]/6 - (5*tci[1, 2])/12) + 
  F[1, 1, 6]*(-17/24 - tci[1, 2]/6) + F[1, 1, 2]*
  (19/36 - F[1, 1, 6]/12 + tci[1, 2]/12) + (17*tci[1, 2])/24) + 
  Cf*(-961/432 + F[1, 1, 1]^3/12 - F[1, 1, 2]^3/2 - (19*F[1, 1, 6]^2)/24 + 
  F[1, 1, 6]^3/12 + F[1, 1, 6]*(5/4 + tci[1, 1]^2/12) + 
  F[1, 1, 1]^2*(-19/24 + F[1, 1, 2]/4 - tci[1, 2]/4) + 
  F[1, 1, 2]^2*(17/12 + F[1, 1, 6]/4 - tci[1, 2]/4) + 
  tci[1, 1]^2*(-121/144 - tci[1, 2]/6) - (5*tci[1, 2])/4 + 
  F[1, 1, 2]*(19/9 - (19*F[1, 1, 6])/12 + F[1, 1, 6]^2/4 + 
  tci[1, 1]^2/3 + (19*tci[1, 2])/12) + F[1, 1, 1]*
  (5/4 + F[1, 1, 2]^2/4 + tci[1, 1]^2/3 + F[1, 1, 2]*
  (-19/12 - tci[1, 2]/2) + (19*tci[1, 2])/12) + (43*tcr[3, 3])/12)))/ep;
