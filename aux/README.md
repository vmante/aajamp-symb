This folder contains auxiliary files with the definitions of the invariants and abbreviations entering the results for the finite remainders of the helicity amplitudes.
We further provide the explicit expressions of the Catani $`I_1`$ and $`I_2`$ operators for the relevant partonic processes.

definitions.m
---------------
This files contains substitution rules for the various abbreviations used for the denominator factors entering the actual results of the helicity amplitudes.

Catani_I_operators.m
-------------------
This files contains the expressions for the Catani $`I_1`$ and $`I_2`$ operators for the scattering processes $`q\bar{q} \to g \gamma \gamma`$, $`q g \to q \gamma \gamma`$ and $`g \bar{q} \to \bar{q} \gamma \gamma`$. Results are stored in the variables `I1[proc]` and `I2[proc]` with `proc` taking values `"qqb"`, `"qg"` and `"gqb"`.

The expressions provided depend on the following set of variables: 

* `ep` : dimensional regulator defined as `ep = (4-d)/2`
* `Ca` : SU(N) Casimir, `Ca = N`
* `Cf` : SU(N) Casimir, `Cf = (N^2-1)/(2*N)`
* `nf` : number of light quarks
* `Tr` : normalisation of the colour operators in the fundamental representation, $`\mathrm{tr}\left[T^a T^b\right] = T_r \delta^{ab}`$; here `Tr = 1/2`.

All transcendental constants and logarithms are expressed in the notation of the [PentagonMI][1] package. Explicitly:

* `F[1,1,1]  `   : $`-\log(\mu^2/s_{12})`$
* `F[1,1,2]  `   : $`-\log(-\mu^2/s_{23})`$
* `F[1,1,6]  `   : $`-\log(-\mu^2/s_{13})`$
* `tci[1,2]  `   : $`i \pi`$
* `tci[1,1]^2`   : $`-\pi^2`$
* `tcr[3,3]  `   : $`\zeta_3`$

[1]: https://gitlab.com/pentagon-functions/PentagonMI "PentagonMI"