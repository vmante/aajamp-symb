This folder contains a few examples which show explicitly the usage of the analytic results provided in the `aajamp-symb` repository.

In order to be able to run these examples, you need the [PentagonMI][1] package to be installed.
Also, in each file you should provide the installation path of the [PentagonMI][1] package. In particular, in the first line of each demo file change the `"my/path/to/PentagonMI/"` to the one that suits you.

evaluate_1loop_amplitudes.m
---------------------------
This demo file computes the one-loop finite remainders of the independent helicity amplitudes for the scattering processes $`q\bar{q} \to g \gamma \gamma`$, $`q g \to q \gamma \gamma`$ and $`g \bar{q} \to \bar{q} \gamma \gamma`$. For futher details on our definitions of the finite remainders and the choice of independent helicity configurations see [2105.04585][2].

The choice of the kinematics is such to reproduce the one-loop benchmark results in Tab.1 of [2105.04585][2].

evaluate_2loop_amplitudes.m
---------------------------
This demo file computes the full-colour two-loop finite remainders of the independent helicity amplitudes for the scattering processes $`q\bar{q} \to g \gamma \gamma`$, $`q g \to q \gamma \gamma`$ and $`g \bar{q} \to \bar{q} \gamma \gamma`$.

The choice of the kinematics is such to reproduce the full colour two-loop benchmark results in Tab.1 of [2105.04585][2].

qqb_2loop_LC_interference.m
---------------------------
This demo file shows how to use our results for the finite remainders of the two-loop helicity amplitudes to construct the polarisation summed interference with the tree level. The scattering process $`q\bar{q} \to g \gamma \gamma`$ is considered. We focus here on the leading-colour contribution because the purpose is to reproduce the results presented in Tab.2 of [2102.01820][3]. It is straightforward to generalise this example to the full-colour case by extending the `TwoLoopColourFactors` list and include all the colour structures one is interested in.

qg_2loop_LC_interference.m
---------------------------
This demo file is equivalent to the **qqb_2loop_LC_interference.m** one, but the $`q g \to q \gamma \gamma`$ scattering process is considered instead. The main feature of this demo is to show how to use the results for both the $`q g \to q \gamma \gamma`$ and $`g \bar{q} \to \bar{q} \gamma \gamma`$ channels to build up the polarisation summed interference with the tree level.

[1]: https://gitlab.com/pentagon-functions/PentagonMI "PentagonMI"
[2]: https://arxiv.org/abs/2105.04585 "2105.04585"
[3]: https://arxiv.org/abs/2102.01820 "2102.01820"
