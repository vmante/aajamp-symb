PathToPentagonMI = "my/path/to/PentagonMI/";

Needs["PentagonFunctions`"]
Get[PathToPentagonMI<>"PentagonMI/datafiles/constants_numerical.m"];

(* Kinematics *)
(*==================================================================*)
Get["../aux/definitions.m"];
KinematicPoint = {157, -43, 83, 61, -37};
musq = 100.;

Invariants = {s12, s23, s34, s45, s51};

eps5bN = 1/s12^2 I Sqrt[Abs[s12^2 (s23 - s51)^2 + (s23 s34 + s45 (-s34 + s51))^2 +
			    2 s12 (-s23^2 s34 + s23 s45 s51 + s45 (s34 - s51) s51 + s23 s34 (s45 + s51))]] /. Thread[Invariants -> KinematicPoint];

(* Parameters *)
(*==================================================================*)
OLCFs = {Nc, Nc^-1, nfaa, nf};
Parameters = {Nc -> 3, nfaa -> (3*Qd^2 + 2*Qu^2)/Q1^2, nf -> 5} /. {Qu -> 2/3, Qd -> -1/3};

(* Load helicity amplitudes *)
(*==================================================================*)

Channels = {"qqb","qg","gqb"};
OneLoopColourFactors = Table["CF"<>ToString[i],{i,1,4}];
HelicityConfigurations = {LmmpE, LmmpO, LmppE, LmppO, LpppE, LpppO};

Do[
  proc = Channels[[k]];
  Print["Loading results for "<> proc <>"\n"];
  Do[
    Do[
       hel  = HelicityConfigurations[[h]];
       cf   = OneLoopColourFactors[[n]];
       R1[proc][cf][hel] = Get["../helicity_remainders/1loop/R1_"<> proc <> "_" <> cf <> "_" <> ToString[hel] <> ".m"];
       (* Select ep^0 coefficient only *)
       R1[proc][cf][hel] = Coefficient[R1[proc][cf][hel],ep,0],
    {h,1,Length[HelicityConfigurations]}],
  {n,1,Length[OneLoopColourFactors]}],
{k,1,Length[Channels]}];
Clear[proc,cf,hel];

(* Build up helicity amplitudes *)
(*==================================================================*)

Print["Building up qqb"];
R1["qqb"]["+++"] = Table[cf = OneLoopColourFactors[[n]]; R1["qqb"][cf][LpppE] + sgn * eps5b * R1["qqb"][cf][LpppO],{n,1,Length[OneLoopColourFactors]}];
R1["qqb"]["-++"] = Table[cf = OneLoopColourFactors[[n]]; R1["qqb"][cf][LmppE] + sgn * eps5b * R1["qqb"][cf][LmppO],{n,1,Length[OneLoopColourFactors]}];
R1["qqb"]["--+"] = Table[cf = OneLoopColourFactors[[n]]; R1["qqb"][cf][LmmpE] + sgn * eps5b * R1["qqb"][cf][LmmpO],{n,1,Length[OneLoopColourFactors]}];

Print["Building up qg"];
R1["qg"]["+++"] = Table[cf = OneLoopColourFactors[[n]]; R1["qg"][cf][LpppE] + sgn * eps5b * R1["qg"][cf][LpppO],{n,1,Length[OneLoopColourFactors]}];
R1["qg"]["-++"] = Table[cf = OneLoopColourFactors[[n]]; R1["qg"][cf][LmppE] + sgn * eps5b * R1["qg"][cf][LmppO],{n,1,Length[OneLoopColourFactors]}];
R1["qg"]["--+"] = Table[cf = OneLoopColourFactors[[n]]; R1["qg"][cf][LmmpE] + sgn * eps5b * R1["qg"][cf][LmmpO],{n,1,Length[OneLoopColourFactors]}];

Print["Building up gqb"];
R1["gqb"]["+++"] = Table[cf = OneLoopColourFactors[[n]]; R1["gqb"][cf][LpppE] + sgn * eps5b * R1["gqb"][cf][LpppO],{n,1,Length[OneLoopColourFactors]}];
R1["gqb"]["-++"] = Table[cf = OneLoopColourFactors[[n]]; R1["gqb"][cf][LmppE] + sgn * eps5b * R1["gqb"][cf][LmppO],{n,1,Length[OneLoopColourFactors]}];
R1["gqb"]["--+"] = Table[cf = OneLoopColourFactors[[n]]; R1["gqb"][cf][LmmpE] + sgn * eps5b * R1["gqb"][cf][LmmpO],{n,1,Length[OneLoopColourFactors]}];

PentagonFunctionsQQb = Join[Cases[Variables[R1["qqb"]["+++"]],_F], Cases[Variables[R1["qqb"]["-++"]],_F], Cases[Variables[R1["qqb"]["--+"]],_F]] // Union;
PentagonFunctionsQG  = Join[Cases[Variables[R1["qg"]["+++"]],_F],  Cases[Variables[R1["qg"]["-++"]],_F],  Cases[Variables[R1["qg"]["--+"]],_F]] // Union;
PentagonFunctionsGQB = Join[Cases[Variables[R1["gqb"]["+++"]],_F], Cases[Variables[R1["gqb"]["-++"]],_F], Cases[Variables[R1["gqb"]["--+"]],_F]] // Union;

PentagonFunctions = Join[PentagonFunctionsQQb,PentagonFunctionsQG,PentagonFunctionsGQB] // Union;

(* Evaluate Pentagon functions *)
(*==================================================================*)

Print["Evaluate Pentagon functions"];

EvaluatorPF = StartEvaluatorProcess[PentagonFunctions];
PFvalues = EvaluateFunctions[EvaluatorPF, KinematicPoint/musq] // Dispatch;

(* Evaluate helicity amplitudes *)
(*==================================================================*)

ResQQbPPP = R1["qqb"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQQbMPP = R1["qqb"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQQbMMP = R1["qqb"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResQQbPPP = ResQQbPPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQQbMPP = ResQQbMPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQQbMMP = ResQQbMMP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["q qb -> g a a. R1[+++] = ", N[ResQQbPPP,16]];
Print["q qb -> g a a. R1[-++] = ", ResQQbMPP];
Print["q qb -> g a a. R1[--+] = ", ResQQbMMP];

Print[""];

(*==================================================================*)

ResQGPPP = R1["qg"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQGMPP = R1["qg"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQGMMP = R1["qg"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResQGPPP = ResQGPPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQGMPP = ResQGMPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQGMMP = ResQGMMP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["q g -> q a a.  R1[+++] = ", N[ResQGPPP,16]];
Print["q g -> q a a.  R1[-++] = ", ResQGMPP];
Print["q g -> q a a.  R1[--+] = ", ResQGMMP];

Print[""];

(*==================================================================*)

ResGQBPPP = R1["gqb"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResGQBMPP = R1["gqb"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResGQBMMP = R1["gqb"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResGQBPPP = ResGQBPPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResGQBMPP = ResGQBMPP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResGQBMMP = ResGQBMMP.OLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["g qb -> q a a. R1[+++] = ", N[ResGQBPPP,16]];
Print["g qb -> q a a. R1[-++] = ", ResGQBMPP];
Print["g qb -> q a a. R1[--+] = ", ResGQBMMP];
