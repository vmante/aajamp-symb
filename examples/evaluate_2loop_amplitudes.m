PathToPentagonMI = "my/path/to/PentagonMI/";

Needs["PentagonFunctions`"]
Get[PathToPentagonMI<>"PentagonMI/datafiles/constants_numerical.m"];

(* Kinematics *)
(*==================================================================*)
Get["../aux/definitions.m"];
KinematicPoint = {157, -43, 83, 61, -37};
musq = 100.;

Invariants = {s12, s23, s34, s45, s51};

eps5bN = 1/s12^2 I Sqrt[Abs[s12^2 (s23 - s51)^2 + (s23 s34 + s45 (-s34 + s51))^2 +
			    2 s12 (-s23^2 s34 + s23 s45 s51 + s45 (s34 - s51) s51 + s23 s34 (s45 + s51))]] /. Thread[Invariants -> KinematicPoint];

(* Parameters *)
(*==================================================================*)
TLCFs = {Nc^2, 1, Nc^-2, Nc*nf, nf/Nc, nf*nfaa, Nc*nfaa, nfaa/Nc, nfa*(Nc-4/Nc), nf^2};
Parameters = {Nc -> 3, nfa -> (3*Qd + 2*Qu)/Q1, nfaa -> (3*Qd^2 + 2*Qu^2)/Q1^2, nf -> 5} /. {Qu -> 2/3, Qd -> -1/3};

(* Load helicity amplitudes *)
(*==================================================================*)

Channels = {"qqb","qg","gqb"};
TwoLoopColourFactors = Table["CF"<>ToString[i],{i,1,10}];
HelicityConfigurations = {LmmpE, LmmpO, LmppE, LmppO, LpppE, LpppO};

Do[
  proc = Channels[[k]];
  Print["Loading results for "<> proc <>"\n"];
  Do[
    Do[
       hel  = HelicityConfigurations[[h]];
       cf   = TwoLoopColourFactors[[n]];
       R2[proc][cf][hel] = Get["../helicity_remainders/2loop/R2_"<> proc <> "_" <> cf <> "_" <> ToString[hel] <> ".m"],
    {h,1,Length[HelicityConfigurations]}],
  {n,1,Length[TwoLoopColourFactors]}],
{k,1,Length[Channels]}];
Clear[proc,cf,hel];

(* Build up helicity amplitudes *)
(*==================================================================*)

Print["Building up qqb"];
R2["qqb"]["+++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qqb"][cf][LpppE] + sgn * eps5b * R2["qqb"][cf][LpppO],{n,1,Length[TwoLoopColourFactors]}];
R2["qqb"]["-++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qqb"][cf][LmppE] + sgn * eps5b * R2["qqb"][cf][LmppO],{n,1,Length[TwoLoopColourFactors]}];
R2["qqb"]["--+"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qqb"][cf][LmmpE] + sgn * eps5b * R2["qqb"][cf][LmmpO],{n,1,Length[TwoLoopColourFactors]}];

Print["Building up qg"];
R2["qg"]["+++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qg"][cf][LpppE] + sgn * eps5b * R2["qg"][cf][LpppO],{n,1,Length[TwoLoopColourFactors]}];
R2["qg"]["-++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qg"][cf][LmppE] + sgn * eps5b * R2["qg"][cf][LmppO],{n,1,Length[TwoLoopColourFactors]}];
R2["qg"]["--+"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qg"][cf][LmmpE] + sgn * eps5b * R2["qg"][cf][LmmpO],{n,1,Length[TwoLoopColourFactors]}];

Print["Building up gqb"];
R2["gqb"]["+++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["gqb"][cf][LpppE] + sgn * eps5b * R2["gqb"][cf][LpppO],{n,1,Length[TwoLoopColourFactors]}];
R2["gqb"]["-++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["gqb"][cf][LmppE] + sgn * eps5b * R2["gqb"][cf][LmppO],{n,1,Length[TwoLoopColourFactors]}];
R2["gqb"]["--+"] = Table[cf = TwoLoopColourFactors[[n]]; R2["gqb"][cf][LmmpE] + sgn * eps5b * R2["gqb"][cf][LmmpO],{n,1,Length[TwoLoopColourFactors]}];

PentagonFunctionsQQb = Join[Cases[Variables[R2["qqb"]["+++"]],_F], Cases[Variables[R2["qqb"]["-++"]],_F], Cases[Variables[R2["qqb"]["--+"]],_F]] // Union;
PentagonFunctionsQG  = Join[Cases[Variables[R2["qg"]["+++"]],_F],  Cases[Variables[R2["qg"]["-++"]],_F],  Cases[Variables[R2["qg"]["--+"]],_F]] // Union;
PentagonFunctionsGQB = Join[Cases[Variables[R2["gqb"]["+++"]],_F], Cases[Variables[R2["gqb"]["-++"]],_F], Cases[Variables[R2["gqb"]["--+"]],_F]] // Union;

PentagonFunctions = Join[PentagonFunctionsQQb,PentagonFunctionsQG,PentagonFunctionsGQB] // Union;

(* Evaluate Pentagon functions *)
(*==================================================================*)

Print["Evaluate Pentagon functions"];

EvaluatorPF = StartEvaluatorProcess[PentagonFunctions];
PFvalues = EvaluateFunctions[EvaluatorPF, KinematicPoint/musq] // Dispatch;

(* Evaluate helicity amplitudes *)
(*==================================================================*)

ResQQbPPP = R2["qqb"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQQbMPP = R2["qqb"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQQbMMP = R2["qqb"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResQQbPPP = ResQQbPPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQQbMPP = ResQQbMPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQQbMMP = ResQQbMMP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["q qb -> g a a. R2[+++] = ", ResQQbPPP];
Print["q qb -> g a a. R2[-++] = ", ResQQbMPP];
Print["q qb -> g a a. R2[--+] = ", ResQQbMMP];

Print[""];

(*==================================================================*)

ResQGPPP = R2["qg"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQGMPP = R2["qg"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResQGMMP = R2["qg"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResQGPPP = ResQGPPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQGMPP = ResQGMPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResQGMMP = ResQGMMP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["q g -> q a a.  R2[+++] = ", ResQGPPP];
Print["q g -> q a a.  R2[-++] = ", ResQGMPP];
Print["q g -> q a a.  R2[--+] = ", ResQGMMP];

Print[""];

(*==================================================================*)

ResGQBPPP = R2["gqb"]["+++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResGQBMPP = R2["gqb"]["-++"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
ResGQBMMP = R2["gqb"]["--+"] /. sgn -> 1 /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

ResGQBPPP = ResGQBPPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResGQBMPP = ResGQBMPP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;
ResGQBMMP = ResGQBMMP.TLCFs /. Parameters /. Q1 -> 2/3 // Expand;

Print["g qb -> q a a. R2[+++] = ", ResGQBPPP];
Print["g qb -> q a a. R2[-++] = ", ResGQBMPP];
Print["g qb -> q a a. R2[--+] = ", ResGQBMMP];
