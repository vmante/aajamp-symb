PathToPentagonMI = "my/path/to/PentagonMI/";

Needs["PentagonFunctions`"]
Get[PathToPentagonMI<>"PentagonMI/datafiles/constants_numerical.m"];

(* Kinematics *)
(*==================================================================*)
Get["../aux/definitions.m"];

Invariants = {s12, s23, s34, s45, s51};

MomentumConservation = {
   s13 -> -s12 - s23 + s45,
   s14 -> s23 - s45 - s51, 
   s24 -> -s23 - s34 + s51, 
   s25 -> -s12 + s34 - s51, 
   s35 -> s12 - s34 - s45, 
   s15 -> s51};

KinematicPoint = {157, -43, 83, 61, -37};

(* 1 <-> 2: {s12 -> s12, s23 -> s13, s34 -> s34, s45 -> s45, s51 -> s25} *)
KinematicPointx12 = {s12, s13, s34, s45, s25} /. MomentumConservation /. Thread[Invariants -> KinematicPoint];

(* 4 <-> 5: {s12 -> s12, s23 -> s23, s34 -> s35, s45 -> s45, s51 -> s14} *)
KinematicPointx45 = {s12, s23, s35, s45, s14} /. MomentumConservation /. Thread[Invariants -> KinematicPoint];

(* 1 <-> 2 && 4<->5: {s12 -> s12, s23 -> s23, s34 -> s35, s45 -> s45, s51 -> s24} *)
KinematicPointx12x45 = {s12, s13, s35, s45, s24} /. MomentumConservation /. Thread[Invariants -> KinematicPoint];

musq = 100.;

eps5bN = 1/s12^2 I Sqrt[Abs[s12^2 (s23 - s51)^2 + (s23 s34 + s45 (-s34 + s51))^2 +
                        2 s12 (-s23^2 s34 + s23 s45 s51 + s45 (s34 - s51) s51 + s23 s34 (s45 + s51))]];
eps5bN = eps5bN /. Thread[Invariants -> KinematicPoint];

(* Parameters *)
(*==================================================================*)
Parameters = {Nc -> 3, nfa -> (3*Qd + 2*Qu)/Q1, nfaa -> (3*Qd^2 + 2*Qu^2)/Q1^2, nf -> 5} /. {Qu -> 2/3, Qd -> -1/3};

TLCFs = {Nc^2};
(* -- Comment out the following line if interested in the full-colour result -- *)
(* TLCFs = {Nc^2, 1, Nc^-2, Nc*nf, nf/Nc, nf*nfaa, Nc*nfaa, nfaa/Nc, nfa*(Nc-4/Nc), nf^2}; *)

(* Tree-level squared qg *)
(*==================================================================*)

(* Independent helicities *)
W00["qg"]["L-++"] = - 8 (s13 s23^2)/(s14 s34 s35 s51) /. MomentumConservation /. Thread[Invariants -> KinematicPoint];
W00["qg"]["L--+"] = - 8 (s51^2 s13)/(s14 s34 s23 s12) /. MomentumConservation /. Thread[Invariants -> KinematicPoint];

(* Permutation of photons *)
W00["qg"]["L-+-"] = - 8 (s51^2 s13)/(s14 s34 s23 s12) /. MomentumConservation /. Thread[Invariants -> KinematicPointx45];

(* Tree-level squared qbg. Obtained from gqb via x12 permutation *)
(*==================================================================*)

(* Independent helicities *)
W00["qbg"]["L-++"] = - 8 (s23 s12^2)/(s34 s24 s25 s35) /. MomentumConservation /. Thread[Invariants -> KinematicPointx12];
W00["qbg"]["L--+"] = - 8 (s35^2 s23)/(s34 s24 s12 s13) /. MomentumConservation /. Thread[Invariants -> KinematicPointx12];

(* Permutation of photons *)
W00["qbg"]["L-+-"] = - 8 (s35^2 s23)/(s34 s24 s12 s13) /. MomentumConservation /. Thread[Invariants -> KinematicPointx12x45];

(* Parity transformation *)
(*==================================================================*)

W00["qg"]["R+--"] = W00["qg"]["L-++"];
W00["qg"]["R++-"] = W00["qg"]["L--+"];
W00["qg"]["R+-+"] = W00["qg"]["L-+-"];

W00["qbg"]["R+--"] = W00["qbg"]["L-++"];
W00["qbg"]["R++-"] = W00["qbg"]["L--+"];
W00["qbg"]["R+-+"] = W00["qbg"]["L-+-"];

(* Remaining qg amplitudes squared from qbg *)
(*==================================================================*)

W00["qg"]["R-++"] = W00["qbg"]["L-++"];
W00["qg"]["R--+"] = W00["qbg"]["L--+"];
W00["qg"]["R-+-"] = W00["qbg"]["L-+-"];

W00["qg"]["L++-"] = W00["qbg"]["R++-"];
W00["qg"]["L+--"] = W00["qbg"]["R+--"];
W00["qg"]["L+-+"] = W00["qbg"]["R+-+"];

(* Tree-level squared *)
(*==================================================================*)

TreeSquared = {
  W00["qg"]["L-++"],
  W00["qg"]["L--+"],
  W00["qg"]["L-+-"],
  W00["qg"]["L++-"],
  W00["qg"]["L+--"],
  W00["qg"]["L+-+"],
  W00["qg"]["R+--"],
  W00["qg"]["R++-"],
  W00["qg"]["R+-+"],
  W00["qg"]["R-++"],
  W00["qg"]["R--+"],
  W00["qg"]["R-+-"]
};

(* Load 2-loop helicity amplitudes *)
(*==================================================================*)

(* -- Set the following to 10 if interested in the full-colour result -- *)
NumberOfColourFactors = 1;

Channels = {"qg","gqb"};
TwoLoopColourFactors = Table["CF"<>ToString[i],{i,1,NumberOfColourFactors}];
HelicityConfigurations = {LmmpE, LmmpO, LmppE, LmppO};

Do[
  proc = Channels[[k]];
  Print["Loading results for "<> proc <>"\n"];
  Do[
    Do[
       hel  = HelicityConfigurations[[h]];
       cf   = TwoLoopColourFactors[[n]];
       R2[proc][cf][hel] = Get["../helicity_remainders/2loop/R2_"<> proc <> "_" <> cf <> "_" <> ToString[hel] <> ".m"],
    {h,1,Length[HelicityConfigurations]}],
  {n,1,Length[TwoLoopColourFactors]}],
{k,1,Length[Channels]}];
Clear[proc,cf,hel];

(* Build up helicity amplitudes *)
(*==================================================================*)

Print["Building up qg"];
R2["qg"]["L-++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qg"][cf][LmppE] + sgn * eps5b * R2["qg"][cf][LmppO],{n,1,Length[TwoLoopColourFactors]}];
R2["qg"]["L--+"] = Table[cf = TwoLoopColourFactors[[n]]; R2["qg"][cf][LmmpE] + sgn * eps5b * R2["qg"][cf][LmmpO],{n,1,Length[TwoLoopColourFactors]}];

Print["Building up gqb"];
R2["gqb"]["L-++"] = Table[cf = TwoLoopColourFactors[[n]]; R2["gqb"][cf][LmppE] + sgn * eps5b * R2["gqb"][cf][LmppO],{n,1,Length[TwoLoopColourFactors]}];
R2["gqb"]["L--+"] = Table[cf = TwoLoopColourFactors[[n]]; R2["gqb"][cf][LmmpE] + sgn * eps5b * R2["gqb"][cf][LmmpO],{n,1,Length[TwoLoopColourFactors]}];

PentagonFunctionsQG  = Join[Cases[Variables[R2["qg"]["L-++"]],_F], Cases[Variables[R2["qg"]["L--+"]],_F]] // Union;
PentagonFunctionsGQB = Join[Cases[Variables[R2["gqb"]["L-++"]],_F], Cases[Variables[R2["gqb"]["L--+"]],_F]] // Union;

PentagonFunctions = Join[PentagonFunctionsQG,PentagonFunctionsGQB] // Union;

(* Evaluate Pentagon functions *)
(*==================================================================*)

Print["Evaluate Pentagon functions"];

EvaluatorPF  = StartEvaluatorProcess[PentagonFunctions];

PFvalues       = EvaluateFunctions[EvaluatorPF, KinematicPoint/musq]      // Dispatch;
PFvaluesx12    = EvaluateFunctions[EvaluatorPF, KinematicPointx12/musq]   // Dispatch;
PFvaluesx45    = EvaluateFunctions[EvaluatorPF, KinematicPointx45/musq]   // Dispatch;
PFvaluesx12x45 = EvaluateFunctions[EvaluatorPF, KinematicPointx12x45/musq] // Dispatch;

(* Evaluate helicity amplitudes for qg *)
(*==================================================================*)

R2N["qg"]["L-++"] = R2["qg"]["L-++"] /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;
R2N["qg"]["L--+"] = R2["qg"]["L--+"] /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPoint] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvalues;

(* x45: Change sign to the odd component of the helicity amplitude because of odd permutation *)
R2N["qg"]["L-+-"] = R2["qg"]["L--+"] /. sgn -> - sgn /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPointx45] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvaluesx45;

(* Evaluate helicity amplitudes for qbg from gqb via x12 *)
(*==================================================================*)

(* x12: Change sign to the odd component of the helicity amplitude because of odd permutation *)
R2N["qbg"]["L-++"] = R2["gqb"]["L-++"] /. sgn -> - sgn /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPointx12] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvaluesx12;
R2N["qbg"]["L--+"] = R2["gqb"]["L--+"] /. sgn -> - sgn /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPointx12] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvaluesx12;

(* x12x45: odd component of the helicity amplitude does not change sign because of even permutation *)
R2N["qbg"]["L-+-"] = R2["gqb"]["L--+"] /. rdefs /. XsToInvariants /. Thread[Invariants -> KinematicPointx12x45] /. {eps5t -> eps5bN, eps5b -> eps5bN} /. ratio -> Identity /. PFvaluesx12x45;

(* Parity transformations *)
R2N["qg"]["R+--"] = R2N["qg"]["L-++"]   /. sgn -> -sgn;
R2N["qg"]["R++-"] = R2N["qg"]["L--+"]   /. sgn -> -sgn;
R2N["qg"]["R+-+"] = R2N["qg"]["L-+-"]   /. sgn -> -sgn;

R2N["qbg"]["R+--"] = R2N["qbg"]["L-++"] /. sgn -> -sgn;
R2N["qbg"]["R++-"] = R2N["qbg"]["L--+"] /. sgn -> -sgn;
R2N["qbg"]["R+-+"] = R2N["qbg"]["L-+-"] /. sgn -> -sgn;

(* Remaining qg amplitudes from qbg *)
(*==================================================================*)

R2N["qg"]["R-++"] = R2N["qbg"]["L-++"];
R2N["qg"]["R--+"] = R2N["qbg"]["L--+"];
R2N["qg"]["R-+-"] = R2N["qbg"]["L-+-"];

R2N["qg"]["L++-"] = R2N["qbg"]["R++-"];
R2N["qg"]["L+--"] = R2N["qbg"]["R+--"];
R2N["qg"]["L+-+"] = R2N["qbg"]["R+-+"];

Print[""];

(*==================================================================*)

AllR2s = {
  R2N["qg"]["L-++"].TLCFs,
  R2N["qg"]["L--+"].TLCFs,
  R2N["qg"]["L-+-"].TLCFs,
  R2N["qg"]["L++-"].TLCFs,
  R2N["qg"]["L+--"].TLCFs,
  R2N["qg"]["L+-+"].TLCFs,
  R2N["qg"]["R+--"].TLCFs,
  R2N["qg"]["R++-"].TLCFs,
  R2N["qg"]["R+-+"].TLCFs,
  R2N["qg"]["R-++"].TLCFs,
  R2N["qg"]["R--+"].TLCFs,
  R2N["qg"]["R-+-"].TLCFs
} /. sgn -> +1 // Expand;

(*==================================================================*)
(* Compare against Table 2. of 2102.01820.
   Divide by factor 2 to match normalisation of tree-loop interference.
   The factor 2 comes Tr[T^a T^a] = (Nc^2-1)/2 from the external gluon and the factor
   2 has been readsorbed in the interference in 2102.01820.
   Remove a factor Nc^2 in order to reproduce the result in 2102.01820. 
   Undo it if the physical result is needed.
*)
Print["Tree-Two-loop interference LC: ", Expand[(AllR2s.TreeSquared)/2/Nc^2 /. Nc -> 3]];
