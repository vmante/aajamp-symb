(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

F[1, 1, 1]/12 + F[1, 1, 2]/12 - tci[1, 2]/12 + 
 ep*(-F[1, 1, 1]^2/24 - F[1, 1, 2]^2/24 - tci[1, 1]^2/18 + 
   (F[1, 1, 1]*tci[1, 2])/12) + ep^2*(F[1, 1, 1]^3/72 + F[1, 1, 2]^3/72 + 
   (7*F[1, 1, 1]*tci[1, 1]^2)/144 + (F[1, 1, 2]*tci[1, 1]^2)/144 - 
   (F[1, 1, 1]^2*tci[1, 2])/24 - tci[1, 2]^3/48 + tcr[3, 3]/18)
