(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

 (  + 1/12*F[1,1,2] - 1/24*F[1,1,2]^2*ep + 1/72*F[1,1,2]^3*ep^2 + 1/144
         *F[1,1,2]*tci[1,1]^2*ep^2 + 1/12*F[1,1,6] - 1/24*F[1,1,6]^2*ep
          + 1/72*F[1,1,6]^3*ep^2 + 1/144*F[1,1,6]*tci[1,1]^2*ep^2 + 1/
         18*tcr[3,3]*ep^2 - 1/72*tci[1,1]^2*ep ) 
