(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

ratio[r1*r3 - r1^2*r3 + r1^3*r3 - r3*r5 + r1*r6 - 2*r1^2*r6 + r1^3*r6 - 
   r1^3*x34 - r1*r3*x34 + r1^2*r3*x34 - r1^3*r3*x34 - r1*r5*x34 - 
   r1^2*r5*x34 + r3*r5*x34 + 3*r1^2*r6*x34 - 2*r1^3*r6*x34 - r1^2*r5*r6*x34 - 
   3*r1^4*x34^2 - r1^3*r4*r5*x34^2 + 3*r1^3*r6*x34^2 - 2*r1^3*r5*r6*x34^2 + 
   r1^4*r4*x34^3 - r1^4*r4*r5*x34^3 + r1^4*r6*x34^3 - r1^4*r5*r6*x34^3 + 
   r1^3*x51 + r1*r3*x51 - r1^2*r3*x51 + r1^3*r3*x51 - r1^4*r3*x51 + 
   r1*r5*x51 - r3*r5*x51 - 3*r1^2*r6*x51 + 4*r1^3*r6*x51 - r1^4*r6*x51 + 
   3*r1^4*x34*x51 - 6*r1^3*r6*x34*x51 + 3*r1^4*r6*x34*x51 - 
   3*r1^4*r6*x34^2*x51 - r1^4*x51^2 + 3*r1^3*r6*x51^2 - 2*r1^4*r6*x51^2 + 
   3*r1^4*r6*x34*x51^2 - r1^4*r6*x51^3]*(5/18 - F[1, 1, 1]/24 - 
  F[1, 1, 2]/6 - F[1, 1, 6]/24 + tci[1, 2]/24)
