(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

-((5*F[1, 1, 1])/108 - (5*F[1, 1, 1]^2)/288 + (5*F[1, 1, 2])/108 - 
 (F[1, 1, 1]*F[1, 1, 2])/144 - (5*F[1, 1, 2]^2)/288 - (13*tci[1, 1]^2)/864 - 
 (5*tci[1, 2])/108 + (5*F[1, 1, 1]*tci[1, 2])/144 + (F[1, 1, 2]*tci[1, 2])/144)
