(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

ratio[r5 - r3*r5 - 4*r5^2 + 3*r3*r5^2 + 5*r5^3 - 3*r3*r5^3 - 2*r5^4 + 
   r3*r5^4 + r3*r5*x34 - 3*r3*r5^2*x34 + 3*r3*r5^3*x34 - r3*r5^4*x34 + 
   r5^2*r8*x45 - 2*r5^3*r8*x45 + r5^4*r8*x45 - r5^2*x51 + r3*r5^2*x51 + 
   4*r5^3*x51 - 2*r3*r5^3*x51 - 3*r5^4*x51 + r3*r5^4*x51 + r5*r8*x51 - 
   2*r5^2*r8*x51 + r5^3*r8*x51 + r5^2*r8*x45*x51 - 4*r5^3*r8*x45*x51 + 
   3*r5^4*r8*x45*x51 - r5^2*r7*x51^2 + r5^3*r7*x51^2 - 2*r5^2*r8*x51^2 + 
   2*r5^3*r8*x51^2 - r5^3*r7*x45*x51^2 - 2*r5^3*r8*x45*x51^2 + 
   3*r5^4*r8*x45*x51^2 + r5^3*r7*x51^3 + r5^3*r8*x51^3 + r5^4*r7*x45*x51^3 + 
   r5^4*r8*x45*x51^3]*(5/18 - F[1, 1, 1]/24 - F[1, 1, 2]/24 - F[1, 1, 6]/6 + 
  tci[1, 2]/24)
