(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

((3*F[1, 1, 1])/8 - (3*F[1, 1, 3])/8)*
  ratio[-r3 + 2*r3^2 + 5*r3*r5 - 6*r3^2*r5 - 7*r3*r5^2 + 6*r3^2*r5^2 + 
    3*r3*r5^3 - 2*r3^2*r5^3 - r5*r8 + 2*r5^2*r8 - r5^3*r8 - 2*r3^2*x34 + 
    6*r3^2*r5*x34 - 6*r3^2*r5^2*x34 + 2*r3^2*r5^3*x34 - 2*r3^2*r5*x51 - 
    4*r3*r5^2*x51 + 4*r3^2*r5^2*x51 + 4*r3*r5^3*x51 - 2*r3^2*r5^3*x51 + 
    2*r5^2*r8*x51 - 2*r5^3*r8*x51 + r5^3*r7*x51^2 - r5^3*r8*x51^2] + 
 ((3*F[1, 1, 3])/8 - (3*F[1, 1, 8])/8)*
  ratio[1 - 3*r3 + 2*r3^2 - 6*r5 + 11*r3*r5 - 6*r3^2*r5 + 9*r5^2 - 
    13*r3*r5^2 + 6*r3^2*r5^2 - 4*r5^3 + 5*r3*r5^3 - 2*r3^2*r5^3 + 3*r3*x34 - 
    4*r3^2*x34 - 11*r3*r5*x34 + 12*r3^2*r5*x34 + 13*r3*r5^2*x34 - 
    12*r3^2*r5^2*x34 - 5*r3*r5^3*x34 + 4*r3^2*r5^3*x34 + 2*r3^2*x34^2 - 
    6*r3^2*r5*x34^2 + 6*r3^2*r5^2*x34^2 - 2*r3^2*r5^3*x34^2 + r5*r8*x45 - 
    2*r5^2*r8*x45 + r5^3*r8*x45 + 2*r3*r5*x51 - 2*r3^2*r5*x51 + 6*r5^2*x51 - 
    8*r3*r5^2*x51 + 4*r3^2*r5^2*x51 - 6*r5^3*x51 + 6*r3*r5^3*x51 - 
    2*r3^2*r5^3*x51 + r5*r8*x51 - 2*r5^2*r8*x51 + r5^3*r8*x51 + 
    2*r3^2*r5*x34*x51 + 4*r3*r5^2*x34*x51 - 4*r3^2*r5^2*x34*x51 - 
    4*r3*r5^3*x34*x51 + 2*r3^2*r5^3*x34*x51 - 2*r5^2*r8*x45*x51 + 
    2*r5^3*r8*x45*x51 - 2*r5^3*x51^2 + r5^2*r7*x51^2 - 2*r5^2*r8*x51^2 + 
    2*r5^3*r8*x51^2 + r5^3*r8*x45*x51^2 - r5^3*r7*x51^3 + r5^3*r8*x51^3] - 
 (eps5t*r5^2*F[2, 2, 1]*ratio[-3 - 6*r3 - r3^2 + 2*r3^3 - 2*r3^4 - 4*r3^3*r5 + 
     2*r3^4*r5 + 7*r7 + 9*r3*r7 + 3*r3^2*r7 - 10*r3*r5*r7 - 3*r3^2*r5*r7 + 
     4*r3*r5^2*r7 + 4*r8 + 6*r3*r8 - 10*r3*r5*r8 + 4*r3*r5^2*r8 - 9*r7*r8 + 
     10*r5*r7*r8 - 4*r5^2*r7*r8 - r8^2 + 2*r7*r8^2 - r5*r7*r8^2 + 3*r3*x23 - 
     r3^2*x23 + 2*r3^4*x23 - 2*r3*r8*x23 - r3^2*x23^2 + 2*r3^3*x23^2 - 
     2*r3^4*x23^2 - 9*r3*x34 + r3^2*x34 + 2*r3^3*x34 + 2*r3^4*x34 - 
     2*r3^4*r5*x34 + 5*r7*x34 + 8*r3*r7*x34 + 3*r3^2*r7*x34 + 
     7*r3^2*x23*x34 - 8*r3^3*x23*x34 - 2*r3^4*x23*x34 - 2*r3^3*x23^2*x34 + 
     4*r3^4*x23^2*x34 - 13*r3^2*x34^2 + 4*r3^3*x34^2 + 11*r3*r7*x34^2 + 
     r3^2*r7*x34^2 + 8*r3^3*x23*x34^2 - 2*r3^4*x23*x34^2 - 
     2*r3^4*x23^2*x34^2 - 8*r3^3*x34^3 + 2*r3^4*x34^3 + 13*r3^2*r7*x34^3 - 
     4*r3^3*r7*x34^3 + 2*r3^4*x23*x34^3 - 2*r3^4*x34^4 + 8*r3^3*r7*x34^4 - 
     2*r3^4*r7*x34^4 + 2*r3^4*r7*x34^5 + r7*x45 - 2*r7*r8*x45 + r7*r8^2*x45 - 
     r3*x51 + 4*r3^3*x51 - 2*r3^4*x51 - 6*r3^3*r5*x51 + 2*r3^4*r5*x51 - 
     2*r7*x51 + 4*r3*r7*x51 + 4*r3^2*r7*x51 - 14*r3*r5*r7*x51 - 
     7*r3^2*r5*r7*x51 + 9*r3*r5^2*r7*x51 + r8*x51 + 6*r3*r8*x51 + 
     r3^2*r8*x51 - 14*r3*r5*r8*x51 - r3^2*r5*r8*x51 + 9*r3*r5^2*r8*x51 - 
     5*r7*r8*x51 + 14*r5*r7*r8*x51 - 9*r5^2*r7*r8*x51 + r8^2*x51 + 
     r3*r8^2*x51 - r3*r5*r8^2*x51 + 2*r7*r8^2*x51 - 2*r5*r7*r8^2*x51 + 
     4*r3^2*x23*x51 - 6*r3^3*x23*x51 + 2*r3^4*x23*x51 - 2*r3*r8*x23*x51 - 
     r3^2*r8*x23*x51 - r3*r8^2*x23*x51 - 2*r3^3*x23^2*x51 + 
     2*r3^4*x23^2*x51 + r3^2*r8*x23^2*x51 + r3^2*x34*x51 - 8*r3*r7*x34*x51 + 
     3*r3^2*r7*x34*x51 + 4*r3^3*x23*x34*x51 - 2*r3^4*x23*x34*x51 - 
     2*r3^4*x23^2*x34*x51 + 4*r3^3*x34^2*x51 - 14*r3^2*r7*x34^2*x51 + 
     4*r3^3*r7*x34^2*x51 + 2*r3^4*x34^3*x51 - 12*r3^3*r7*x34^3*x51 + 
     2*r3^4*r7*x34^3*x51 - 4*r3^4*r7*x34^4*x51 - 2*r3^2*x51^2 + r3*r7*x51^2 + 
     2*r3^2*r7*x51^2 - 3*r3*r5*r7*x51^2 - 6*r3^2*r5*r7*x51^2 + 
     3*r3*r5^2*r7*x51^2 + r3*r7^2*x51^2 - r3*r5*r7^2*x51^2 + 2*r3*r8*x51^2 - 
     3*r3*r5*r8*x51^2 + 3*r3*r5^2*r8*x51^2 + 3*r5*r7*r8*x51^2 - 
     3*r5^2*r7*r8*x51^2 + r5*r7^2*r8*x51^2 + 2*r3*r8^2*x51^2 - 
     2*r3*r5*r8^2*x51^2 - r5*r7*r8^2*x51^2 + 4*r3^3*x23*x51^2 - 
     2*r3^4*x23*x51^2 - 4*r3^2*r8*x23*x51^2 + 2*r3^3*r8*x23*x51^2 - 
     2*r3^4*x23^2*x51^2 + 2*r3^3*r8*x23^2*x51^2 + 3*r3^2*r7*x34*x51^2 + 
     4*r3^3*r7*x34^2*x51^2 + 2*r3^4*r7*x34^3*x51^2 - 2*r3^2*r5*r7*x51^3 + 
     2*r3*r5^2*r7*x51^3 - r3*r5*r7^2*x51^3 + 2*r3^2*r8*x51^3 - 
     2*r3^2*r5*r8*x51^3 + 2*r3*r5^2*r8*x51^3 - 2*r5^2*r7*r8*x51^3 - 
     r3*r5*r8^2*x51^3 - 4*r3^3*r8*x23*x51^3 + 2*r3^4*r8*x23*x51^3 + 
     2*r3^4*r8*x23^2*x51^3])/16 + 
 ratio[-r20 - 4*r5 + 3*r20*r5 + 5*r3*r5 + 16*r5^2 + r10*r5^2 - 3*r20*r5^2 - 
    15*r3*r5^2 - 20*r5^3 - 2*r10*r5^3 + r20*r5^3 + 15*r3*r5^3 + 8*r5^4 + 
    r10*r5^4 - 5*r3*r5^4 + r10*r5*x34 - 5*r3*r5*x34 - 3*r10*r5^2*x34 + 
    15*r3*r5^2*x34 + 3*r10*r5^3*x34 - 15*r3*r5^3*x34 - r10*r5^4*x34 + 
    5*r3*r5^4*x34 - r20*r5^2*x45 + 2*r20*r5^3*x45 - r20*r5^4*x45 - 
    5*r5^2*r8*x45 + 10*r5^3*r8*x45 - 5*r5^4*r8*x45 - r10*r5*x51 + 
    r20*r5*x51 + 4*r5^2*x51 + r10*r5^2*x51 - 2*r20*r5^2*x51 - 5*r3*r5^2*x51 - 
    16*r5^3*x51 - r10*r5^3*x51 + r20*r5^3*x51 + 10*r3*r5^3*x51 + 
    12*r5^4*x51 + r10*r5^4*x51 - 5*r3*r5^4*x51 - 5*r5*r8*x51 + 
    10*r5^2*r8*x51 - 5*r5^3*r8*x51 - r20*r5^2*x45*x51 + 4*r20*r5^3*x45*x51 - 
    3*r20*r5^4*x45*x51 - 5*r5^2*r8*x45*x51 + 20*r5^3*r8*x45*x51 - 
    15*r5^4*r8*x45*x51 + r20*r5^2*x51^2 + 2*r23*r5^2*x51^2 - r20*r5^3*x51^2 - 
    2*r23*r5^3*x51^2 + 5*r5^2*r7*x51^2 - 5*r5^3*r7*x51^2 + 10*r5^2*r8*x51^2 - 
    10*r5^3*r8*x51^2 + 2*r20*r5^3*x45*x51^2 + r23*r5^3*x45*x51^2 - 
    3*r20*r5^4*x45*x51^2 + 5*r5^3*r7*x45*x51^2 + 10*r5^3*r8*x45*x51^2 - 
    15*r5^4*r8*x45*x51^2 - r20*r5^3*x51^3 - r23*r5^3*x51^3 - 
    5*r5^3*r7*x51^3 - 5*r5^3*r8*x51^3 - r20*r5^4*x45*x51^3 - 
    r23*r5^4*x45*x51^3 - 5*r5^4*r7*x45*x51^3 - 5*r5^4*r8*x45*x51^3]/4 + 
 (-F[1, 1, 6]/4 + F[1, 1, 10]/4)*ratio[-r20 + 2*r20^2 + r5 - 3*r20^2*r5 - 
    r3*r5 + r3^2*r5 - 4*r5^2 + r20^2*r5^2 + 4*r3*r5^2 - 3*r3^2*r5^2 + 
    5*r5^3 - 5*r3*r5^3 + 3*r3^2*r5^3 - 2*r5^4 + 2*r3*r5^4 - r3^2*r5^4 + 
    r20^2*x34 + r3*r5*x34 - 2*r3^2*r5*x34 - 4*r3*r5^2*x34 + 6*r3^2*r5^2*x34 + 
    5*r3*r5^3*x34 - 6*r3^2*r5^3*x34 - 2*r3*r5^4*x34 + 2*r3^2*r5^4*x34 + 
    r3^2*r5*x34^2 - 3*r3^2*r5^2*x34^2 + 3*r3^2*r5^3*x34^2 - r3^2*r5^4*x34^2 - 
    r20^2*r5*x45 - r20*r5^2*x45 + 2*r20^2*r5^2*x45 + 2*r20*r5^3*x45 - 
    r20^2*r5^3*x45 - r20*r5^4*x45 - r20*r5*x51 - 2*r20^2*r5*x51 - r3*r5*x51 + 
    r3^2*r5*x51 - r5^2*x51 + 2*r20*r5^2*x51 + r20^2*r5^2*x51 + 
    5*r3*r5^2*x51 - 4*r3^2*r5^2*x51 + 4*r5^3*x51 - r20*r5^3*x51 - 
    9*r3*r5^3*x51 + 5*r3^2*r5^3*x51 - 3*r5^4*x51 + 5*r3*r5^4*x51 - 
    2*r3^2*r5^4*x51 - r3^2*r5*x34*x51 - r3*r5^2*x34*x51 + 
    4*r3^2*r5^2*x34*x51 + 4*r3*r5^3*x34*x51 - 5*r3^2*r5^3*x34*x51 - 
    3*r3*r5^4*x34*x51 + 2*r3^2*r5^4*x34*x51 - r20^2*r5*x45*x51 - 
    r20*r5^2*x45*x51 + 4*r20^2*r5^2*x45*x51 + 4*r20*r5^3*x45*x51 - 
    3*r20^2*r5^3*x45*x51 - 3*r20*r5^4*x45*x51 + r20^2*r5*x51^2 + 
    2*r20*r5^2*x51^2 - r20^2*r5^2*x51^2 + r3*r5^2*x51^2 - r3^2*r5^2*x51^2 + 
    r5^3*x51^2 - 2*r20*r5^3*x51^2 - 4*r3*r5^3*x51^2 + 2*r3^2*r5^3*x51^2 - 
    3*r5^4*x51^2 + 3*r3*r5^4*x51^2 - r3^2*r5^4*x51^2 - r5^2*r7*x51^2 + 
    2*r5^3*r7*x51^2 - r5*r7^2*x51^2 + r5^2*r7^2*x51^2 + 
    2*r20^2*r5^2*x45*x51^2 + 2*r20*r5^3*x45*x51^2 - 3*r20^2*r5^3*x45*x51^2 - 
    3*r20*r5^4*x45*x51^2 - r5^3*r7*x45*x51^2 + 3*r5^4*r7*x45*x51^2 - 
    2*r5^2*r7^2*x45*x51^2 + r5^3*r7^2*x45*x51^2 - r5^3*r7^2*x45^2*x51^2 - 
    r20^2*r5^2*x51^3 - r20*r5^3*x51^3 + r5^2*r7^2*x51^3 - 
    r20^2*r5^3*x45*x51^3 - r20*r5^4*x45*x51^3 + 2*r5^3*r7^2*x45*x51^3 + 
    r5^4*r7^2*x45^2*x51^3] + (F[1, 1, 6]/4 - F[1, 1, 7]/4)*
  ratio[r3 - r3^2 + 2*r5 - 6*r3*r5 + 4*r3^2*r5 - 8*r5^2 + 12*r3*r5^2 - 
    6*r3^2*r5^2 + 10*r5^3 - 10*r3*r5^3 + 4*r3^2*r5^3 - 4*r5^4 + 3*r3*r5^4 - 
    r3^2*r5^4 - r8 + 4*r5*r8 - 5*r5^2*r8 + 2*r5^3*r8 + r3^2*x34 + 
    2*r3*r5*x34 - 5*r3^2*r5*x34 - 7*r3*r5^2*x34 + 9*r3^2*r5^2*x34 + 
    8*r3*r5^3*x34 - 7*r3^2*r5^3*x34 - 3*r3*r5^4*x34 + 2*r3^2*r5^4*x34 + 
    r3^2*r5*x34^2 - 3*r3^2*r5^2*x34^2 + 3*r3^2*r5^3*x34^2 - r3^2*r5^4*x34^2 - 
    r5*r8*x45 + 5*r5^2*r8*x45 - 7*r5^3*r8*x45 + 3*r5^4*r8*x45 - r5*r8^2*x45 + 
    2*r5^2*r8^2*x45 - r5^3*r8^2*x45 - r5^2*r8^2*x45^2 + 2*r5^3*r8^2*x45^2 - 
    r5^4*r8^2*x45^2 - 2*r3*r5*x51 + 2*r3^2*r5*x51 - 2*r5^2*x51 + 
    10*r3*r5^2*x51 - 6*r3^2*r5^2*x51 + 8*r5^3*x51 - 14*r3*r5^3*x51 + 
    6*r3^2*r5^3*x51 - 6*r5^4*x51 + 6*r3*r5^4*x51 - 2*r3^2*r5^4*x51 + 
    2*r5*r8*x51 - 6*r5^2*r8*x51 + 4*r5^3*r8*x51 - r8^2*x51 + 2*r5*r8^2*x51 - 
    r5^2*r8^2*x51 - r3^2*r5*x34*x51 - r3*r5^2*x34*x51 + 4*r3^2*r5^2*x34*x51 + 
    4*r3*r5^3*x34*x51 - 5*r3^2*r5^3*x34*x51 - 3*r3*r5^4*x34*x51 + 
    2*r3^2*r5^4*x34*x51 + 2*r5^2*r8*x45*x51 - 8*r5^3*r8*x45*x51 + 
    6*r5^4*r8*x45*x51 - 2*r5*r8^2*x45*x51 + 6*r5^2*r8^2*x45*x51 - 
    4*r5^3*r8^2*x45*x51 - r5^2*r8^2*x45^2*x51 + 4*r5^3*r8^2*x45^2*x51 - 
    3*r5^4*r8^2*x45^2*x51 - 2*r23^2*r5*x51^2 - r23*r5^2*x51^2 + 
    2*r23^2*r5^2*x51^2 + r3*r5^2*x51^2 - r3^2*r5^2*x51^2 + r5^3*x51^2 + 
    r23*r5^3*x51^2 - 4*r3*r5^3*x51^2 + 2*r3^2*r5^3*x51^2 - 3*r5^4*x51^2 + 
    3*r3*r5^4*x51^2 - r3^2*r5^4*x51^2 - r5^2*r8*x51^2 + 2*r5^3*r8*x51^2 + 
    2*r5*r8^2*x51^2 - 2*r5^2*r8^2*x51^2 - r23^2*r5^2*x45*x51^2 - 
    r23*r5^3*x45*x51^2 - r5^3*r8*x45*x51^2 + 3*r5^4*r8*x45*x51^2 + 
    4*r5^2*r8^2*x45*x51^2 - 5*r5^3*r8^2*x45*x51^2 + 2*r5^3*r8^2*x45^2*x51^2 - 
    3*r5^4*r8^2*x45^2*x51^2 + r23^2*r5^2*x51^3 + r23*r5^3*x51^3 - 
    r5^2*r8^2*x51^3 + r23^2*r5^3*x45*x51^3 + r23*r5^4*x45*x51^3 - 
    2*r5^3*r8^2*x45*x51^3 - r5^4*r8^2*x45^2*x51^3] + 
 ratio[r3 - 2*r3^2 + 2*r3^3 + r5 - 8*r3*r5 + 12*r3^2*r5 - 8*r3^3*r5 - 
    6*r5^2 + 20*r3*r5^2 - 24*r3^2*r5^2 + 12*r3^3*r5^2 + 9*r5^3 - 20*r3*r5^3 + 
    20*r3^2*r5^3 - 8*r3^3*r5^3 - 4*r5^4 + 7*r3*r5^4 - 6*r3^2*r5^4 + 
    2*r3^3*r5^4 + r5*r8 - 2*r5^2*r8 + r5^3*r8 + 2*r3^2*x34 - 4*r3^3*x34 + 
    3*r3*r5*x34 - 16*r3^2*r5*x34 + 18*r3^3*r5*x34 - 13*r3*r5^2*x34 + 
    38*r3^2*r5^2*x34 - 30*r3^3*r5^2*x34 + 17*r3*r5^3*x34 - 36*r3^2*r5^3*x34 + 
    22*r3^3*r5^3*x34 - 7*r3*r5^4*x34 + 12*r3^2*r5^4*x34 - 6*r3^3*r5^4*x34 + 
    2*r3^3*x34^2 + 4*r3^2*r5*x34^2 - 12*r3^3*r5*x34^2 - 14*r3^2*r5^2*x34^2 + 
    24*r3^3*r5^2*x34^2 + 16*r3^2*r5^3*x34^2 - 20*r3^3*r5^3*x34^2 - 
    6*r3^2*r5^4*x34^2 + 6*r3^3*r5^4*x34^2 + 2*r3^3*r5*x34^3 - 
    6*r3^3*r5^2*x34^3 + 6*r3^3*r5^3*x34^3 - 2*r3^3*r5^4*x34^3 + r5^2*r8*x45 - 
    2*r5^3*r8*x45 + r5^4*r8*x45 - 2*r3^2*x51 + 2*r3^3*x51 - 6*r3*r5*x51 + 
    16*r3^2*r5*x51 - 12*r3^3*r5*x51 - 3*r5^2*x51 + 27*r3*r5^2*x51 - 
    44*r3^2*r5^2*x51 + 24*r3^3*r5^2*x51 + 12*r5^3*x51 - 40*r3*r5^3*x51 + 
    48*r3^2*r5^3*x51 - 20*r3^3*r5^3*x51 - 9*r5^4*x51 + 19*r3*r5^4*x51 - 
    18*r3^2*r5^4*x51 + 6*r3^3*r5^4*x51 + r3*r8*x51 + 2*r5*r8*x51 - 
    2*r3*r5*r8*x51 - 6*r5^2*r8*x51 + r3*r5^2*r8*x51 + 4*r5^3*r8*x51 - 
    2*r3^3*x34*x51 - 8*r3^2*r5*x34*x51 + 16*r3^3*r5*x34*x51 - 
    4*r3*r5^2*x34*x51 + 36*r3^2*r5^2*x34*x51 - 38*r3^3*r5^2*x34*x51 + 
    16*r3*r5^3*x34*x51 - 52*r3^2*r5^3*x34*x51 + 36*r3^3*r5^3*x34*x51 - 
    12*r3*r5^4*x34*x51 + 24*r3^2*r5^4*x34*x51 - 12*r3^3*r5^4*x34*x51 - 
    4*r3^3*r5*x34^2*x51 - 2*r3^2*r5^2*x34^2*x51 + 14*r3^3*r5^2*x34^2*x51 + 
    8*r3^2*r5^3*x34^2*x51 - 16*r3^3*r5^3*x34^2*x51 - 6*r3^2*r5^4*x34^2*x51 + 
    6*r3^3*r5^4*x34^2*x51 + r5^2*r8*x45*x51 - 4*r5^3*r8*x45*x51 + 
    3*r5^4*r8*x45*x51 + 4*r3^2*r5*x51^2 - 4*r3^3*r5*x51^2 + 8*r3*r5^2*x51^2 - 
    22*r3^2*r5^2*x51^2 + 14*r3^3*r5^2*x51^2 + 4*r5^3*x51^2 - 
    24*r3*r5^3*x51^2 + 36*r3^2*r5^3*x51^2 - 16*r3^3*r5^3*x51^2 - 
    6*r5^4*x51^2 + 18*r3*r5^4*x51^2 - 18*r3^2*r5^4*x51^2 + 
    6*r3^3*r5^4*x51^2 - r3*r5*r7*x51^2 - 2*r5^2*r7*x51^2 + r3*r5^2*r7*x51^2 + 
    r5^3*r7*x51^2 - 2*r3*r5*r8*x51^2 - 4*r5^2*r8*x51^2 + 2*r3*r5^2*r8*x51^2 + 
    5*r5^3*r8*x51^2 + 2*r3^3*r5*x34*x51^2 + 4*r3^2*r5^2*x34*x51^2 - 
    10*r3^3*r5^2*x34*x51^2 + 2*r3*r5^3*x34*x51^2 - 16*r3^2*r5^3*x34*x51^2 + 
    14*r3^3*r5^3*x34*x51^2 - 6*r3*r5^4*x34*x51^2 + 12*r3^2*r5^4*x34*x51^2 - 
    6*r3^3*r5^4*x34*x51^2 - r5^3*r7*x45*x51^2 - 2*r5^3*r8*x45*x51^2 + 
    3*r5^4*r8*x45*x51^2 - 2*r3^2*r5^2*x51^3 + 2*r3^3*r5^2*x51^3 - 
    4*r3*r5^3*x51^3 + 8*r3^2*r5^3*x51^3 - 4*r3^3*r5^3*x51^3 - 2*r5^4*x51^3 + 
    6*r3*r5^4*x51^3 - 6*r3^2*r5^4*x51^3 + 2*r3^3*r5^4*x51^3 + 
    r3*r5^2*r7*x51^3 + 2*r5^3*r7*x51^3 + r3*r5^2*r8*x51^3 + 2*r5^3*r8*x51^3 + 
    r5^4*r7*x45*x51^3 + r5^4*r8*x45*x51^3]*(-F[1, 1, 6]^2/8 + 
   (F[1, 1, 6]*F[1, 1, 7])/8 + (F[1, 1, 6]*F[1, 1, 10])/8 - 
   (F[1, 1, 7]*F[1, 1, 10])/8 - F[2, 1, 8]/8 - F[2, 1, 14]/8 - 
   tci[1, 1]^2/48) + ratio[r3 - 5*r3^2 + 8*r3^3 - 6*r3^4 + r5 - 8*r3*r5 + 
    20*r3^2*r5 - 24*r3^3*r5 + 12*r3^4*r5 - 2*r5^2 + 10*r3*r5^2 - 
    20*r3^2*r5^2 + 20*r3^3*r5^2 - 8*r3^4*r5^2 + r5^3 - 4*r3*r5^3 + 
    7*r3^2*r5^3 - 6*r3^3*r5^3 + 2*r3^4*r5^3 + r3^2*x23 - 2*r3^3*x23 + 
    2*r3^4*x23 + 6*r3^2*x34 - 16*r3^3*x34 + 18*r3^4*x34 + 6*r3*r5*x34 - 
    31*r3^2*r5*x34 + 54*r3^3*r5*x34 - 36*r3^4*r5*x34 - 9*r3*r5^2*x34 + 
    33*r3^2*r5^2*x34 - 46*r3^3*r5^2*x34 + 24*r3^4*r5^2*x34 + 4*r3*r5^3*x34 - 
    12*r3^2*r5^3*x34 + 14*r3^3*r5^3*x34 - 6*r3^4*r5^3*x34 - r3^2*x23*x34 + 
    4*r3^3*x23*x34 - 6*r3^4*x23*x34 - r3^2*x34^2 + 8*r3^3*x34^2 - 
    18*r3^4*x34^2 + 11*r3^2*r5*x34^2 - 36*r3^3*r5*x34^2 + 36*r3^4*r5*x34^2 - 
    13*r3^2*r5^2*x34^2 + 32*r3^3*r5^2*x34^2 - 24*r3^4*r5^2*x34^2 + 
    5*r3^2*r5^3*x34^2 - 10*r3^3*r5^3*x34^2 + 6*r3^4*r5^3*x34^2 - 
    2*r3^3*x23*x34^2 + 6*r3^4*x23*x34^2 + 6*r3^4*x34^3 + 6*r3^3*r5*x34^3 - 
    12*r3^4*r5*x34^3 - 6*r3^3*r5^2*x34^3 + 8*r3^4*r5^2*x34^3 + 
    2*r3^3*r5^3*x34^3 - 2*r3^4*r5^3*x34^3 - 2*r3^4*x23*x34^3 + 2*r3^3*x51 - 
    6*r3*r5*x51 + 9*r3^2*r5*x51 - 16*r3^3*r5*x51 + 6*r3^4*r5*x51 - 
    2*r5^2*x51 + 19*r3*r5^2*x51 - 27*r3^2*r5^2*x51 + 30*r3^3*r5^2*x51 - 
    10*r3^4*r5^2*x51 + 2*r5^3*x51 - 12*r3*r5^3*x51 + 16*r3^2*r5^3*x51 - 
    14*r3^3*r5^3*x51 + 4*r3^4*r5^3*x51 - 2*r3*r8*x51 + 2*r5*r8*x51 + 
    10*r3*r5*r8*x51 - 4*r5^2*r8*x51 - 14*r3*r5^2*r8*x51 + 2*r5^3*r8*x51 + 
    6*r3*r5^3*r8*x51 - 4*r5*r8^2*x51 + 8*r5^2*r8^2*x51 - 4*r5^3*r8^2*x51 - 
    r3^2*x23*x51 + 2*r3^3*x23*x51 - 2*r3^4*x23*x51 - 2*r3^3*x34*x51 - 
    8*r3^2*r5*x34*x51 + 20*r3^3*r5*x34*x51 - 12*r3^4*r5*x34*x51 - 
    6*r3*r5^2*x34*x51 + 30*r3^2*r5^2*x34*x51 - 44*r3^3*r5^2*x34*x51 + 
    20*r3^4*r5^2*x34*x51 + 6*r3*r5^3*x34*x51 - 20*r3^2*r5^3*x34*x51 + 
    22*r3^3*r5^3*x34*x51 - 8*r3^4*r5^3*x34*x51 - 2*r3^3*x23*x34*x51 + 
    4*r3^4*x23*x34*x51 - 4*r3^3*r5*x34^2*x51 + 6*r3^4*r5*x34^2*x51 - 
    4*r3^2*r5^2*x34^2*x51 + 14*r3^3*r5^2*x34^2*x51 - 10*r3^4*r5^2*x34^2*x51 + 
    4*r3^2*r5^3*x34^2*x51 - 8*r3^3*r5^3*x34^2*x51 + 4*r3^4*r5^3*x34^2*x51 - 
    2*r3^4*x23*x34^2*x51 - 2*r3^3*x51^2 + 2*r3^3*r5*x51^2 + 3*r3*r5^2*x51^2 - 
    8*r3^2*r5^2*x51^2 + 6*r3^3*r5^2*x51^2 - 2*r3^4*r5^2*x51^2 + r5^3*x51^2 - 
    8*r3*r5^3*x51^2 + 12*r3^2*r5^3*x51^2 - 8*r3^3*r5^3*x51^2 + 
    2*r3^4*r5^3*x51^2 + r3^2*r5*r8*x51^2 - 4*r5^2*r8*x51^2 - 
    5*r3*r5^2*r8*x51^2 - r3^2*r5^2*r8*x51^2 + 4*r5^3*r8*x51^2 + 
    6*r3*r5^3*r8*x51^2 - r3*r8^2*x51^2 - r5*r8^2*x51^2 + 2*r3*r5*r8^2*x51^2 + 
    10*r5^2*r8^2*x51^2 - r3*r5^2*r8^2*x51^2 - 9*r5^3*r8^2*x51^2 - 
    2*r3^3*x23*x51^2 + 2*r3^4*x23*x51^2 + r3^2*r8*x23*x51^2 + 
    2*r3^2*r5^2*x34*x51^2 - 4*r3^3*r5^2*x34*x51^2 + 2*r3^4*r5^2*x34*x51^2 + 
    2*r3*r5^3*x34*x51^2 - 6*r3^2*r5^3*x34*x51^2 + 6*r3^3*r5^3*x34*x51^2 - 
    2*r3^4*r5^3*x34*x51^2 - 2*r3^4*x23*x34*x51^2 + 2*r3^3*r8*x51^3 - 
    2*r3^3*r5*r8*x51^3 + 2*r3^2*r5^2*r8*x51^3 + 2*r5^3*r8*x51^3 + 
    2*r3*r5*r8^2*x51^3 + 2*r5^2*r8^2*x51^3 - 2*r3*r5^2*r8^2*x51^3 - 
    6*r5^3*r8^2*x51^3 - 2*r3^4*x23*x51^3 + 2*r3^3*r8*x23*x51^3 - 
    r3*r5^2*r8^2*x51^4 - r5^3*r8^2*x51^4 + 2*r3^4*r8*x23*x51^4]*
  (F[1, 1, 1]^2/8 - (F[1, 1, 1]*F[1, 1, 3])/8 - (F[1, 1, 1]*F[1, 1, 8])/8 + 
   (F[1, 1, 3]*F[1, 1, 8])/8 + F[2, 1, 1]/8 + F[2, 1, 9]/8 + 
   tci[1, 1]^2/48) + ratio[-2*r3*r5*x51 + 2*r3^2*r5*x51 + 5*r3*r5^2*x51 - 
    4*r3^2*r5^2*x51 - 3*r3*r5^3*x51 + 2*r3^2*r5^3*x51 + r5*r8*x51 - 
    2*r5^2*r8*x51 + r5^3*r8*x51 - 2*r3^2*r5*x34*x51 + 4*r3^2*r5^2*x34*x51 - 
    2*r3^2*r5^3*x34*x51 + 2*r3*r5^2*x51^2 - 2*r3^2*r5^2*x51^2 - 
    4*r3*r5^3*x51^2 + 2*r3^2*r5^3*x51^2 - 2*r5^2*r8*x51^2 + 2*r5^3*r8*x51^2 - 
    r5^3*r7*x51^3 + r5^3*r8*x51^3]*((3*F[1, 1, 3])/8 - (3*F[1, 1, 5])/8 - 
   (3*tci[1, 2])/8) + ratio[-3 + 4*r3 + 7*r5 - 8*r3*r5 - 4*r5^2 + 4*r3*r5^2 - 
    4*r3*x34 + 8*r3*r5*x34 - 4*r3*r5^2*x34 + 2*r5*x51 - 4*r3*r5*x51 - 
    4*r5^2*x51 + 4*r3*r5^2*x51]*(F[1, 1, 3]/8 - F[1, 1, 6]/8 - tci[1, 2]/8) + 
 ratio[-8*r3 + 6*r3^2 - 8*r5 + 31*r3*r5 - 18*r3^2*r5 + 17*r5^2 - 38*r3*r5^2 + 
    18*r3^2*r5^2 - 9*r5^3 + 15*r3*r5^3 - 6*r3^2*r5^3 + 3*r8 - 9*r5*r8 + 
    9*r5^2*r8 - 3*r5^3*r8 + 2*r3*x34 - 6*r3^2*x34 - 10*r3*r5*x34 + 
    18*r3^2*r5*x34 + 14*r3*r5^2*x34 - 18*r3^2*r5^2*x34 - 6*r3*r5^3*x34 + 
    6*r3^2*r5^3*x34 + 3*r5*r8*x45 - 6*r5^2*r8*x45 + 3*r5^3*r8*x45 + 
    2*r5*x51 + 8*r3*r5*x51 - 6*r3^2*r5*x51 + 8*r5^2*x51 - 26*r3*r5^2*x51 + 
    12*r3^2*r5^2*x51 - 12*r5^3*x51 + 18*r3*r5^3*x51 - 6*r3^2*r5^3*x51 - 
    6*r5*r8*x51 + 12*r5^2*r8*x51 - 6*r5^3*r8*x51 - 6*r5^2*r8*x45*x51 + 
    6*r5^3*r8*x45*x51 - 3*r5^2*r7*x51^2 + 3*r5^3*r7*x51^2 + 3*r5^2*r8*x51^2 - 
    3*r5^3*r8*x51^2 - 3*r5^3*r7*x45*x51^2 + 3*r5^3*r8*x45*x51^2]*
  (F[1, 1, 2]/8 - F[1, 1, 3]/8 + tci[1, 2]/8) + 
 ratio[-3 + 4*r3 + 5*r5 + 2*r10*r5 - 2*r10^2*r5 - 8*r3*r5 + 4*r5^2 - 
    6*r10*r5^2 + 4*r10^2*r5^2 + 4*r3*r5^2 - 10*r5^3 + 6*r10*r5^3 - 
    2*r10^2*r5^3 + 4*r5^4 - 2*r10*r5^4 - 2*r10^2*x34 - 4*r3*x34 - 
    2*r10*r5*x34 + 6*r10^2*r5*x34 + 8*r3*r5*x34 + 6*r10*r5^2*x34 - 
    6*r10^2*r5^2*x34 - 4*r3*r5^2*x34 - 6*r10*r5^3*x34 + 2*r10^2*r5^3*x34 + 
    2*r10*r5^4*x34 + 2*r5*r8*x45 - 8*r5^2*r8*x45 + 10*r5^3*r8*x45 - 
    4*r5^4*r8*x45 + 2*r5^2*r8^2*x45^2 - 4*r5^3*r8^2*x45^2 + 
    2*r5^4*r8^2*x45^2 + 2*r10^2*x51 + 2*r5*x51 - 2*r10^2*r5*x51 - 
    4*r3*r5*x51 - 2*r5^2*x51 - 2*r10*r5^2*x51 + 2*r10^2*r5^2*x51 + 
    4*r3*r5^2*x51 - 8*r5^3*x51 + 4*r10*r5^3*x51 - 2*r10^2*r5^3*x51 + 
    6*r5^4*x51 - 2*r10*r5^4*x51 - 2*r5^2*r8*x45*x51 + 8*r5^3*r8*x45*x51 - 
    6*r5^4*r8*x45*x51 + 2*r5*r8^2*x45*x51 - 4*r5^2*r8^2*x45*x51 + 
    2*r5^3*r8^2*x45*x51 + 2*r5^2*r8^2*x45^2*x51 - 8*r5^3*r8^2*x45^2*x51 + 
    6*r5^4*r8^2*x45^2*x51 - 4*r5^3*r7*x45*x51^2 + 6*r5^4*r7*x45*x51^2 - 
    2*r5^2*r7^2*x45*x51^2 + 2*r5^3*r7^2*x45*x51^2 - 2*r5^3*r8*x45*x51^2 - 
    4*r5^2*r8^2*x45*x51^2 + 4*r5^3*r8^2*x45*x51^2 - 2*r5^3*r7^2*x45^2*x51^2 - 
    4*r5^3*r8^2*x45^2*x51^2 + 6*r5^4*r8^2*x45^2*x51^2 + 2*r5^4*r7*x45*x51^3 + 
    2*r5^3*r7^2*x45*x51^3 + 2*r5^4*r8*x45*x51^3 + 2*r5^3*r8^2*x45*x51^3 + 
    2*r5^4*r7^2*x45^2*x51^3 + 2*r5^4*r8^2*x45^2*x51^3]*
  (-F[1, 1, 4]/8 + F[1, 1, 6]/8 + tci[1, 2]/8) + 
 ratio[2*r3 - 2*r3^2 - 7*r3*r5 + 6*r3^2*r5 + 8*r3*r5^2 - 6*r3^2*r5^2 - 
    3*r3*r5^3 + 2*r3^2*r5^3 - r8 + 3*r5*r8 - 3*r5^2*r8 + r5^3*r8 + 
    2*r3^2*x34 - 6*r3^2*r5*x34 + 6*r3^2*r5^2*x34 - 2*r3^2*r5^3*x34 - 
    4*r3*r5*x51 + 4*r3^2*r5*x51 + 11*r3*r5^2*x51 - 8*r3^2*r5^2*x51 - 
    7*r3*r5^3*x51 + 4*r3^2*r5^3*x51 + 3*r5*r8*x51 - 6*r5^2*r8*x51 + 
    3*r5^3*r8*x51 - 2*r3^2*r5*x34*x51 + 4*r3^2*r5^2*x34*x51 - 
    2*r3^2*r5^3*x34*x51 + 2*r3*r5^2*x51^2 - 2*r3^2*r5^2*x51^2 - 
    4*r3*r5^3*x51^2 + 2*r3^2*r5^3*x51^2 + r5^2*r7*x51^2 - r5^3*r7*x51^2 - 
    3*r5^2*r8*x51^2 + 3*r5^3*r8*x51^2 - r5^3*r7*x51^3 + r5^3*r8*x51^3]*
  ((-3*F[1, 1, 3])/8 + (3*F[1, 1, 9])/8 + (3*tci[1, 2])/8) + 
 ratio[r3 - 5*r3^2 + 8*r3^3 - 6*r3^4 + 6*r5 - 24*r3*r5 + 28*r3^2*r5 - 
    22*r3^3*r5 + 12*r3^4*r5 - 5*r5^2 + 16*r3*r5^2 - 23*r3^2*r5^2 + 
    20*r3^3*r5^2 - 8*r3^4*r5^2 + r5^3 - 4*r3*r5^3 + 7*r3^2*r5^3 - 
    6*r3^3*r5^3 + 2*r3^4*r5^3 + r6 - r5*r6 - 6*r7 + 16*r3*r7 - 8*r3^2*r7 - 
    2*r3^3*r7 + 25*r5*r7 - 30*r3*r5*r7 + 9*r3^2*r5*r7 + 2*r3^3*r5*r7 - 
    9*r5^2*r7 + 9*r3*r5^2*r7 - 3*r3^2*r5^2*r7 + r3^2*x23 - 2*r3^3*x23 + 
    2*r3^4*x23 - 10*r3^3*x34 + 16*r3^4*x34 - 2*r3*r5*x34 + 36*r3^3*r5*x34 - 
    30*r3^4*r5*x34 + 4*r3*r5^2*x34 - 24*r3^3*r5^2*x34 + 18*r3^4*r5^2*x34 - 
    2*r3*r5^3*x34 + 6*r3^3*r5^3*x34 - 4*r3^4*r5^3*x34 - 4*r3*r6*x34 + 
    6*r3*r5*r6*x34 - 2*r3*r5^2*r6*x34 + 6*r3*r7*x34 - 7*r3^2*r7*x34 - 
    2*r3^3*r7*x34 - r3^2*x23*x34 + 4*r3^3*x23*x34 - 6*r3^4*x23*x34 + 
    5*r3^2*x34^2 - 4*r3^3*x34^2 - 12*r3^4*x34^2 + 20*r3^4*r5*x34^2 - 
    10*r3^4*r5^2*x34^2 + 2*r3^4*r5^3*x34^2 - 6*r3^2*r7*x34^2 - 
    2*r3^3*r7*x34^2 - 2*r3^3*x23*x34^2 + 6*r3^4*x23*x34^2 + 6*r3^3*x34^3 - 
    7*r3^2*r7*x34^3 - 2*r3^3*r7*x34^3 - 2*r3^4*x23*x34^3 + 2*r3^4*x34^4 - 
    6*r3^3*r7*x34^4 - 2*r3^4*r7*x34^5 - 6*r5*r7*x45 + 3*r5^2*r7*x45 - 
    r3*x51 - r3^2*x51 + 6*r3^3*x51 - 6*r3^4*x51 - 4*r3*r5*x51 + 
    21*r3^2*r5*x51 - 24*r3^3*r5*x51 + 18*r3^4*r5*x51 + 2*r5^2*x51 + 
    6*r3*r5^2*x51 - 34*r3^2*r5^2*x51 + 38*r3^3*r5^2*x51 - 18*r3^4*r5^2*x51 - 
    4*r5^3*x51 - 2*r3*r5^3*x51 + 16*r3^2*r5^3*x51 - 16*r3^3*r5^3*x51 + 
    6*r3^4*r5^3*x51 - r6*x51 - 7*r3^2*r7*x51 - 4*r3^3*r7*x51 - 
    16*r3*r5*r7*x51 + 16*r3^2*r5*r7*x51 + 6*r3^3*r5*r7*x51 + 16*r5^2*r7*x51 - 
    2*r3*r5^2*r7*x51 - 8*r3^2*r5^2*r7*x51 - 12*r5^3*r7*x51 + 
    6*r3*r5^3*r7*x51 - 2*r3^2*x34*x51 - 4*r3^3*x34*x51 + 12*r3^4*x34*x51 - 
    2*r3^2*r5*x34*x51 + 24*r3^3*r5*x34*x51 - 32*r3^4*r5*x34*x51 + 
    4*r3^2*r5^2*x34*x51 - 28*r3^3*r5^2*x34*x51 + 28*r3^4*r5^2*x34*x51 - 
    2*r3^2*r5^3*x34*x51 + 10*r3^3*r5^3*x34*x51 - 8*r3^4*r5^3*x34*x51 + 
    2*r3*r6*x34*x51 - 2*r3^3*r7*x34*x51 - 2*r3^3*x34^2*x51 - 
    6*r3^4*x34^2*x51 + 12*r3^4*r5*x34^2*x51 - 8*r3^4*r5^2*x34^2*x51 + 
    2*r3^4*r5^3*x34^2*x51 + 7*r3^2*r7*x34^2*x51 + 6*r3^3*r7*x34^3*x51 + 
    2*r3^4*r7*x34^4*x51 - 6*r5^2*r7*x45*x51 + 6*r5^3*r7*x45*x51 - 
    4*r3^3*r5*x51^2 + 6*r3^4*r5*x51^2 + 2*r3*r5^2*x51^2 - 
    11*r3^2*r5^2*x51^2 + 20*r3^3*r5^2*x51^2 - 12*r3^4*r5^2*x51^2 - 
    4*r3*r5^3*x51^2 + 11*r3^2*r5^3*x51^2 - 14*r3^3*r5^3*x51^2 + 
    6*r3^4*r5^3*x51^2 - 2*r3^3*r7*x51^2 + 7*r3^2*r5*r7*x51^2 + 
    6*r3^3*r5*r7*x51^2 + 4*r5^2*r7*x51^2 - 5*r3*r5^2*r7*x51^2 - 
    7*r3^2*r5^2*r7*x51^2 - 4*r5^3*r7*x51^2 + 6*r3*r5^3*r7*x51^2 - 
    r3*r7^2*x51^2 + r5*r7^2*x51^2 + 2*r3*r5*r7^2*x51^2 - 4*r5^2*r7^2*x51^2 - 
    r3*r5^2*r7^2*x51^2 + 3*r5^3*r7^2*x51^2 - 6*r3^4*r5*x34*x51^2 - 
    6*r3^3*r5^2*x34*x51^2 + 10*r3^4*r5^2*x34*x51^2 + 4*r3^3*r5^3*x34*x51^2 - 
    4*r3^4*r5^3*x34*x51^2 - 2*r5^3*r7*x45*x51^2 + 5*r5^2*r7^2*x45*x51^2 - 
    6*r5^3*r7^2*x45*x51^2 + 3*r5^3*r7^2*x45^2*x51^2 + 2*r3^3*r5^2*x51^3 - 
    2*r3^4*r5^2*x51^3 + 2*r3^2*r5^3*x51^3 - 4*r3^3*r5^3*x51^3 + 
    2*r3^4*r5^3*x51^3 + 2*r3^3*r5*r7*x51^3 - 2*r3^2*r5^2*r7*x51^3 - 
    2*r5^3*r7*x51^3 + 2*r3*r5*r7^2*x51^3 - 2*r3*r5^2*r7^2*x51^3 + 
    2*r5^3*r7^2*x51^3 - 2*r5^3*r7^2*x45*x51^3 - r3*r5^2*r7^2*x51^4 - 
    r5^3*r7^2*x51^4]*(-F[1, 1, 1]^2/16 + F[1, 1, 2]^2/16 - 
   (F[1, 1, 2]*F[1, 1, 5])/8 + (F[1, 1, 2]*F[1, 1, 8])/8 + 
   (F[1, 1, 1]*F[1, 1, 9])/8 - (F[1, 1, 2]*F[1, 1, 9])/8 + 
   (F[1, 1, 5]*F[1, 1, 9])/8 - (F[1, 1, 8]*F[1, 1, 9])/8 + F[2, 1, 4]/8 - 
   F[2, 1, 9]/8 + (F[1, 1, 1]*tci[1, 2])/8 - (F[1, 1, 2]*tci[1, 2])/8) + 
 ratio[2 + 12*r3 - 2*r3^2 + 3*r4 - 4*r3*r4 + 10*r5 - 30*r3*r5 + 6*r3^2*r5 - 
    7*r4*r5 + 6*r3*r4*r5 - 15*r5^2 + 28*r3*r5^2 - 6*r3^2*r5^2 + 2*r4*r5^2 - 
    2*r3*r4*r5^2 + 7*r5^3 - 10*r3*r5^3 + 2*r3^2*r5^3 - 14*r8 + 35*r5*r8 - 
    31*r5^2*r8 + 10*r5^3*r8 - 4*r3*x23 - 3*r4*x23 + 2*r3*r4*x23 + 3*r8*x23 + 
    2*r3^2*x34 + 2*r4*x34 + 4*r3*r4*x34 + 2*r3*r5*x34 - 6*r3^2*r5*x34 - 
    6*r3*r4*r5*x34 - 4*r3*r5^2*x34 + 6*r3^2*r5^2*x34 + 2*r3*r4*r5^2*x34 + 
    2*r3*r5^3*x34 - 2*r3^2*r5^3*x34 - 2*r3*r4*x23*x34 + 3*r4*x45 - 
    14*r5*r8*x45 + 19*r5^2*r8*x45 - 8*r5^3*r8*x45 + 3*r8^2*x45 - 
    10*r5*r8^2*x45 + 11*r5^2*r8^2*x45 - 4*r5^3*r8^2*x45 + 3*r5*r8^2*x45^2 - 
    6*r5^2*r8^2*x45^2 + 3*r5^3*r8^2*x45^2 + r3*x51 + r3^2*x51 - 4*r3^3*x51 + 
    6*r3^4*x51 - 2*r3*r5*x51 - 3*r3^2*r5*x51 + 12*r3^3*r5*x51 - 
    12*r3^4*r5*x51 - 2*r5^2*x51 + 10*r3*r5^2*x51 - 2*r3^2*r5^2*x51 - 
    8*r3^3*r5^2*x51 + 8*r3^4*r5^2*x51 + 4*r5^3*x51 - 8*r3*r5^3*x51 + 
    2*r3^2*r5^3*x51 + 2*r3^3*r5^3*x51 - 2*r3^4*r5^3*x51 - 12*r3*r8*x51 - 
    2*r5*r8*x51 + 26*r3*r5*r8*x51 - 12*r5^2*r8*x51 - 20*r3*r5^2*r8*x51 + 
    10*r5^3*r8*x51 + 6*r3*r5^3*r8*x51 + 2*r8^2*x51 - 8*r5*r8^2*x51 + 
    10*r5^2*r8^2*x51 - 4*r5^3*r8^2*x51 - r3^2*x23*x51 + 2*r3^3*x23*x51 - 
    2*r3^4*x23*x51 + 4*r3*r8*x23*x51 + 2*r3^2*x34*x51 + 2*r3^3*x34*x51 - 
    12*r3^4*x34*x51 - 2*r3^2*r5*x34*x51 - 12*r3^3*r5*x34*x51 + 
    24*r3^4*r5*x34*x51 + 4*r3^2*r5^2*x34*x51 + 8*r3^3*r5^2*x34*x51 - 
    16*r3^4*r5^2*x34*x51 - 2*r3^2*r5^3*x34*x51 - 2*r3^3*r5^3*x34*x51 + 
    4*r3^4*r5^3*x34*x51 - 2*r3^3*x23*x34*x51 + 4*r3^4*x23*x34*x51 + 
    2*r3^3*x34^2*x51 + 6*r3^4*x34^2*x51 - 12*r3^4*r5*x34^2*x51 + 
    8*r3^4*r5^2*x34^2*x51 - 2*r3^4*r5^3*x34^2*x51 - 2*r3^4*x23*x34^2*x51 + 
    10*r5^2*r8*x45*x51 - 10*r5^3*r8*x45*x51 - 4*r5*r8^2*x45*x51 + 
    10*r5^2*r8^2*x45*x51 - 6*r5^3*r8^2*x45*x51 - 6*r5^2*r8^2*x45^2*x51 + 
    6*r5^3*r8^2*x45^2*x51 - 2*r3^3*x51^2 + 8*r3^3*r5*x51^2 - 
    6*r3^4*r5*x51^2 + 2*r3*r5^2*x51^2 + r3^2*r5^2*x51^2 - 
    14*r3^3*r5^2*x51^2 + 10*r3^4*r5^2*x51^2 - 4*r3*r5^3*x51^2 + 
    r3^2*r5^3*x51^2 + 6*r3^3*r5^3*x51^2 - 4*r3^4*r5^3*x51^2 + 
    r3^2*r5*r8*x51^2 - 2*r5^2*r8*x51^2 - 5*r3*r5^2*r8*x51^2 - 
    r3^2*r5^2*r8*x51^2 + 2*r5^3*r8*x51^2 + 6*r3*r5^3*r8*x51^2 - 
    r3*r8^2*x51^2 - 5*r5*r8^2*x51^2 + 2*r3*r5*r8^2*x51^2 + 
    14*r5^2*r8^2*x51^2 - r3*r5^2*r8^2*x51^2 - 9*r5^3*r8^2*x51^2 - 
    2*r3^3*x23*x51^2 + 2*r3^4*x23*x51^2 + r3^2*r8*x23*x51^2 + 
    6*r3^4*r5*x34*x51^2 + 6*r3^3*r5^2*x34*x51^2 - 10*r3^4*r5^2*x34*x51^2 - 
    4*r3^3*r5^3*x34*x51^2 + 4*r3^4*r5^3*x34*x51^2 - 2*r3^4*x23*x34*x51^2 - 
    2*r5^3*r8*x45*x51^2 - r5^2*r8^2*x45*x51^2 + 3*r5^3*r8^2*x45^2*x51^2 - 
    2*r3^3*r5^2*x51^3 + 2*r3^4*r5^2*x51^3 - 2*r3^2*r5^3*x51^3 + 
    4*r3^3*r5^3*x51^3 - 2*r3^4*r5^3*x51^3 + 2*r3^3*r8*x51^3 - 
    2*r3^3*r5*r8*x51^3 + 2*r3^2*r5^2*r8*x51^3 + 2*r5^3*r8*x51^3 + 
    2*r3*r5*r8^2*x51^3 + 4*r5^2*r8^2*x51^3 - 2*r3*r5^2*r8^2*x51^3 - 
    6*r5^3*r8^2*x51^3 - 2*r3^4*x23*x51^3 + 2*r3^3*r8*x23*x51^3 + 
    2*r5^3*r8^2*x45*x51^3 - r3*r5^2*r8^2*x51^4 - r5^3*r8^2*x51^4 + 
    2*r3^4*r8*x23*x51^4]*(-F[1, 1, 1]^2/16 - F[1, 1, 2]^2/16 + 
   (F[1, 1, 2]*F[1, 1, 8])/8 + (F[1, 1, 1]*F[1, 1, 9])/8 - 
   (F[1, 1, 8]*F[1, 1, 9])/8 - F[2, 1, 9]/8 - F[2, 1, 11]/8 - 
   tci[1, 1]^2/48 + (F[1, 1, 1]*tci[1, 2])/8 - (F[1, 1, 2]*tci[1, 2])/8) + 
 ratio[r3 - 5*r3^2 + 8*r3^3 - 6*r3^4 + 28*r3^2*r5 - 22*r3^3*r5 + 12*r3^4*r5 - 
    23*r3^2*r5^2 + 20*r3^3*r5^2 - 8*r3^4*r5^2 + 7*r3^2*r5^3 - 6*r3^3*r5^3 + 
    2*r3^4*r5^3 - 2*r7 - 8*r3*r7 - 8*r3^2*r7 - 2*r3^3*r7 + 10*r3*r5*r7 + 
    9*r3^2*r5*r7 + 2*r3^3*r5*r7 - 11*r3*r5^2*r7 - 3*r3^2*r5^2*r7 + 
    4*r3*r5^3*r7 + 3*r7*r8 - 10*r5*r7*r8 + 11*r5^2*r7*r8 - 4*r5^3*r7*r8 + 
    r5*r8^2 - 2*r5^2*r8^2 + r5^3*r8^2 - r7*r8^2 + 2*r5*r7*r8^2 - 
    r5^2*r7*r8^2 + r3^2*x23 - 2*r3^3*x23 + 2*r3^4*x23 + 2*r3*x34 - 
    10*r3^3*x34 + 16*r3^4*x34 + 36*r3^3*r5*x34 - 30*r3^4*r5*x34 - 
    24*r3^3*r5^2*x34 + 18*r3^4*r5^2*x34 + 6*r3^3*r5^3*x34 - 4*r3^4*r5^3*x34 - 
    r7*x34 - 6*r3*r7*x34 - 7*r3^2*r7*x34 - 2*r3^3*r7*x34 - r3^2*x23*x34 + 
    4*r3^3*x23*x34 - 6*r3^4*x23*x34 + 5*r3^2*x34^2 - 4*r3^3*x34^2 - 
    12*r3^4*x34^2 + 20*r3^4*r5*x34^2 - 10*r3^4*r5^2*x34^2 + 
    2*r3^4*r5^3*x34^2 - 4*r3*r7*x34^2 - 6*r3^2*r7*x34^2 - 2*r3^3*r7*x34^2 - 
    2*r3^3*x23*x34^2 + 6*r3^4*x23*x34^2 + 6*r3^3*x34^3 - 7*r3^2*r7*x34^3 - 
    2*r3^3*r7*x34^3 - 2*r3^4*x23*x34^3 + 2*r3^4*x34^4 - 6*r3^3*r7*x34^4 - 
    2*r3^4*r7*x34^5 + 2*r3^3*x51 - 8*r3*r5*x51 + 16*r3^2*r5*x51 - 
    12*r3^3*r5*x51 + 6*r3^4*r5*x51 + 16*r3*r5^2*x51 - 32*r3^2*r5^2*x51 + 
    30*r3^3*r5^2*x51 - 10*r3^4*r5^2*x51 - 8*r3*r5^3*x51 + 16*r3^2*r5^3*x51 - 
    14*r3^3*r5^3*x51 + 4*r3^4*r5^3*x51 + r7*x51 - 7*r3^2*r7*x51 - 
    4*r3^3*r7*x51 + 8*r3*r5*r7*x51 + 16*r3^2*r5*r7*x51 + 6*r3^3*r5*r7*x51 - 
    18*r3*r5^2*r7*x51 - 8*r3^2*r5^2*r7*x51 + 10*r3*r5^3*r7*x51 - 
    4*r3*r8*x51 + 4*r5*r8*x51 + 18*r3*r5*r8*x51 - 8*r5^2*r8*x51 - 
    24*r3*r5^2*r8*x51 + 4*r5^3*r8*x51 + 10*r3*r5^3*r8*x51 - 8*r5*r7*r8*x51 + 
    18*r5^2*r7*r8*x51 - 10*r5^3*r7*r8*x51 - 6*r5*r8^2*x51 + 
    10*r5^2*r8^2*x51 - 4*r5^3*r8^2*x51 + 2*r5*r7*r8^2*x51 - 
    2*r5^2*r7*r8^2*x51 - r3^2*x23*x51 + 2*r3^3*x23*x51 - 2*r3^4*x23*x51 - 
    2*r3^3*x34*x51 - 4*r3^2*r5*x34*x51 + 12*r3^3*r5*x34*x51 - 
    8*r3^4*r5*x34*x51 + 8*r3^2*r5^2*x34*x51 - 20*r3^3*r5^2*x34*x51 + 
    12*r3^4*r5^2*x34*x51 - 4*r3^2*r5^3*x34*x51 + 8*r3^3*r5^3*x34*x51 - 
    4*r3^4*r5^3*x34*x51 + 4*r3*r7*x34*x51 - 2*r3^3*r7*x34*x51 - 
    2*r3^3*x23*x34*x51 + 4*r3^4*x23*x34*x51 + 7*r3^2*r7*x34^2*x51 - 
    2*r3^4*x23*x34^2*x51 + 6*r3^3*r7*x34^3*x51 + 2*r3^4*r7*x34^4*x51 - 
    2*r3^3*x51^2 + 4*r3^3*r5*x51^2 + 4*r3*r5^2*x51^2 - 10*r3^2*r5^2*x51^2 + 
    6*r3^3*r5^2*x51^2 - 2*r3^4*r5^2*x51^2 - 8*r3*r5^3*x51^2 + 
    12*r3^2*r5^3*x51^2 - 8*r3^3*r5^3*x51^2 + 2*r3^4*r5^3*x51^2 - 
    2*r3^3*r7*x51^2 + 7*r3^2*r5*r7*x51^2 + 6*r3^3*r5*r7*x51^2 - 
    5*r3*r5^2*r7*x51^2 - 7*r3^2*r5^2*r7*x51^2 + 6*r3*r5^3*r7*x51^2 - 
    r3*r7^2*x51^2 - r5*r7^2*x51^2 + 2*r3*r5*r7^2*x51^2 - r3*r5^2*r7^2*x51^2 + 
    r3^2*r5*r8*x51^2 - 8*r5^2*r8*x51^2 - 5*r3*r5^2*r8*x51^2 - 
    r3^2*r5^2*r8*x51^2 + 8*r5^3*r8*x51^2 + 6*r3*r5^3*r8*x51^2 + 
    6*r5^2*r7*r8*x51^2 - 6*r5^3*r7*r8*x51^2 + r5^2*r7^2*r8*x51^2 - 
    r3*r8^2*x51^2 - r5*r8^2*x51^2 + 2*r3*r5*r8^2*x51^2 + 14*r5^2*r8^2*x51^2 - 
    r3*r5^2*r8^2*x51^2 - 12*r5^3*r8^2*x51^2 - r5^2*r7*r8^2*x51^2 - 
    2*r3^3*x23*x51^2 + 2*r3^4*x23*x51^2 + r3^2*r8*x23*x51^2 - 
    2*r3^4*x23*x34*x51^2 + 2*r3^3*r5*r7*x51^3 - 2*r3^2*r5^2*r7*x51^3 + 
    2*r3*r5*r7^2*x51^3 + 2*r5^2*r7^2*x51^3 - 2*r3*r5^2*r7^2*x51^3 + 
    2*r3^3*r8*x51^3 - 2*r3^3*r5*r8*x51^3 + 2*r3^2*r5^2*r8*x51^3 + 
    4*r5^3*r8*x51^3 - 4*r5^3*r7*r8*x51^3 + 2*r3*r5*r8^2*x51^3 + 
    2*r5^2*r8^2*x51^3 - 2*r3*r5^2*r8^2*x51^3 - 8*r5^3*r8^2*x51^3 - 
    2*r3^4*x23*x51^3 + 2*r3^3*r8*x23*x51^3 - r3*r5^2*r7^2*x51^4 - 
    r5^3*r7^2*x51^4 - r3*r5^2*r8^2*x51^4 - r5^3*r8^2*x51^4 + 
    2*r3^4*r8*x23*x51^4]*((F[1, 1, 1]*F[1, 1, 3])/16 - 
   (F[1, 1, 2]*F[1, 1, 3])/16 - (F[1, 1, 1]*F[1, 1, 5])/16 + 
   (F[1, 1, 2]*F[1, 1, 5])/16 + (F[1, 1, 3]*F[1, 1, 5])/16 + 
   (F[1, 1, 1]*F[1, 1, 8])/16 - (F[1, 1, 2]*F[1, 1, 8])/16 - 
   (F[1, 1, 3]*F[1, 1, 8])/16 - (F[1, 1, 1]*F[1, 1, 9])/16 + 
   (F[1, 1, 2]*F[1, 1, 9])/16 - (F[1, 1, 5]*F[1, 1, 9])/16 + 
   (F[1, 1, 8]*F[1, 1, 9])/16 - (F[1, 1, 1]*tci[1, 2])/8 + 
   (F[1, 1, 2]*tci[1, 2])/8) + 
 ratio[-r5 + 2*r3*r5 + 2*r5^2 - r5^3 - 2*r3*r5^3 + r3*r5^4 + r7 - 2*r3*r7 - 
    6*r5*r7 + 3*r3*r5*r7 + 6*r5^2*r7 - r3*r5^2*r7 - 2*r5^3*r7 - r5*r8 + 
    2*r5^2*r8 - r5^3*r8 + r7*r8 - 2*r5*r7*r8 + r5^2*r7*r8 + r3*r5*x34 - 
    3*r3*r5^2*x34 + 3*r3*r5^3*x34 - r3*r5^4*x34 - r3*r7*x34 + 2*r5*r7*x45 - 
    4*r5^2*r7*x45 + 2*r5^3*r7*x45 + r5^2*r8*x45 - 2*r5^3*r8*x45 + 
    r5^4*r8*x45 - r5^2*x51 + r3*r5^2*x51 + 4*r5^3*x51 - 2*r3*r5^3*x51 - 
    3*r5^4*x51 + r3*r5^4*x51 + 2*r3*r5*r7*x51 - r3*r5^2*r7*x51 + 
    2*r5^2*r8*x51 - 2*r5^3*r8*x51 - 2*r5*r7*r8*x51 + 2*r5^2*r7*r8*x51 + 
    2*r5^2*r7*x45*x51 - 8*r5^3*r7*x45*x51 + 6*r5^4*r7*x45*x51 + 
    r5^2*r8*x45*x51 - 4*r5^3*r8*x45*x51 + 3*r5^4*r8*x45*x51 - r5^3*r7*x51^2 - 
    r5^3*r8*x51^2 + r5^2*r7*r8*x51^2 - r5^3*r7*x45*x51^2 - 
    2*r5^2*r7^2*x45*x51^2 + 4*r5^3*r7^2*x45*x51^2 - 2*r5*r7^3*x45*x51^2 + 
    2*r5^2*r7^3*x45*x51^2 - 2*r5^3*r8*x45*x51^2 + 3*r5^4*r8*x45*x51^2 - 
    2*r5^3*r7^2*x45^2*x51^2 + 6*r5^4*r7^2*x45^2*x51^2 - 
    4*r5^2*r7^3*x45^2*x51^2 + 2*r5^3*r7^3*x45^2*x51^2 - 
    2*r5^3*r7^3*x45^3*x51^2 + r5^4*r7*x45*x51^3 + 2*r5^2*r7^3*x45*x51^3 + 
    r5^4*r8*x45*x51^3 + 4*r5^3*r7^3*x45^2*x51^3 + 2*r5^4*r7^3*x45^3*x51^3]*
  (F[1, 1, 4]^2/16 - F[1, 1, 6]^2/16 - (F[1, 1, 4]*F[1, 1, 10])/8 + 
   (F[1, 1, 6]*F[1, 1, 10])/8 + F[2, 1, 6]/8 - F[2, 1, 14]/8 + 
   tci[1, 1]^2/48 + (F[1, 1, 10]*tci[1, 2])/8 - (F[1, 2, 3]*tci[1, 2])/8) + 
 ratio[-r3 - 3*r5 + 4*r3*r5 + 10*r5^2 - 6*r3*r5^2 - 11*r5^3 + 4*r3*r5^3 + 
    4*r5^4 - r3*r5^4 + 2*r8 - 7*r5*r8 + 8*r5^2*r8 - 3*r5^3*r8 - r3*r5*x34 + 
    3*r3*r5^2*x34 - 3*r3*r5^3*x34 + r3*r5^4*x34 + 4*r5*r8*x45 - 
    15*r5^2*r8*x45 + 18*r5^3*r8*x45 - 7*r5^4*r8*x45 - 2*r8^2*x45 + 
    8*r5*r8^2*x45 - 10*r5^2*r8^2*x45 + 4*r5^3*r8^2*x45 - 2*r5*r8^2*x45^2 + 
    10*r5^2*r8^2*x45^2 - 14*r5^3*r8^2*x45^2 + 6*r5^4*r8^2*x45^2 - 
    2*r5*r8^3*x45^2 + 4*r5^2*r8^3*x45^2 - 2*r5^3*r8^3*x45^2 - 
    2*r5^2*r8^3*x45^3 + 4*r5^3*r8^3*x45^3 - 2*r5^4*r8^3*x45^3 + r5^2*x51 - 
    r3*r5^2*x51 - 4*r5^3*x51 + 2*r3*r5^3*x51 + 3*r5^4*x51 - r3*r5^4*x51 + 
    r3*r8*x51 - 2*r3*r5*r8*x51 + 2*r5^2*r8*x51 + r3*r5^2*r8*x51 - 
    2*r5^3*r8*x51 - 3*r5^2*r8*x45*x51 + 12*r5^3*r8*x45*x51 - 
    9*r5^4*r8*x45*x51 + 4*r5*r8^2*x45*x51 - 12*r5^2*r8^2*x45*x51 + 
    8*r5^3*r8^2*x45*x51 - 2*r8^3*x45*x51 + 4*r5*r8^3*x45*x51 - 
    2*r5^2*r8^3*x45*x51 + 4*r5^2*r8^2*x45^2*x51 - 16*r5^3*r8^2*x45^2*x51 + 
    12*r5^4*r8^2*x45^2*x51 - 4*r5*r8^3*x45^2*x51 + 12*r5^2*r8^3*x45^2*x51 - 
    8*r5^3*r8^3*x45^2*x51 - 2*r5^2*r8^3*x45^3*x51 + 8*r5^3*r8^3*x45^3*x51 - 
    6*r5^4*r8^3*x45^3*x51 - r5^3*r7*x51^2 - r5^3*r8*x51^2 + 
    r5^2*r7*r8*x51^2 + r5^3*r7*x45*x51^2 + 2*r5^3*r8*x45*x51^2 - 
    3*r5^4*r8*x45*x51^2 - 2*r5^2*r8^2*x45*x51^2 + 4*r5^3*r8^2*x45*x51^2 + 
    4*r5*r8^3*x45*x51^2 - 4*r5^2*r8^3*x45*x51^2 - 2*r5^3*r8^2*x45^2*x51^2 + 
    6*r5^4*r8^2*x45^2*x51^2 + 8*r5^2*r8^3*x45^2*x51^2 - 
    10*r5^3*r8^3*x45^2*x51^2 + 4*r5^3*r8^3*x45^3*x51^2 - 
    6*r5^4*r8^3*x45^3*x51^2 - r5^4*r7*x45*x51^3 - r5^4*r8*x45*x51^3 - 
    2*r5^2*r8^3*x45*x51^3 - 4*r5^3*r8^3*x45^2*x51^3 - 
    2*r5^4*r8^3*x45^3*x51^3]*(-F[1, 1, 4]^2/16 + F[1, 1, 6]^2/16 + 
   (F[1, 1, 4]*F[1, 1, 7])/8 - (F[1, 1, 6]*F[1, 1, 7])/8 - F[2, 1, 6]/8 + 
   F[2, 1, 8]/8 - tci[1, 1]^2/48 - (F[1, 1, 7]*tci[1, 2])/8 + 
   (F[1, 2, 3]*tci[1, 2])/8) + 
 ratio[-r5 + r6 - r5*r6 + 4*r3*r5*x34 - 2*r3*r5^2*x34 - 4*r3*r6*x34 + 
    6*r3*r5*r6*x34 - 2*r3*r5^2*r6*x34 - 2*r3*r5*x51 - 2*r5^2*x51 + 
    2*r3*r5^2*x51 - r6*x51 + 2*r3*r6*x34*x51]*
  (F[1, 1, 1]^2/16 + F[1, 1, 2]^2/16 - (F[1, 1, 2]*F[1, 1, 4])/8 + 
   (F[1, 1, 4]*F[1, 1, 5])/8 - (F[1, 1, 2]*F[1, 1, 8])/8 - 
   (F[1, 1, 1]*F[1, 1, 9])/8 + (F[1, 1, 2]*F[1, 1, 9])/8 - 
   (F[1, 1, 5]*F[1, 1, 9])/8 + (F[1, 1, 8]*F[1, 1, 9])/8 + F[2, 1, 3]/8 + 
   F[2, 1, 9]/8 - (F[1, 1, 1]*tci[1, 2])/8 + (F[1, 1, 2]*tci[1, 2])/8 - 
   (F[1, 1, 5]*tci[1, 2])/8 + (F[1, 2, 6]*tci[1, 2])/8) + 
 ratio[-3 + 4*r3 - 3*r4 + 4*r3*r4 + 7*r5 - 8*r3*r5 + 7*r4*r5 - 6*r3*r4*r5 - 
    4*r5^2 + 4*r3*r5^2 - 2*r4*r5^2 + 2*r3*r4*r5^2 + 3*r4*x23 - 2*r3*r4*x23 - 
    2*r3*x34 - 2*r4*x34 - 4*r3*r4*x34 + 4*r3*r5*x34 + 6*r3*r4*r5*x34 - 
    2*r3*r5^2*x34 - 2*r3*r4*r5^2*x34 + 2*r3*r4*x23*x34 - 3*r4*x45 - 
    2*r3*r5*x51 - 2*r5^2*x51 + 2*r3*r5^2*x51]*
  (-F[1, 1, 1]^2/16 + F[1, 1, 2]^2/16 - (F[1, 1, 2]*F[1, 1, 4])/8 + 
   (F[1, 1, 2]*F[1, 1, 8])/8 + (F[1, 1, 1]*F[1, 1, 9])/8 - 
   (F[1, 1, 2]*F[1, 1, 9])/8 + (F[1, 1, 4]*F[1, 1, 9])/8 - 
   (F[1, 1, 8]*F[1, 1, 9])/8 + F[2, 1, 3]/8 - F[2, 1, 9]/8 - tci[1, 1]^2/48 + 
   (F[1, 1, 1]*tci[1, 2])/8 - (F[1, 1, 2]*tci[1, 2])/8 - 
   (F[1, 1, 9]*tci[1, 2])/8 + (F[1, 2, 6]*tci[1, 2])/8) + 
 ratio[-r3^2 - r3*r5 + 3*r3^2*r5 + 2*r3*r5^2 - 3*r3^2*r5^2 - r3*r5^3 + 
    r3^2*r5^3 + r3^2*x34 - 3*r3^2*r5*x34 + 3*r3^2*r5^2*x34 - r3^2*r5^3*x34 + 
    r3*r5*x51 + r3^2*r5*x51 - 2*r3^2*r5^2*x51 - r3*r5^3*x51 + r3^2*r5^3*x51]*
  (F[1, 1, 1]^2/8 - (F[1, 1, 1]*F[1, 1, 2])/4 + F[1, 1, 2]^2/8 + 
   F[2, 1, 2]/4 + F[2, 1, 3]/4 + tci[1, 1]^2/24 - (F[1, 1, 1]*tci[1, 2])/4 + 
   (F[1, 2, 6]*tci[1, 2])/4) + 
 ratio[-7*r5 + 24*r3*r5 + 5*r5^2 - 16*r3*r5^2 - r5^3 + 4*r3*r5^3 + 4*r7 - 
    24*r3*r7 - 25*r5*r7 + 40*r3*r5*r7 + 9*r5^2*r7 - 20*r3*r5^2*r7 + 
    4*r3*r5^3*r7 + 3*r7*r8 - 10*r5*r7*r8 + 11*r5^2*r7*r8 - 4*r5^3*r7*r8 + 
    r5*r8^2 - 2*r5^2*r8^2 + r5^3*r8^2 - r7*r8^2 + 2*r5*r7*r8^2 - 
    r5^2*r7*r8^2 + 2*r3*x34 + 6*r3*r5*x34 - 6*r3*r5^2*x34 + 2*r3*r5^3*x34 - 
    r7*x34 - 12*r3*r7*x34 - 4*r3*r7*x34^2 + 6*r5*r7*x45 - 3*r5^2*r7*x45 - 
    2*r3*r5*x51 - 4*r5^2*x51 + 4*r3*r5^2*x51 + 4*r5^3*x51 - 2*r3*r5^3*x51 + 
    r7*x51 + 24*r3*r5*r7*x51 - 16*r5^2*r7*x51 - 16*r3*r5^2*r7*x51 + 
    12*r5^3*r7*x51 + 4*r3*r5^3*r7*x51 - 2*r3*r8*x51 + 2*r5*r8*x51 + 
    8*r3*r5*r8*x51 - 4*r5^2*r8*x51 - 10*r3*r5^2*r8*x51 + 2*r5^3*r8*x51 + 
    4*r3*r5^3*r8*x51 - 8*r5*r7*r8*x51 + 18*r5^2*r7*r8*x51 - 
    10*r5^3*r7*r8*x51 - 2*r5*r8^2*x51 + 2*r5^2*r8^2*x51 + 2*r5*r7*r8^2*x51 - 
    2*r5^2*r7*r8^2*x51 + 4*r3*r7*x34*x51 + 6*r5^2*r7*x45*x51 - 
    6*r5^3*r7*x45*x51 - 4*r5^2*r7*x51^2 + 4*r5^3*r7*x51^2 - 2*r5*r7^2*x51^2 + 
    4*r5^2*r7^2*x51^2 - 3*r5^3*r7^2*x51^2 - 4*r5^2*r8*x51^2 + 
    4*r5^3*r8*x51^2 + 6*r5^2*r7*r8*x51^2 - 6*r5^3*r7*r8*x51^2 + 
    r5^2*r7^2*r8*x51^2 + 4*r5^2*r8^2*x51^2 - 3*r5^3*r8^2*x51^2 - 
    r5^2*r7*r8^2*x51^2 + 2*r5^3*r7*x45*x51^2 - 5*r5^2*r7^2*x45*x51^2 + 
    6*r5^3*r7^2*x45*x51^2 - 3*r5^3*r7^2*x45^2*x51^2 + 2*r5^3*r7*x51^3 + 
    2*r5^2*r7^2*x51^3 - 2*r5^3*r7^2*x51^3 + 2*r5^3*r8*x51^3 - 
    4*r5^3*r7*r8*x51^3 - 2*r5^3*r8^2*x51^3 + 2*r5^3*r7^2*x45*x51^3]*
  (-F[1, 1, 1]^2/16 + (F[1, 1, 2]*F[1, 1, 3])/8 - F[1, 1, 3]^2/16 - 
   (F[1, 1, 2]*F[1, 1, 5])/8 + (F[1, 1, 2]*F[1, 1, 8])/8 + 
   (F[1, 1, 1]*F[1, 1, 9])/8 - (F[1, 1, 2]*F[1, 1, 9])/8 + 
   (F[1, 1, 5]*F[1, 1, 9])/8 - (F[1, 1, 8]*F[1, 1, 9])/8 - F[2, 1, 5]/8 - 
   F[2, 1, 9]/8 - tci[1, 1]^2/48 + (F[1, 1, 1]*tci[1, 2])/8 - 
   (F[1, 1, 2]*tci[1, 2])/4 + (F[1, 2, 7]*tci[1, 2])/8) + 
 ratio[-1 + 18*r3 + 17*r5 - 36*r3*r5 - 19*r5^2 + 28*r3*r5^2 + 7*r5^3 - 
    8*r3*r5^3 - 2*r7 - 8*r3*r7 + 18*r3*r5*r7 - 14*r3*r5^2*r7 + 4*r3*r5^3*r7 - 
    14*r8 + 35*r5*r8 - 31*r5^2*r8 + 10*r5^3*r8 + 3*r7*r8 - 10*r5*r7*r8 + 
    11*r5^2*r7*r8 - 4*r5^3*r7*r8 + r5*r8^2 - 2*r5^2*r8^2 + r5^3*r8^2 - 
    r7*r8^2 + 2*r5*r7*r8^2 - r5^2*r7*r8^2 - 4*r3*x23 + 3*r8*x23 - 2*r3*x34 + 
    6*r3*r5*x34 - 6*r3*r5^2*x34 + 2*r3*r5^3*x34 - 2*r3*r7*x34 - 
    14*r5*r8*x45 + 19*r5^2*r8*x45 - 8*r5^3*r8*x45 + 3*r8^2*x45 - 
    10*r5*r8^2*x45 + 11*r5^2*r8^2*x45 - 4*r5^3*r8^2*x45 + 3*r5*r8^2*x45^2 - 
    6*r5^2*r8^2*x45^2 + 3*r5^3*r8^2*x45^2 - 2*r3*r5*x51 - 4*r5^2*x51 + 
    4*r3*r5^2*x51 + 4*r5^3*x51 - 2*r3*r5^3*x51 + 8*r3*r5*r7*x51 - 
    10*r3*r5^2*r7*x51 + 4*r3*r5^3*r7*x51 - 12*r3*r8*x51 - 2*r5*r8*x51 + 
    24*r3*r5*r8*x51 - 12*r5^2*r8*x51 - 16*r3*r5^2*r8*x51 + 10*r5^3*r8*x51 + 
    4*r3*r5^3*r8*x51 - 8*r5*r7*r8*x51 + 18*r5^2*r7*r8*x51 - 
    10*r5^3*r7*r8*x51 + 2*r8^2*x51 - 6*r5*r8^2*x51 + 4*r5^2*r8^2*x51 + 
    2*r5*r7*r8^2*x51 - 2*r5^2*r7*r8^2*x51 + 4*r3*r8*x23*x51 + 
    10*r5^2*r8*x45*x51 - 10*r5^3*r8*x45*x51 - 4*r5*r8^2*x45*x51 + 
    10*r5^2*r8^2*x45*x51 - 6*r5^3*r8^2*x45*x51 - 6*r5^2*r8^2*x45^2*x51 + 
    6*r5^3*r8^2*x45^2*x51 - 2*r5^2*r7*x51^2 + 2*r5^3*r7*x51^2 + 
    2*r5^2*r7^2*x51^2 - 3*r5^3*r7^2*x51^2 - 2*r5^2*r8*x51^2 + 
    2*r5^3*r8*x51^2 + 6*r5^2*r7*r8*x51^2 - 6*r5^3*r7*r8*x51^2 + 
    r5^2*r7^2*r8*x51^2 - 4*r5*r8^2*x51^2 + 8*r5^2*r8^2*x51^2 - 
    3*r5^3*r8^2*x51^2 - r5^2*r7*r8^2*x51^2 - 2*r5^3*r8*x45*x51^2 - 
    r5^2*r8^2*x45*x51^2 + 3*r5^3*r8^2*x45^2*x51^2 + 2*r5^3*r7*x51^3 - 
    2*r5^3*r7^2*x51^3 + 2*r5^3*r8*x51^3 - 4*r5^3*r7*r8*x51^3 + 
    2*r5^2*r8^2*x51^3 - 2*r5^3*r8^2*x51^3 + 2*r5^3*r8^2*x45*x51^3]*
  (F[1, 1, 1]^2/16 - F[1, 1, 8]^2/16 - (F[1, 1, 1]*F[1, 1, 9])/8 + 
   (F[1, 1, 8]*F[1, 1, 9])/8 + F[2, 1, 9]/8 - F[2, 1, 12]/8 - 
   (F[1, 1, 1]*tci[1, 2])/8 + (F[1, 2, 9]*tci[1, 2])/8)
