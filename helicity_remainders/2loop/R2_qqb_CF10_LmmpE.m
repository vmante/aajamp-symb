(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

-(40*F[1, 1, 2] - 15*F[1, 1, 2]^2 + 40*F[1, 1, 6] - 6*F[1, 1, 2]*F[1, 1, 6] - 
 15*F[1, 1, 6]^2 + 2*tci[1, 1]^2)/864
