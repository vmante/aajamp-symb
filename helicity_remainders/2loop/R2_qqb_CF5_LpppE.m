(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

(19/144 - F[1, 1, 9]/16)*ratio[-2 + 2*r2 - 4*r3 - 2*r5 + 6*r3*r5 + 2*r5^2 - 
    2*r3*r5^2 + 2*r8 - 2*r2*r8 + 2*x23 + 2*r2*x23 + 2*r3*x23 + 2*r8*x23 - 
    2*r2*r8*x23 + x34 + 8*r3*x34 + 6*r5*x34 - 12*r3*r5*x34 - 3*r5^2*x34 + 
    4*r3*r5^2*x34 - 4*r3*x23*x34 - 2*x34^2 - 4*r3*x34^2 + 6*r3*r5*x34^2 - 
    2*r3*r5^2*x34^2 + 2*r3*x23*x34^2 - 4*x45 - 2*r2*x45 + 3*r5*x45 - 
    r5^2*x45 + 2*r2*r8*x45 - 3*r5*r8*x45 + r5^2*r8*x45 + 2*x23*x45 - 
    2*x45^2 + 2*r9*x23*x45^2 - 2*r9*x45^3 - 4*r2*x51 - 4*r3*x51 - 5*r5*x51 + 
    2*r2*r5*x51 + 8*r3*r5*x51 + 5*r5^2*x51 - 4*r3*r5^2*x51 + r7*x51 + 
    r5*r7*x51 - 3*r8*x51 + r5*r8*x51 + 2*r5*r9*x51 - 2*r7*r9*x51 + 
    4*x34*x51 + 4*r3*x34*x51 - 2*r5*x34*x51 - 8*r3*r5*x34*x51 - 
    2*r5^2*x34*x51 + 4*r3*r5^2*x34*x51 + 4*r2*x45*x51 + r5*x45*x51 - 
    4*r2*r5*x45*x51 - r5^2*x45*x51 + r5*r7*x45*x51 - 3*r5*r8*x45*x51 + 
    2*r5^2*r8*x45*x51 + 4*r9*x45*x51 - 4*r5*r9*x45*x51 + 2*r2*r5*x45^2*x51 - 
    4*r9*x45^2*x51 + 2*r5*r9*x45^2*x51 + 2*r2*r5*x51^2 + 2*r3*r5*x51^2 + 
    2*r5^2*x51^2 - 2*r3*r5^2*x51^2 + r5*r7*x51^2 + r5*r8*x51^2 + 
    2*r5*r9*x51^2 + 2*r5*x45*x51^2 - 4*r2*r5*x45*x51^2 + r5^2*r7*x45*x51^2 + 
    r5^2*r8*x45*x51^2 - 4*r5*r9*x45*x51^2 + 2*r2*r5*x45^2*x51^2 + 
    2*r5*r9*x45^2*x51^2] + (F[1, 1, 6]/96 - F[1, 1, 9]/96)*
  ratio[6 - 6*r2 + 4*r3 - 6*r5 - 6*r3*r5 + 2*r5^2 + 2*r3*r5^2 - 6*r8 + 
    6*r2*r8 - 2*x23 - 6*r2*x23 - 2*r3*x23 - 6*r8*x23 + 6*r2*r8*x23 - 
    8*x23^2 + 8*x23^3 + 4*r3*x23^3 - x34 - 8*r3*x34 - 6*r5*x34 + 
    12*r3*r5*x34 + 3*r5^2*x34 - 4*r3*r5^2*x34 + 4*r3*x23*x34 + 12*x23^2*x34 - 
    4*r3*x23^2*x34 - 4*r3*x23^3*x34 + 6*x34^2 + 4*r3*x34^2 - 6*r3*r5*x34^2 + 
    2*r3*r5^2*x34^2 - 8*x23*x34^2 + 2*r3*x23*x34^2 + 4*r3*x23^2*x34^2 + 
    4*x34^3 - 8*r20*x34^3 - 4*r3*x34^3 - 4*r3*x23*x34^3 + 4*r3*x34^4 + 
    4*r20*r7*x34^4 + 4*r3*r7*x34^4 - 4*r20*r7*x34^5 - 4*r3*r7*x34^5 + 8*x45 + 
    6*r2*x45 - 4*r20*x45 - 3*r5*x45 + 4*r20*r5*x45 + r5^2*x45 - 6*r2*r8*x45 + 
    11*r5*r8*x45 - 5*r5^2*r8*x45 + 2*x23*x45 - 24*x23^2*x45 + 
    8*r9*x23^2*x45 + 12*r23*x23^3*x45 + 4*x34*x45 - 4*r20*x34*x45 - 
    12*x23*x34*x45 + 4*x34^2*x45 + 2*x45^2 + 24*x23*x45^2 + 8*r23*x23*x45^2 - 
    26*r9*x23*x45^2 - 24*r23*x23^2*x45^2 - 12*r9*x23^2*x45^2 + 
    4*r23*r9*x23^2*x45^2 + 4*r23*r9*x23^3*x45^2 + 4*x34*x45^2 - 8*x45^3 - 
    8*r23*x45^3 + 18*r9*x45^3 + 12*r23*x23*x45^3 + 24*r9*x23*x45^3 - 
    8*r23*r9*x23*x45^3 - 12*r23*r9*x23^2*x45^3 - 12*r9*x45^4 + 
    4*r23*r9*x45^4 + 12*r23*r9*x23*x45^4 - 4*r23*r9*x45^5 + 8*r2*x51 + 
    4*r3*x51 + 5*r5*x51 - 2*r2*r5*x51 - 8*r3*r5*x51 - 5*r5^2*x51 + 
    4*r3*r5^2*x51 - r7*x51 - 5*r5*r7*x51 + 11*r8*x51 - 5*r5*r8*x51 - 
    4*r9*x51 - 2*r5*r9*x51 + 6*r7*r9*x51 + 4*r2*x23*x51 + 4*r9*x23*x51 - 
    4*r3*x23^2*x51 - 12*r23*x23^3*x51 - 4*r3*x23^3*x51 - 4*x34*x51 - 
    4*r3*x34*x51 + 2*r5*x34*x51 + 8*r3*r5*x34*x51 + 2*r5^2*x34*x51 - 
    4*r3*r5^2*x34*x51 + 12*x23*x34*x51 - 16*x34^2*x51 + 8*r20*x34^2*x51 + 
    4*r3*x34^2*x51 + 4*r3*x23*x34^2*x51 + 12*r20*x34^3*x51 - 8*r3*x34^3*x51 - 
    8*r20*r7*x34^3*x51 - 8*r3*r7*x34^3*x51 + 12*r20*r7*x34^4*x51 + 
    12*r3*r7*x34^4*x51 - 8*r2*x45*x51 + 4*r23*x45*x51 - r5*x45*x51 + 
    4*r2*r5*x45*x51 + 4*r20*r5*x45*x51 - 4*r23*r5*x45*x51 + r5^2*x45*x51 - 
    r5*r7*x45*x51 + 11*r5*r8*x45*x51 - 10*r5^2*r8*x45*x51 - 8*r9*x45*x51 + 
    4*r5*r9*x45*x51 + 12*x23*x45*x51 - 4*r23*x23*x45*x51 - 
    16*r9*x23*x45*x51 + 12*r23*x23^2*x45*x51 - 12*x34*x45*x51 + 
    4*r20*x34*x45*x51 - 8*x45^2*x51 - 2*r2*r5*x45^2*x51 + 24*r9*x45^2*x51 - 
    2*r5*r9*x45^2*x51 + 12*r9*x23*x45^2*x51 - 12*r9*x45^3*x51 - 4*r2*x51^2 - 
    2*r2*r5*x51^2 - 2*r3*r5*x51^2 - 2*r5^2*x51^2 + 2*r3*r5^2*x51^2 - 
    5*r5*r7*x51^2 - 5*r5*r8*x51^2 - 4*r9*x51^2 - 2*r5*r9*x51^2 + 
    4*r23*r8*x23^2*x51^2 + 4*r3*r8*x23^2*x51^2 + 4*r23*r8*x23^3*x51^2 + 
    4*r3*r8*x23^3*x51^2 + 12*x34*x51^2 - 24*r20*x34^2*x51^2 + 
    4*r3*x34^2*x51^2 + 4*r20*r7*x34^2*x51^2 + 4*r3*r7*x34^2*x51^2 - 
    12*r20*r7*x34^3*x51^2 - 12*r3*r7*x34^3*x51^2 + 12*x45*x51^2 + 
    8*r2*x45*x51^2 - 2*r5*x45*x51^2 + 4*r2*r5*x45*x51^2 - 
    5*r5^2*r7*x45*x51^2 - 5*r5^2*r8*x45*x51^2 + 8*r9*x45*x51^2 + 
    4*r5*r9*x45*x51^2 - 12*r20*x34*x45*x51^2 + 4*r2*r20*x45^2*x51^2 - 
    2*r2*r5*x45^2*x51^2 - 4*r9*x45^2*x51^2 - 2*r5*r9*x45^2*x51^2 + 
    12*r20*x34*x51^3 + 4*r20*r7*x34^2*x51^3 + 4*r3*r7*x34^2*x51^3 + 
    12*r20*x45*x51^3 + 4*r2*r20*x45^2*x51^3] + 
 (F[1, 1, 9]/24 - F[1, 1, 10]/24)*ratio[-1 + 5*r15 - 2*r3 - 6*r5 - r15*r5 + 
    3*r3*r5 + 2*r5^2 - r3*r5^2 + x23 + r3*x23 + 4*x34 - 10*r15*x34 + 
    5*r3*x34 + 7*r5*x34 - 7*r3*r5*x34 - 2*r5^2*x34 + 2*r3*r5^2*x34 - 
    2*x23*x34 - 3*r3*x23*x34 - 5*x34^2 + 10*r15*x34^2 - 3*r3*x34^2 + 
    4*r3*r5*x34^2 - r3*r5^2*x34^2 + x23*x34^2 + 3*r3*x23*x34^2 + 2*x34^3 - 
    5*r15*x34^3 - 2*r20*x34^3 - r3*x34^3 - r3*x23*x34^3 + r15*x34^4 + 
    r3*x34^4 + r20*r7*x34^4 + r3*r7*x34^4 - r20*r7*x34^5 - r3*r7*x34^5 - 
    x45 - 2*r15*x45 - r20*x45 + 3*r5*x45 + r15*r5*x45 + r20*r5*x45 - 
    r5^2*x45 + x23*x45 + 3*x34*x45 - r20*x34*x45 - x23*x34*x45 - 
    2*x34^2*x45 + 2*r15*x34^2*x45 - r15*x34^3*x45 - x45^2 + x23*x45^2 + 
    2*x34*x45^2 - r15*x34*x45^2 + r15*x34^2*x45^2 - 2*x45^3 + r9*x45^3 + 
    r9*x23*x45^3 - r15*x34*x45^3 + r15*x45^4 - 2*r9*x45^4 - r15*r9*x45^4 + 
    r15*r9*x45^5 - 2*x51 + 4*r15*x51 - 3*r3*x51 - 5*r5*x51 - r15*r5*x51 + 
    5*r3*r5*x51 + 3*r5^2*x51 - 2*r3*r5^2*x51 + x23*x51 + r3*x23*x51 + 
    7*x34*x51 - 6*r15*x34*x51 + 4*r3*x34*x51 - r5*x34*x51 - 6*r3*r5*x34*x51 - 
    r5^2*x34*x51 + 2*r3*r5^2*x34*x51 - x23*x34*x51 - 2*r3*x23*x34*x51 - 
    5*x34^2*x51 + 4*r15*x34^2*x51 + 2*r20*x34^2*x51 + r3*x34^2*x51 + 
    r3*x23*x34^2*x51 - r15*x34^3*x51 + 3*r20*x34^3*x51 - 2*r3*x34^3*x51 - 
    2*r20*r7*x34^3*x51 - 2*r3*r7*x34^3*x51 + 3*r20*r7*x34^4*x51 + 
    3*r3*r7*x34^4*x51 - 2*x45*x51 + r15*r5*x45*x51 + r20*r5*x45*x51 - 
    r5^2*x45*x51 + x23*x45*x51 + 5*x34*x45*x51 - 5*r15*x34*x45*x51 + 
    r20*x34*x45*x51 + 4*r15*x34^2*x45*x51 - 5*x45^2*x51 + 3*r9*x45^2*x51 + 
    r9*x23*x45^2*x51 - 4*r15*x34*x45^2*x51 + 4*r15*x45^3*x51 - 
    5*r9*x45^3*x51 - 4*r15*r9*x45^3*x51 + 4*r15*r9*x45^4*x51 - x51^2 - 
    r3*x51^2 + r15*r5*x51^2 + r2*r5*x51^2 + 2*r3*r5*x51^2 + r5^2*x51^2 - 
    r15*r5^2*x51^2 - r2*r5^2*x51^2 - r3*r5^2*x51^2 + 3*x34*x51^2 - 
    6*r20*x34^2*x51^2 + r3*x34^2*x51^2 + r20*r7*x34^2*x51^2 + 
    r3*r7*x34^2*x51^2 - 3*r20*r7*x34^3*x51^2 - 3*r3*r7*x34^3*x51^2 - 
    3*x45*x51^2 - 2*r15*x45*x51^2 + r5*x45*x51^2 - 2*r15*r5*x45*x51^2 + 
    2*r15*r5^2*x45*x51^2 + 2*r2*r5^2*x45*x51^2 + 2*r9*x45*x51^2 + 
    r5*r9*x45*x51^2 - 3*r15*x34*x45*x51^2 - 3*r20*x34*x45*x51^2 + 
    6*r15*x45^2*x51^2 + r2*r20*x45^2*x51^2 + r15*r5*x45^2*x51^2 - 
    r2*r5*x45^2*x51^2 - r15*r5^2*x45^2*x51^2 - r2*r5^2*x45^2*x51^2 - 
    3*r9*x45^2*x51^2 - 6*r15*r9*x45^2*x51^2 - r5*r9*x45^2*x51^2 + 
    6*r15*r9*x45^3*x51^2 + r2*r5*x51^3 - 2*r15*r5^2*x51^3 - 2*r2*r5^2*x51^3 + 
    r5*r9*x51^3 + 3*r20*x34*x51^3 + r20*r7*x34^2*x51^3 + r3*r7*x34^2*x51^3 + 
    3*r20*x45*x51^3 - r15*r5*x45*x51^3 + 4*r15*r5^2*x45*x51^3 + 
    4*r2*r5^2*x45*x51^3 - 4*r15*r9*x45*x51^3 + r2*r20*x45^2*x51^3 + 
    r15*r5*x45^2*x51^3 - r2*r5*x45^2*x51^3 - 2*r15*r5^2*x45^2*x51^3 - 
    2*r2*r5^2*x45^2*x51^3 + 4*r15*r9*x45^2*x51^3 - r5*r9*x45^2*x51^3 - 
    r15*r5*x51^4 - r15*r5^2*x51^4 - r2*r5^2*x51^4 - r15*r9*x51^4 + 
    r5*r9*x51^4 + r15*r5*x45*x51^4 + 2*r15*r5^2*x45*x51^4 + 
    2*r2*r5^2*x45*x51^4 + r15*r9*x45*x51^4 - r5*r9*x45*x51^4 - 
    r15*r5^2*x45^2*x51^4 - r2*r5^2*x45^2*x51^4] + 
 (F[1, 1, 7]/24 - F[1, 1, 9]/24)*ratio[2 + 4*r3 + 2*r5 - 5*r3*r5 - r5^2 + 
    r3*r5^2 - 4*x23 - 3*r3*x23 + 4*x23^2 + 2*r3*x23^2 - 2*x23^3 - r3*x23^3 - 
    5*x34 - 7*r3*x34 - r5*x34 + 9*r3*r5*x34 + r5^2*x34 - 2*r3*r5^2*x34 + 
    7*x23*x34 + 5*r3*x23*x34 - 5*x23^2*x34 - 3*r3*x23^2*x34 + r3*x23^3*x34 + 
    2*x34^2 + 3*r3*x34^2 - 4*r3*r5*x34^2 + r3*r5^2*x34^2 - 2*x23*x34^2 - 
    2*r3*x23*x34^2 + r3*x23^2*x34^2 + 2*x45 - 5*x23*x45 + 8*x23^2*x45 - 
    2*r9*x23^2*x45 - 3*r23*x23^3*x45 - 4*x34*x45 + 7*x23*x34*x45 + 
    x34^2*x45 + 2*x45^2 - 10*x23*x45^2 - 2*r23*x23*x45^2 + 6*r9*x23*x45^2 + 
    6*r23*x23^2*x45^2 + 5*r9*x23^2*x45^2 - r23*r9*x23^2*x45^2 - 
    r23*r9*x23^3*x45^2 - 3*x34*x45^2 + 4*x45^3 + 2*r23*x45^3 - 4*r9*x45^3 - 
    3*r23*x23*x45^3 - 10*r9*x23*x45^3 + 2*r23*r9*x23*x45^3 + 
    3*r23*r9*x23^2*x45^3 + 5*r9*x45^4 - r23*r9*x45^4 - 3*r23*r9*x23*x45^4 + 
    r23*r9*x45^5 + 2*x51 + r12*x51 + r2*x51 + 5*r3*x51 + 2*r5*x51 - 
    r12*r5*x51 - r2*r5*x51 - 7*r3*r5*x51 - 2*r5^2*x51 + 2*r3*r5^2*x51 + 
    r9*x51 - r5*r9*x51 - 3*x23*x51 - r2*x23*x51 - 3*r3*x23*x51 - r9*x23*x51 + 
    r3*x23^2*x51 + 3*r23*x23^3*x51 + r3*x23^3*x51 - 5*x34*x51 + r12*x34*x51 - 
    4*r3*x34*x51 + r5*x34*x51 + 6*r3*r5*x34*x51 + r5^2*x34*x51 - 
    2*r3*r5^2*x34*x51 + 4*x23*x34*x51 + 2*r3*x23*x34*x51 - x34^2*x51 + 
    r12*x34^2*x51 + r12*x34^3*x51 + 4*x45*x51 - r12*x45*x51 - r2*x45*x51 - 
    r23*x45*x51 - r5*x45*x51 + 2*r12*r5*x45*x51 + 2*r2*r5*x45*x51 + 
    r23*r5*x45*x51 - 3*r9*x45*x51 + 4*r5*r9*x45*x51 - 10*x23*x45*x51 + 
    r23*x23*x45*x51 + 8*r9*x23*x45*x51 - 3*r23*x23^2*x45*x51 - 
    4*x34*x45*x51 + r12*x34^2*x45*x51 + 9*x45^2*x51 - r12*r5*x45^2*x51 - 
    r2*r5*x45^2*x51 - 6*r9*x45^2*x51 - 3*r5*r9*x45^2*x51 - 
    10*r9*x23*x45^2*x51 + 10*r9*x45^3*x51 + x51^2 + 3*r2*x51^2 + r3*x51^2 - 
    r12*r5*x51^2 - 3*r2*r5*x51^2 - 2*r3*r5*x51^2 - r5^2*x51^2 + 
    r12*r5^2*x51^2 + r3*r5^2*x51^2 + 3*r9*x51^2 - 4*r5*r9*x51^2 + 
    r5^2*r9*x51^2 - r23*r8*x23^2*x51^2 - r3*r8*x23^2*x51^2 - 
    r23*r8*x23^3*x51^2 - r3*r8*x23^3*x51^2 + 6*x45*x51^2 - 4*r12*x45*x51^2 - 
    2*r2*x45*x51^2 - r5*x45*x51^2 + 5*r12*r5*x45*x51^2 + 5*r2*r5*x45*x51^2 - 
    2*r12*r5^2*x45*x51^2 - 12*r9*x45*x51^2 + 12*r5*r9*x45*x51^2 - 
    2*r5^2*r9*x45*x51^2 - 3*r12*x34*x45*x51^2 - 4*r12*r5*x45^2*x51^2 - 
    2*r2*r5*x45^2*x51^2 + r12*r5^2*x45^2*x51^2 + 10*r9*x45^2*x51^2 - 
    8*r5*r9*x45^2*x51^2 + r5^2*r9*x45^2*x51^2 - 3*r2*r5*x51^3 + 
    2*r12*r5^2*x51^3 - 3*r5*r9*x51^3 + 2*r5^2*r9*x51^3 + 3*r12*r5*x45*x51^3 + 
    4*r2*r5*x45*x51^3 - 4*r12*r5^2*x45*x51^3 + 8*r5*r9*x45*x51^3 - 
    4*r5^2*r9*x45*x51^3 - 3*r12*r5*x45^2*x51^3 - r2*r5*x45^2*x51^3 + 
    2*r12*r5^2*x45^2*x51^3 - 5*r5*r9*x45^2*x51^3 + 2*r5^2*r9*x45^2*x51^3 + 
    r12*r2*x51^4 + r12*r5*x51^4 - r2*r5*x51^4 + r12*r5^2*x51^4 + 
    r5^2*r9*x51^4 - r12*r2*x45*x51^4 - r12*r5*x45*x51^4 + r2*r5*x45*x51^4 - 
    2*r12*r5^2*x45*x51^4 - 2*r5^2*r9*x45*x51^4 + r12*r5^2*x45^2*x51^4 + 
    r5^2*r9*x45^2*x51^4] + (F[1, 1, 5]/24 - F[1, 1, 9]/24)*
  ratio[-(r17*r3*x34^2) - r17*r5*x34^2 + r3*r5*x34^2 + r17*r3*r5*x34^2 - 
    2*x51 - r12*x51 - r3*x51 + 2*r5*x51 + r12*r5*x51 + r3*r5*x51 - r7*x51 - 
    r5*r9*x51 + r7*r9*x51 + x23*x51 + r3*x23*x51 + 3*x34*x51 - r12*x34*x51 + 
    2*r3*x34*x51 - 2*r3*r5*x34*x51 - x23*x34*x51 - 2*r3*x23*x34*x51 - 
    x34^2*x51 - r12*x34^2*x51 + r17*x34^2*x51 - r3*x34^2*x51 + 
    r17*r3*x34^2*x51 + r3*x23*x34^2*x51 - r12*x34^3*x51 - 2*x45*x51 + 
    r12*x45*x51 - 2*r12*r5*x45*x51 - r5*r7*x45*x51 + x23*x45*x51 + 
    x34*x45*x51 - r12*x34^2*x45*x51 + r17*x34^2*x45*x51 - x45^2*x51 + 
    r12*r5*x45^2*x51 - r9*x45^2*x51 + r5*r9*x45^2*x51 + r9*x23*x45^2*x51 - 
    r9*x45^3*x51 - x51^2 - r3*x51^2 + r12*r5*x51^2 + r3*r5*x51^2 - 
    r12*r5^2*x51^2 - r5^2*r9*x51^2 + 3*x34*x51^2 - 2*r17*x34^2*x51^2 + 
    r3*x34^2*x51^2 - r17*r3*x34^2*x51^2 - 3*x45*x51^2 + 4*r12*x45*x51^2 - 
    5*r12*r5*x45*x51^2 + 2*r12*r5^2*x45*x51^2 + 2*r9*x45*x51^2 - 
    4*r5*r9*x45*x51^2 + 2*r5^2*r9*x45*x51^2 + 3*r12*x34*x45*x51^2 - 
    3*r17*x34*x45*x51^2 + 4*r12*r5*x45^2*x51^2 - r12*r5^2*x45^2*x51^2 - 
    3*r9*x45^2*x51^2 + 4*r5*r9*x45^2*x51^2 - r5^2*r9*x45^2*x51^2 + 
    r2*r5*x51^3 - 2*r12*r5^2*x51^3 + r5*r9*x51^3 - 2*r5^2*r9*x51^3 + 
    3*r17*x34*x51^3 + r17*r3*x34^2*x51^3 + 3*r17*x45*x51^3 - 
    3*r12*r5*x45*x51^3 + 4*r12*r5^2*x45*x51^3 - 4*r5*r9*x45*x51^3 + 
    4*r5^2*r9*x45*x51^3 + 3*r12*r5*x45^2*x51^3 - 2*r12*r5^2*x45^2*x51^3 + 
    3*r5*r9*x45^2*x51^3 - 2*r5^2*r9*x45^2*x51^3 - r12*r2*x51^4 - 
    r12*r5*x51^4 + 2*r2*r5*x51^4 - r12*r5^2*x51^4 - r17*r7*x51^4 - 
    r2*r7*x51^4 + r17*r5*r7*x51^4 + r2*r5*r7*x51^4 - r5^2*r9*x51^4 + 
    r12*r2*x45*x51^4 + r12*r5*x45*x51^4 + 2*r12*r5^2*x45*x51^4 - 
    r17*r7*x45*x51^4 - r2*r7*x45*x51^4 + 2*r5^2*r9*x45*x51^4 - 
    r12*r5^2*x45^2*x51^4 - r5^2*r9*x45^2*x51^4 + r2*r5*x51^5 - r17*r7*x51^5 - 
    r2*r7*x51^5 + 2*r17*r5*r7*x51^5 + 2*r2*r5*r7*x51^5 + r17*r5*r7*x51^6 + 
    r2*r5*r7*x51^6] + (-F[1, 1, 2]/96 + F[1, 1, 9]/96)*
  ratio[-2 + 2*r2 + 8*r24 - 8*r3 - 4*r24*r3 + 2*r5 - 12*r24*r5 + 10*r3*r5 + 
    4*r24*r3*r5 + 2*r5^2 - 2*r3*r5^2 + 2*r8 - 2*r2*r8 + 2*x23 + 2*r2*x23 - 
    4*r24*x23 + 6*r3*x23 + 4*r24*r3*x23 + 2*r8*x23 - 2*r2*r8*x23 - 4*x23^2 - 
    4*r3*x23^2 - 4*r24*r3*x23^2 + 4*x23^3 + 4*r24*x23^3 + 4*r24*r3*x23^3 + 
    x34 - 8*r24*x34 + 8*r3*x34 + 8*r24*r3*x34 - 2*r5*x34 + 16*r24*r5*x34 - 
    12*r3*r5*x34 - 8*r24*r3*r5*x34 - 3*r5^2*x34 + 4*r3*r5^2*x34 - 
    4*r3*x23*x34 - 8*r24*r3*x23*x34 + 8*x23^2*x34 + 8*r24*x23^2*x34 + 
    8*r24*r3*x23^2*x34 - 4*r24*x23^3*x34 - 8*r24*r3*x23^3*x34 + 2*x34^2 - 
    4*r17*r3*x34^2 - 4*r24*r3*x34^2 - 4*r17*r5*x34^2 - 4*r24*r5*x34^2 + 
    6*r3*r5*x34^2 + 4*r17*r3*r5*x34^2 + 4*r24*r3*r5*x34^2 - 2*r3*r5^2*x34^2 - 
    4*x23*x34^2 + 4*r24*x23*x34^2 - 2*r3*x23*x34^2 + 4*r24*r3*x23*x34^2 - 
    8*r24*x23^2*x34^2 + 4*r3*x23^2*x34^2 - 4*r24*r3*x23^2*x34^2 + 
    4*r24*r3*x23^3*x34^2 - 4*x45 - 2*r2*x45 + 3*r5*x45 - r5^2*x45 + 
    2*r2*r8*x45 - 3*r5*r8*x45 + r5^2*r8*x45 + 2*x23*x45 - 8*x23^2*x45 + 
    4*r9*x23^2*x45 + 4*r9*x23^3*x45 - 4*x23*x34*x45 + 4*x34^2*x45 - 2*x45^2 + 
    4*x23*x45^2 - 2*r9*x23*x45^2 - 8*r9*x23^2*x45^2 - 2*r9*x45^3 + 
    4*r9*x23*x45^3 - 8*r2*x51 + 4*r24*x51 - 4*r3*x51 - r5*x51 + 6*r2*r5*x51 - 
    12*r24*r5*x51 + 8*r3*r5*x51 + 5*r5^2*x51 - 4*r3*r5^2*x51 + r7*x51 + 
    r5*r7*x51 - 3*r8*x51 + r5*r8*x51 + 2*r5*r9*x51 - 2*r7*r9*x51 + 
    4*x23*x51 + 4*r2*x23*x51 - 4*x23^2*x51 + 4*r9*x23^2*x51 - 
    4*r24*x23^3*x51 - 4*r9*x23^3*x51 + 4*x34*x51 + 4*r3*x34*x51 - 
    2*r5*x34*x51 + 8*r24*r5*x34*x51 - 8*r3*r5*x34*x51 - 2*r5^2*x34*x51 + 
    4*r3*r5^2*x34*x51 + 4*x23*x34*x51 + 4*r24*x23^2*x34*x51 - 8*x34^2*x51 + 
    4*r17*x34^2*x51 - 4*r3*x34^2*x51 + 4*r17*r3*x34^2*x51 - 
    4*r24*x23*x34^2*x51 + 4*r3*x23*x34^2*x51 + 8*r2*x45*x51 + r5*x45*x51 - 
    12*r2*r5*x45*x51 - r5^2*x45*x51 + r5*r7*x45*x51 - 3*r5*r8*x45*x51 + 
    2*r5^2*r8*x45*x51 + 4*r9*x45*x51 - 4*r5*r9*x45*x51 + 8*x23*x45*x51 - 
    8*r9*x23*x45*x51 - 4*r9*x23^2*x45*x51 - 12*x34*x45*x51 + 
    4*r17*x34^2*x45*x51 + 6*r2*r5*x45^2*x51 - 4*r9*x45^2*x51 + 
    2*r5*r9*x45^2*x51 + 8*r9*x23*x45^2*x51 - 4*r2*x51^2 + 6*r2*r5*x51^2 - 
    4*r24*r5*x51^2 + 2*r3*r5*x51^2 + 2*r5^2*x51^2 - 2*r3*r5^2*x51^2 + 
    r5*r7*x51^2 + r5*r8*x51^2 + 8*r9*x51^2 - 4*r24*r9*x51^2 - 
    10*r5*r9*x51^2 + 4*r24*r5*r9*x51^2 + 4*x23*x51^2 - 4*r24*x23*x51^2 - 
    8*r9*x23*x51^2 + 4*r24*r9*x23*x51^2 + 4*r24*x23^2*x51^2 + 
    4*r9*x23^2*x51^2 - 4*r24*r9*x23^2*x51^2 + 4*r24*r9*x23^3*x51^2 + 
    12*x34*x51^2 + 8*r24*x23*x34*x51^2 - 8*r17*x34^2*x51^2 + 
    4*r3*x34^2*x51^2 - 4*r17*r3*x34^2*x51^2 + 12*x45*x51^2 + 8*r2*x45*x51^2 + 
    2*r5*x45*x51^2 - 16*r2*r5*x45*x51^2 + r5^2*r7*x45*x51^2 + 
    r5^2*r8*x45*x51^2 - 4*r9*x45*x51^2 + 8*r5*r9*x45*x51^2 + 
    4*r9*x23*x45*x51^2 - 12*r17*x34*x45*x51^2 + 10*r2*r5*x45^2*x51^2 - 
    2*r5*r9*x45^2*x51^2 + 12*r9*x51^3 - 12*r24*r9*x51^3 - 24*r5*r9*x51^3 + 
    16*r24*r5*r9*x51^3 - 4*r24*x23*x51^3 - 4*r9*x23*x51^3 + 
    8*r24*r9*x23*x51^3 - 4*r24*r9*x23^2*x51^3 + 12*r17*x34*x51^3 + 
    4*r17*r3*x34^2*x51^3 + 12*r17*x45*x51^3 - 4*r9*x45*x51^3 + 
    16*r5*r9*x45*x51^3 + 4*r2*r5*x45^2*x51^3 - 4*r5*r9*x45^2*x51^3 + 
    4*r2*r5*x51^4 - 4*r17*r7*x51^4 - 4*r2*r7*x51^4 + 4*r17*r5*r7*x51^4 + 
    4*r2*r5*r7*x51^4 + 4*r9*x51^4 - 12*r24*r9*x51^4 - 16*r5*r9*x51^4 + 
    24*r24*r5*r9*x51^4 + 4*r24*r9*x23*x51^4 + 4*r2*r5*x45*x51^4 - 
    4*r17*r7*x45*x51^4 - 4*r2*r7*x45*x51^4 + 4*r5*r9*x45*x51^4 + 
    4*r2*r5*x51^5 - 4*r17*r7*x51^5 - 4*r2*r7*x51^5 + 8*r17*r5*r7*x51^5 + 
    8*r2*r5*r7*x51^5 - 4*r24*r9*x51^5 - 4*r5*r9*x51^5 + 16*r24*r5*r9*x51^5 + 
    4*r17*r5*r7*x51^6 + 4*r2*r5*r7*x51^6 + 4*r24*r5*r9*x51^6]
