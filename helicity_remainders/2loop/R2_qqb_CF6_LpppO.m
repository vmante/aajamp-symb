(*
   "Two-loop helicity amplitudes for diphoton plus jet production in full color"
    by Bakul Agarwal, Federico Buccioni, Andreas von Manteuffel and Lorenzo Tancredi.
    If you use this result, please cite 2105.04585
*)

-(ratio[-1 - r3 + r3*r5 + 2*x23 + r3*x23 - x23^2 - r3*x23^2 + x34 + r3*x34 - 
    r3*r5*x34 - 2*x23*x34 - r3*x23*x34 + r3*x23^2*x34 - x45 + 2*x23*x45 - 
    r9*x23^2*x45 + x34*x45 - x45^2 + 2*r9*x23*x45^2 - r9*x45^3 - 2*x51 - 
    r3*x51 + r5*x51 + r3*r5*x51 + r9*x51 - r5*r9*x51 + 3*x23*x51 + 
    r3*x23*x51 - r9*x23*x51 - 3*x45*x51 + r5*r9*x45*x51 + 3*r9*x23*x45*x51 - 
    3*r9*x45^2*x51 + r2*x51^2 - r2*r5*x51^2 + 2*r9*x51^2 - 2*r5*r9*x51^2 + 
    r2*r5*x45*x51^2 - 3*r9*x45*x51^2 + 2*r5*r9*x45*x51^2 - r2*r5*x51^3 - 
    r5*r9*x51^3 + r2*r5*x45*x51^3 + r5*r9*x45*x51^3]*
  (5/18 - F[1, 1, 1]/6 - F[1, 1, 2]/24 - F[1, 1, 6]/24 + tci[1, 2]/6))
