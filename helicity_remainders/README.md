This folder contains the one- and two-loop finite remainders of the helicity amplitudes for diphoton plus jet production. For details on the definitions of the finite remainders see [2105.04585][1].

1loop
---------------
This folder contains the results for the one-loop finite remainders. The files follow the naming scheme `R1_proc_CFi_hel.m` where `proc` can be `qqb`, `qg` and `gqb`, `i` is an index for the colour factor, here `i=1,...,4`, and `hel` labels a particular helicity configuration, and the options are `LpppE`, `LpppO`, `LmppE`, `LmppO`, `LmmpE` and `LmmpO`. In particular, `Lppp`, `Lmpp` and `Lmmp` identify the $`\left\lbrace L,+,+,+\right\rbrace`$, the $`\left\lbrace L,-,+,+\right\rbrace`$ and the $`\left\lbrace L,-,-,+\right\rbrace`$ helicity configurations respectively, whereas the ending `E/O` specifies the even/odd component of the helicity amplitude. For more details on the definition of the various helicity configurations and the separation into even and odd components see [2105.04585][1].

The explicit separation into gauge invariant colour structures at one loop follows Eqs.(30,31) of [2105.04585][1].

2loop
---------------
This folder contains the results for the two-loop finite remainders. The files follow the naming scheme `R2_proc_CFi_hel.m` where `proc` and hel are equivalent to the one-loop case, while the colour factor index `i` here takes values `i=1,...,10`.

The explicit separation into gauge invariant colour structures at two loop follows Eqs.(30,32) of [2105.04585][1].

[1]: https://arxiv.org/abs/2105.04585 "2105.04585"